<?php
/**
 * Template Name: Kariera
 *
 */

get_header();

wp_enqueue_script( 'contact-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php if ( ICL_LANGUAGE_CODE=='en' ) : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu-en"); ?>
					<?php else : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
					<?php endif; ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
				<?php the_content(); ?>
				<fieldset class="kontakt-obrazec">
					<form id="kontaktirajte-nas-kariera">
						<div class="row">
							<div class="col-lg-6">
								<input type="text" name="your-name" placeholder="<?php _e('Ime *', 'mlinotest'); ?>">	
							</div>
							<div class="col-lg-6">
								<input type="email" name="your-email" placeholder="<?php _e('E-pošta *', 'mlinotest'); ?>">
							</div>
							<div class="col-12">
								<textarea name="your-message" placeholder="<?php _e('Sporočilo *', 'mlinotest'); ?>"></textarea>
							</div>
						</div>
						<button class="btn btn-submit" id="form-submit-btn" type="submit"><?php _e('Pošlji', 'mlinotest'); ?></button>
					</form>
					<div class="form-submit-msg"></div>
				</fieldset>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
