<?php
/**
 * Template Name: B2B Kontakt
 *
 */

get_header();

wp_enqueue_script( 'b2b-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/b2b-left-menu"); ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
                <?php the_content(); ?>
                    <h1 class="mt-5"><?php echo __('Kontaktirajte nas', 'mlinotest') ?></h1>
                    <p>
                        <?php echo __('Če vas zanima sodelovanje z nami, stopite v stik z nami preko spodnjega obrazca. Z veseljem vam bomo odgovorili  na vsa vprašanja.', 'mlinotest')
                        ?>
                    </p>
                    <fieldset class="kontakt-obrazec">
                        <form id="kontaktirajte-nas-b2b">
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="text" name="your-name" placeholder="<?php _e('Ime in priimek *', 'mlinotest'); ?>">	

                                </div>
                                <div class="col-lg-6">
                                    <input type="email" name="your-email" placeholder="<?php _e('E-pošta *', 'mlinotest'); ?>">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="your-phone" placeholder="<?php _e('Telefon', 'mlinotest'); ?>">
                                </div>
                                <div class="col-lg-6">
                                    <select name="your-option" id="your-option" placeholder="<?php _e('Kaj vas zanima?', 'mlinotest'); ?>">
                                        <?php if(get_locale()==='sl_SI'): ?>
                                            <option value="blagovna znamka Mlinotest za slovenski trg"><?= __('blagovna znamka Mlinotest za slovenski trg', 'mlinotest'); ?></option>
                                            <option value="blagovna znamka Mlinotest za tuje trge"><?= __('blagovna znamka Mlinotest za tuje trge','mlinotest'); ?></option>
                                        <?php endif; ?>
                                        <option value="privat label"><?= __('privat label','mlinotest'); ?></option>
                                        <option value="izdelki za Ho.Re.Ca"><?= __('izdelki za Ho.Re.Ca','mlinotest'); ?></option>
                                        <option value="drugo"> <?= __('drugo', 'mlinotest'); ?></option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="your-company" placeholder="<?php _e('Podjetje *', 'mlinotest'); ?>">
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" name="your-subject" placeholder="<?php _e('Zadeva *', 'mlinotest'); ?>">
                                </div>
                                <div class="col-12">
                                    <textarea name="your-message" placeholder="<?php _e('Sporočilo *', 'mlinotest'); ?>"></textarea>
                                </div>
                            </div>
                            <button class="btn btn-submit" id="form-submit-btn" type="submit"><?php _e('Pošlji', 'mlinotest'); ?></button>
                        </form>
                        <div class="form-submit-msg"></div>
                    </fieldset>
                    
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
