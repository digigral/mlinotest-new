<?php
/**
 * Template Name: O podjetju
 *
 */

get_header();

wp_enqueue_script( 'contact-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php if ( ICL_LANGUAGE_CODE=='en' ) : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu-en"); ?>
					<?php else : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
					<?php endif; ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
				<?php the_content(); ?>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
