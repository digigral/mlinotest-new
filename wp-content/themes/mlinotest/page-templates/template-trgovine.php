<?php
/**
 * Template Name: Trgovine
 *
 */

get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside id="sidebar-top--js">
					<?php get_template_part("page-templates-parts/side/shops-left-menu"); ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
				<?php the_content(); ?>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
