<?php

/**
 * Template Name: Landing3
 *
 */
get_header();

?>

<style>
	.header {
		top: 0;
	}

	.wrapper {
		padding-bottom: 2rem;
	}

	.landing-link img:hover {
		opacity: 0.7;
		transition: 0.3s;
		display: block;
	}

	.landing-titles {
		font-weight: bold;
		display: block;
		font-size: 20px;
	}

	#landing {
		height: 500px;
		background: url('/wp-content/uploads/2020/11/baner-polente_Pc_1920x752px.jpg');
		background-position-x: center;
		background-repeat: no-repeat;
		background-size: contain;

	}

	#landing img {
		display: none;
	}

	@media only screen and (max-width: 667px) {

		.izdelek_prvi {
			margin-bottom: 50px;
			margin-top: 10px;
		}

		.col-sm-4,
		col-sm-3,
		col-sm-12,
		col-sm-6 {
			margin-bottom: 10px;
			margin-top: 10px;
		}

		#landing {
			height: auto;
			background: none;
		}

		#landing img {
			display: block;
		}

		.wrapper {
			padding-top: 1.5rem !important;
		}
	}

	.wpcf7-submit {

		background: #93100e !important;
		color: white !important;
		cursor: pointer !important;
		padding: 10px 20px 10px 20px !important;
		font-size: 22px !important;
		text-align: center !important;
		margin-top: 10px;

	}

	.wpcf7-submit:hover {

		opacity: 0.9;
		transition: 0.3s;
		cursor: pointer;

	}

	.wpcf7-text {
		border: 1px solid #00000073 !important;
		padding: 10px 10px 10px 10px !important;
		margin-top: 0px;
	}

	label {

		font-size: 1.2rem;
		margin-bottom: 0;
	}

	.wpcf7-response-output {
		margin: 0 !important;
		margin-bottom: 30px !important;
		margin-top: -20px !important;
	}

	.recipe_link:hover {}

	.izdelek_link:hover {
		opacity: 0.7;
		transition: 0.3s;
		cursor: pointer;
		text-decoration: none;
	}




	.izdelek_link {
		opacity: 0.7;
		transition: 0.3s;
		text-decoration: none;
	}

	p {

		font-size: 1.3rem;
	}
</style>

<section id="landing" style="" id="home-banner-main">
	<?php
	$bannerLink = get_field('banner_link', apply_filters('wpml_object_id', 5, 'post'));
	$link2 = $bannerLink['url'];
	?>
	<img src="/wp-content/uploads/2020/11/baner-polente_M_500x400px.jpg" alt="">
</section>



<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="container">

		<!-- tekst na zacetku -->
		<div class="row">
			<div class="col-sm-12">
				<h2 style="text-align: center;">Tradicionalno na sodoben način</h2>
				<p style="text-align: center;">Marsikatero živilo iz naše zgodovine je dandanes na novo odkrito. Veliki kuharski chefi navdih za svoje kreacije iščejo v tradicionalni kulinariki. S tem svojim kreacijam dajo pridih avtentičnosti in nas spominjajo na jedi, ki so nam jih pripravile naše babice - tudi polento.</p>
				<!-- <h2  style="text-align: center; margin-bottom: 20px;">Navdušeni nad polento?</h2>
				<h2  style="text-align: center; margin-bottom: 20px;">Preizkusite jo in se nam pridružite za našo mizo.</h2>
				<p style="text-align: center;"> Tiste, ki boste polento preizkusili in to delili z nami (mnenje, slika, recept na Facebooku in info@mlinotest.si)<br /> bomo tudi 2 izžrebanca nagradili s kulinaričnim doživetjem Juliane Krapež zanj in še prijatelja.</p> -->
			</div>
		</div>
		<!-- tekst na zacetku -->


		<!-- 3 slike -->
		<div style="margin-bottom: 30px; margin-top:10px;" class="row">
			<div class="col-sm-12">
				<h2 style="text-align: center;">Zakaj Mlinotestova polenta naše none?</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p style="text-align: center;"><img style="max-width: 70%; border: 5px solid transparent; border-radius: 50%;" src="/wp-content/uploads/2019/03/mlin_nakljucni.jpg"></p>
				<p style="text-align: center;     margin-bottom: 0.3rem;">Polenta je pri nas pogosta jed. Vesela sem, da lahko posežem po polenti slovenskega porekla. Počutim se bolj varno.</p>
				<p style="text-align: center;font-weight: bold;">Monika, potrošnica</p>
			</div>
			<div class="col-sm-4">
				<p style="text-align: center;"><img style="max-width: 70%; border: 5px solid transparent; border-radius: 50%;" src="/wp-content/uploads/2019/03/julijana.jpg"></p>
				<p style="text-align: center;  margin-bottom: 0.3rem;">Rada navdušujem z malo drugačnimi in kreativnimi jedmi iz polente. Pri mojih receptih, s spoštovanjem do naših non in tradicije, polenta zavzema osrednjo točko.</p>
				<p style="text-align: center;font-weight: bold;">Julijana Krapež, chefinja</p>
			</div>
			<div class="col-sm-4">
				<p style="text-align: center;"><img style="max-width: 70%; border: 5px solid transparent; border-radius: 50%;" src="/wp-content/uploads/2019/03/sean-čebron-1-1.jpg"></p>
				<p style="text-align: center;  margin-bottom: 0.3rem;">Ne parimo že zmletega zdroba, temveč v pari predkuhamo celo koruzno zrno, ki ga šele nato zmeljemo. To daje polenti poseben, poln okus.</p>
				<p style="text-align: center;font-weight: bold;">Sean Čebron, tehnolog</p>
			</div>
		</div>
		<!-- 3 slike -->


		<!-- obdaritev -->
		<!-- video  -->
		<div style="margin-bottom: 60px; margin-top:30px;" class="row">
			<div class="col-sm-12" style="text-align: center;">
				<iframe width="720" height="405" src="https://www.youtube.com/embed/lxGdy1z1aK4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		<!-- video --->
		<!-- izdelki -->
		<div style="margin-top: 10px;" class="row">
			<div class="izdelek_prvi col-sm-6">
				<a class="izdelek_link" target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-500-hitra-priprava">
					<img style="float: left;  padding-right: 35px; max-height: 264px;" src="/wp-content/uploads/2020/11/Pirina_poletna.png">
					<p style="font-size: 1.3rem; line-height: 1.3; font-weight: bold; text-transform: uppercase; margin-bottom: 5px;">Pirina polenta</p>
					<p style="font-size: 1.2rem;  line-height: 1.3;"><strong>Povezuje tradicijo in želje sodobnega potrošnika.</strong><br /> Vsebuje zdrob iz starodavnega žita pire in koruze poltrdinke, pridelane v Sloveniji. Hitra in enostavna priprava pirine polente, ki sodobni pripravi doda tradicionalni okus.</p>
					<p><a target="_blank" href="https://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/" style="color:white; cursor:pointer; margin-bottom:0px;" class="btn btn-primary">Več o izdelku</a></p>
					<!-- <p><a target="_blank"  href="http://www.mlinotest.si/nasveti/nekaj-dejstev-o-koruzi/" style="color:white; cursor:pointer; margin-bottom:10px;" class="btn btn-primary">Več o poltrdinki</a></p> -->
				</a>
			</div>
			<div class="col-sm-6">
				<a class="izdelek_link" target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-z-ajdo-nase-none-500-g">
					<img style="float: left;  padding-right: 35px;" src="http://www.mlinotest.si/wp-content/uploads/2019/02/polenta-z-ajdo-naše-none-500-g-L.png">
					<p style="font-size: 1.3rem; line-height: 1.3; font-weight: bold; text-transform: uppercase; margin-bottom: 5px;">Polenta z ajdo</p>
					<p style="font-size: 1.2rem;  line-height: 1.3;"><strong>Združuje dva tipična slovenska okusa - koruzo in ajdo.</strong><br /> Tako koruza kot ajda sta tradicionalna okusa slovenske kuhinje. Zdaj smo jo združili v polenti z ajdo. Bogat okus bo podžgal vašo domišlijo v kuhinji.</p>
					<p><a target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-z-ajdo-nase-none-500-g" style="color:white; cursor:pointer; margin-bottom:10px;" class="btn btn-primary">Več o izdelku</a></p>
				</a>
			</div>
		</div>
		<div style="margin-top: 50px;" class="row">
			<div class="izdelek_prvi col-sm-6">
				<a class="izdelek_link" target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-500-hitra-priprava">
					<img style="float: left;  padding-right: 35px;" src="http://www.mlinotest.si/wp-content/uploads/2018/06/polenta-naše-none-500-g-L.png">
					<p style="font-size: 1.3rem; line-height: 1.3; font-weight: bold; text-transform: uppercase; margin-bottom: 5px;">POLENTA POLTRDINKA NAŠE NONE </p>
					<p style="font-size: 1.2rem;  line-height: 1.3;"><strong>Aromatična  100 % slovenska poltrdinka je tradicionalna slovenska jed.</strong>  Tudi danes je polenta priljubljena jed, ki popestri naš vsakdanjik in tudi praznični jedilnik. Njen okus je enak kot včasih, priprava pa hitra in preprosta.</p>
					<p><a target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-z-ajdo-nase-none-500-g" style="color:white; cursor:pointer; margin-bottom:0px;" class="btn btn-primary">Več o izdelku</a></p>
					<p><a target="_blank" href="http://www.mlinotest.si/nasveti/nekaj-dejstev-o-koruzi/" style="color:white; cursor:pointer; margin-bottom:10px;" class="btn btn-primary">Več o poltrdinki</a></p>
				</a>
			</div>
			<div class="col-sm-6">
				<a class="izdelek_link" target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#koruzni-zdrob-1kg">
					<img style="float: left;  padding-right: 35px;" src="http://www.mlinotest.si/wp-content/uploads/2018/06/koruzni-zdrob-500-g-D.png">
					<p style="font-size: 1.3rem; line-height: 1.3; font-weight: bold; text-transform: uppercase; margin-bottom: 5px;">Koruzni zdrob</p>
					<p style="font-size: 1.2rem;  line-height: 1.3;"><strong>Za pravo primorsko polento po starem.</strong><br /> Naše babice so polento kuhale počasi, v bakrenih kotličkih, ob stalnem mešanju. Takšno polento, ki jo odlikuje posebno bogata tekstura, si lahko pripravite s koruznim zdrobom, ki je zdaj na voljo tudi v 500 g pakiranju. </p>
					<p><a target="_blank" href="http://www.mlinotest.si/izdelki/mlinotest/moke-zdrobi-in-kase/zdrobi/#polenta-z-ajdo-nase-none-500-g" style="color:white; cursor:pointer; margin-bottom:10px;" class="btn btn-primary">Več o izdelku</a></p>
				</a>
			</div>
		</div>
		<!-- izdelki -->

		<!-- recepti -->
		<div style="margin-bottom: 30px; margin-top:50px;" class="row">
			<div class="col-sm-12">
				<h2 style="text-align: center; margin-bottom: 30px;">Spoznajte recepte</h2>
			</div>
			<div style="padding: 5px;" class="col-sm-3">
				<a style="text-decoration:none; font-weight: bold;" class="recipe_link" target="_blank" href="https://www.mlinotest.si/recepti/mafin-iz-pirine-polente-s-skuto-in-spinaco/">

					<img src="/wp-content/uploads/2020/11/mafin-iz-pirine-poletno-s-skuto-in-spinaco_2.png" style="width: 100%; max-height: 160px;object-fit: cover;">
					<p style="line-height: 1.3; text-decoration:none; font-weight: bold; text-align:center; margin-top:8px;">Mafin iz pirine polente<br />s skuto in špinačo</p>
				</a>
			</div>
			<div style="padding: 5px;" class="col-sm-3">
				<a style="text-decoration:none; font-weight: bold;" class="recipe_link" target="_blank" href="http://www.mlinotest.si/recepti/krema-koruznega-zdroba-s-popecenimi-breskvami/">

					<img src="/wp-content/uploads/2019/02/krema-koruznega-zdroba-s-pope%C4%8Denimi-breskvami2.jpg">
					<p style="line-height: 1.3; text-decoration:none; font-weight: bold; text-align:center;  margin-top:8px;">Krema koruznega<br />zdroba s popečenimi breskvami</p>
				</a>
			</div>
			<div style="padding: 5px;" class="col-sm-3">
				<a style="text-decoration:none; font-weight: bold;" class="recipe_link" target="_blank" href="http://www.mlinotest.si/recepti/zeliscna-polenta-z-mozzarello-paradiznikom-in-prsutom/">

					<img src="/wp-content/uploads/2019/02/zeliščna-polenta-z-mozzarello-paradižnikom-in-pršutom.jpg">
					<p style="line-height: 1.3; text-decoration:none; font-weight: bold; text-align:center;  margin-top:8px;">Zeliščna polenta z mozzarello, paradižnikom<br />in pršutom</p>
				</a>
			</div>
			<div style="padding: 5px;" class="col-sm-3">
				<a style="text-decoration:none; font-weight: bold;" class="recipe_link" target="_blank" href="http://www.mlinotest.si/recepti/polnjeni-lignji-s-polento-in-olivami/">

					<img src="/wp-content/uploads/2019/02/polnjeni-lignji-s-polento-in-olivami.jpg">
					<p style="line-height: 1.3; text-decoration:none; font-weight: bold; text-align:center;  margin-top:8px;">Polnjeni lignji s<br />polento in<br />olivami</p>
				</a>
			</div>
		</div>
		<!-- recepti -->


		<!-- cta gumbi -->
		<div style="margin-top:50px;" class="row">

			<div class="col-12 text-center">
				<a href="https://www.mlinotest.si/recepti/" style="color:white; cursor:pointer; margin-bottom:10px;" class="btn btn-primary">Vsi recepti</a>
			</div>

			<div class="col-12 text-center">
				<a href="https://www.facebook.com/sharer/sharer.php?u=http://www.mlinotest.si/aromaticna-poletna-iz-slovenske-poltrdinke/" target="_blank" style=" margin-bottom:10px;color:white; cursor:pointer;" class="btn btn-primary">Namigni prijatelju</a>
			</div>

			<div class="col-12 text-center">
				<a href="https://www.facebook.com/mlinotest/" style="color:white; cursor:pointer;" class="btn btn-primary">Spremljaj Facebook</a>
			</div>


		</div>
		<!-- cta gumbi -->


		<!--
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<?php the_content(); ?>
				</div>
			</div>

			<div style="margin-bottom: 60px;" class="row">
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ravioli-spinaca-in-ricotta-250-g"><img class="alignleft size-large wp-image-26911 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/šinaša-in-ricota.png" alt="" width="640" height="920" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#tortelini-pesto-bazilika-250-g"><img class="alignleft size-large wp-image-26913 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/pestobazilika.png" alt="" width="640" height="881" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#tortelini-pecenka-250-g"><img class="alignleft size-large wp-image-26909 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/pečenka.png" alt="" width="640" height="892" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ravioli-zlahtni-siri-250-g"><img class="alignleft size-large wp-image-26907 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/žlahtni-sir.png" alt="" width="640" height="866" /></a>
				</div>
			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/" style="color:white; cursor:pointer;" class="btn btn-primary">Podrobno o izdelkih</a>
				</div>
			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<h3 class="entry-title">Recepti, ki prebudijo brbončice najbolj zahtevnih.</h3>
				</div>
			</div>

			<div style="margin-bottom: 60px; padding-left: 10px; padding-right: 10px;"  class="row">
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/zlahtna-pasta-festa/"><img class="alignleft size-large wp-image-26911 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/žlahtna-pašta-fešta.jpg" alt="" width="640" height="920" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Žlahtna pašta fešta</p>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/prasicek-v-radicu/"><img class="alignleft size-large wp-image-26913 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/prašiček-v-radiču.jpg" alt="" width="640" height="881" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Prašiček v radiču</p>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/pasta-popaj/"><img class="alignleft size-large wp-image-26909 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/pašta-popaj.jpg" alt="" width="640" height="892" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Pašta popaj</p>
				</div>
				<div class="col-sm-3">
					<a href="https://www.mlinotest.si/recepti/pasta-pod-oljko/" class="landing-link" ><img class="alignleft size-large wp-image-26907 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/pašta-pod-oljko-1.jpg" alt="" width="640" height="866" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Pašta pod oljko</p>
				</div>
			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/recepti/" style="color:white; cursor:pointer;" class="btn btn-primary" >Vsi recepti</a>
				</div>
			</div>

				<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.mlinotest.si/pastafesta/" target="_blank" style="color:white; cursor:pointer;" class="btn btn-primary">Namigni prijatelju</a>
				</div>
			</div>

			-->


	</div>




</div>
<br /><br />
<?php get_footer(); ?>

<script>
	var headerHeight = document.querySelector(".header").offsetHeight;
	document.getElementById("landing").style.marginTop = headerHeight;
</script>