<?php
/**
 * Template Name: Sklepi
 *
 */

get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
				</aside>
			</div>

            <?php
            $args1 = array(
                'post_type' => 'sklepi',
                'posts_per_page' => -1
            );
            $query1 = new WP_Query( $args1 );

            ?>

            
            <div class="col-md-8 order-1 order-md-2">
                <?php if($query1->have_posts()) : ?>
                    <ul class="sklici-list">
                    <?php while ($query1->have_posts()) : $query1->the_post(); ?>
                        <li>
                        <p><?php echo get_the_date('d.m.Y G:i'); ?></p>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>

		</div>
	</div>
</div>

<?php get_footer(); ?>