<style>
	body{
		background: white !important;
	}
</style>

<?php
/**
 * Template Name: Landing2
 *
 */
get_header();

?>

<style>

	.wrapper  {
		padding-bottom: 0;
	}

  .landing-link img:hover {
	  opacity: 0.7;
	  transition: 0.3s;
	  display: block;
  }

  .landing-titles {
	  font-weight: bold;
	  display: block;
	  font-size: 20px;
  }

  #landing {
	  height: 472px;  background: url('http://www.mlinotest.si/wp-content/uploads/2021/02/cover-2560x472-1.png');     background-position-x: center;
	  background-repeat: no-repeat;
	  /*background-size: contain;*/
  }

  @media only screen and (max-width: 667px) {
    #landing {
        height: 240px;
		background: url('http://www.mlinotest.si/wp-content/uploads/2021/02/cover-646x472-1.png');     background-position-x: center;
		background-repeat: no-repeat;
		background-size: contain;
    }

	.wrapper {
		padding-top: 1.5rem !important;
	}


}

  .wpcf7-submit {

			background: #93100e !important;
			color: white !important;
			cursor: pointer !important;
			padding: 10px 20px 10px 20px !important;
			font-size: 22px !important;
			text-align: center !important;
			margin-top: 10px;

	  }

	    .wpcf7-submit:hover {

				opacity: 0.9;
				transition: 0.3s;
				cursor:pointer;

	  }

	.wpcf7-text {
		border: 1px solid #00000073 !important;
		padding: 10px 20px 10px 20px !important;
		margin-top: 10px;
	}
	.carmedkrofi{
		background: white;
	}

	label {

		font-size: 1.2rem;
	}

</style>

<section id="landing" >
	<?php
	$bannerLink = get_field('banner_link', apply_filters( 'wpml_object_id', 5, 'post' ));
	$link2 = $bannerLink['url'];
	?>

		<div class="container">
			<div class="row justify-content-end align-items-center">

				<!-- <div class="col-6 d-none d-lg-block title-image">
					<img src="<?php echo get_template_directory_uri() . '/img/banner.jpg'; ?>" alt="">
				</div> -->

			</div>
		</div>



</section>



<div class="wrapper carmedkrofi" id="single-wrapper">

	<div class="container">

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<?php the_content(); ?>
				</div>
			</div>


			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<?php echo do_shortcode( '[contact-form-7 id="28776" title="Landing"]' ); ?>
				</div>
			</div>

		<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<p><a style="text-decoration:none;" href="https://www.facebook.com/mlinotest" target="_blank"><img style="width: 100px; height: auto;" src="/wp-content/uploads/2019/02/facebook-icon-1.png"> Mlinotest Facebook</a></p>

					<p>Sledite naši FB skupnosti, kjer vas bomo obvestili o zmagovalnem kraju.</p>
				</div>
			</div>

				<div class="row" style="margin-bottom: 0px;">
				<div class="col-12 text-center">
					<p><a style="text-decoration:none;" href="https://www.facebook.com/mlinotest" target="_blank">Pravila sodelovanja</a></p>


				</div>
			</div>




	</div>




</div>
<br/><br/>

<script>
document.addEventListener( 'wpcf7submit', function( event ) {
    return gtag_report_conversion('http://www.mlinotest.si/carmedkrofi/');
}, false );
</script>
<?php get_footer(); ?>
