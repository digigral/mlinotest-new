<?php
/**
 * Template Name: Zgodovina Old
 *
 */

get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php if ( ICL_LANGUAGE_CODE=='en' ) : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu-en"); ?>
					<?php else : ?>
						<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
					<?php endif; ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">

                <?php if ( have_rows('letnice_repeater') ) : ?>
                
                    <?php while( have_rows('letnice_repeater') ) : the_row(); ?>

                        <div class="zgodovina-single-wrapper">
                            <span class="letnica"><?php the_sub_field('letnica'); ?></span>
                            <span class="letnica-text"><?php the_sub_field('letnica_text'); ?></span>
						</div>
                
                    <?php endwhile; ?>
                
				<?php endif; ?>
				
				<?php 
				$thecontent = get_the_content();
				if(!empty($thecontent)) {
					the_content(); 
				}
				?>
                
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
