<?php
/**
 * Template Name: Izdelki-glavna
 *
 */

get_header();

$product_main_categories = mlinotest_get_main_product_categories( get_the_id() );
//d($product_main_categories);
?>

<div class="wrapper wrapper-subpages" id="izdelki-wrapper">
	<div class="container" >

		<div class="row justify-content-center izdelki-top">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>
			<div class="col-lg-9 col-12">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="row">

			<?php foreach ($product_main_categories as $key => $mc): ?>
				<div class="col-lg-4 col-md-6 col-12 main-cat-wrapper">

					<?php
					$logo = get_field('logotip_znamke', $key);
					$my_wp_query = new WP_Query();
					$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
					$child = get_page_children( $mc->ID, $all_wp_pages );

					$categories = mlinotest_get_page_children($key);
					$first_item = $categories;
					reset($first_item);
					$first_key = key($first_item);

					// check if main cat has children
					$childFirstKey = mlinotest_get_page_children( $first_key );
					if($childFirstKey) {
						$linkID = reset($childFirstKey)->ID;
					} else {
						$linkID = $first_key;
					}
					?>

					<div class="main-cat-images">
						<a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' )); ?>">
							<img class="izdelki-logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
							<img class="img-fluid izdelki-img" src="<?php echo get_the_post_thumbnail_url($key); ?>"> <!-- img -->
						</a>
					</div>

					<div class="white-desc-box">
						<h3 class="entry-title"><a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' ));  ?>"><?php echo get_the_title($key);  ?></a></h3>  <!-- title / link -->
						<?php echo get_post_field('post_content', $key); ?>
						<!-- list subcategorie -->
						<ul>
						<?php foreach($categories as $cat) :  ?>

							<?php
							$third = mlinotest_get_page_children($cat->ID);

							$perma;
							$has_third = false;

							if( count($third) > 0 ):
									$has_third = true;
					
										$perma = get_the_permalink( reset($third) );

						endif;

							?>

							<li><a href="<?php if($has_third):  echo $perma; else: echo get_the_permalink($cat->ID); endif; ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><span><?php echo $cat->post_title; ?></span></a></li>
						<?php endforeach; ?>

						</ul>
					</div>

					

				</div>
			<?php endforeach; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>
