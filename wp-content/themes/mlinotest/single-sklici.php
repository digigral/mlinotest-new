<?php
get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="container" id="content" tabindex="-1">
		
		<div class="row">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h1><?php _e('Sklici skupščine', 'mlinotest'); ?></h1>
				</div>
			</div>
		</div>

		<div class="row">
		
            <div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
				</aside>
			</div>
			
			<div class="col-md-8 order-1 order-md-2">
			
				<main class="site-main" id="main">
					

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'single' ); ?>

					<?php endwhile; ?>
					

				</main><!-- #main -->

			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>