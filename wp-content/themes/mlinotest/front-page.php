<?php get_header();

wp_enqueue_script( 'homepage-scripts' );
?>

<?php
$fields = get_fields();
?>

<?php if( get_locale() == 'sl_SI' ) : ?>
    <div class="page-wrapper">
        <?php include("page-templates-parts/home/slo/home-section-1-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-2-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-3-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-4-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-5-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-6-sl.php"); ?>
    </div>

<?php else : ?>
    <div class="page-wrapper">
        <?php include("page-templates-parts/home/slo/home-section-1-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-2-sl.php"); ?>
        <?php include("page-templates-parts/home/slo/home-section-4-sl.php"); ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
