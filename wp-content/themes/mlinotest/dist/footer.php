<?php
$product_main_categories = mlinotest_get_main_product_categories( 7 );
$fields = get_fields('option');
?>
<footer class="footer">
	<!-- <a href="#" id="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a> -->
	<section class="footer-main">
		<div class="container">
			<div class="row">

				<div class="col-lg-3">
					<div class="footer-logo-wrapper">
						<img class="img-fluid" src="<?php echo get_template_directory_uri() . '/img/header-logo-mlinotest.png'; ?>" alt="">
					</div>
					<?php if($fields['footer_facebook_link']) : ?>
						<p class="d-none d-md-block fb-desktop"><a href="<?php echo $fields['footer_facebook_link']; ?>" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i></span>facebook.com/mlinotest</a></p>
					<?php endif; ?>
				</div>

				<div class="col-lg-3 col-md-6 d-none d-md-block footer-section">

					<?php if( $fields['footer_o_nas_naslov'] ) : ?>
					<h3><?php echo $fields['footer_o_nas_naslov']; ?></h3>
					<?php endif; ?>

					<?php if( $fields['footer_o_nas_text'] ) : ?>
					<p><?php echo $fields['footer_o_nas_text']; ?></p>
					<?php endif; ?>

					<?php if( $fields['footer_o_nas_link'] ) : ?>
					<a href="<?php echo $fields['footer_o_nas_link']; ?>" class="link-more" ><?php echo $fields['footer_o_nas_link_text']; ?></a>
					<?php endif; ?>

				</div>
				<div class="col-lg-2 col-md-6 d-none d-md-block">
					<?php if($fields['footer_izdelki_naslov']) : ?>
					<h3><?php echo $fields['footer_izdelki_naslov']; ?></h3>
					<?php endif; ?>

					<ul class="footer-izdelki">

						<?php foreach ($product_main_categories as $key => $mc): ?>

							<?php
							$my_wp_query = new WP_Query();
							$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
							$child = get_page_children( $mc->ID, $all_wp_pages );

							$categories = mlinotest_get_page_children($key);
							$first_item = $categories;
							reset($first_item);
							$first_key = key($first_item);

							// check if main cat has children
							$childFirstKey = mlinotest_get_page_children( $first_key );
							if($childFirstKey) {
								$linkID = reset($childFirstKey)->ID;
							} else {
								$linkID = $first_key;
							}
							?>

							<li class="menu-item"><span>&gt;</span><a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' )); ?>"><?php echo get_the_title($key); ?></a></li>
						<?php endforeach; ?>

					</ul>

				</div>

				<div class="col-lg-2 col-md-6 d-none d-md-block footer-section">
					<?php if($fields['footer_meni_naslov']) : ?>
					<h3><?php echo $fields['footer_meni_naslov']; ?></h3>
					<?php endif; ?>

					<ul class="footer-strani">
						<?php foreach($fields['footer_meni_repeater'] as $s) : ?>
						<li class="menu-item"><span>&gt;</span><a href="<?php echo $s['footer_meni_link']; ?>"><?php echo $s['footer_meni_link_text']; ?></a></li>
						<?php endforeach; ?>
					</ul>

				</div>

				<div class="col-lg-2 col-md-6 d-none d-md-block footer-section">
					<?php if($fields['footer_kontakt_naslov']) : ?>
					<h3><?php echo $fields['footer_kontakt_naslov']; ?></h3>
					<?php endif; ?>

					<?php if ($fields['footer_kontakt_repeater']) : ?>
					<ul class="footer-kontakt">
						<?php foreach($fields['footer_kontakt_repeater'] as $kont) : ?>
						<?php
						if($kont['footer_kontakt_ikona'] === 'Lokacija') :
						$icon = 'fa fa-map-marker';
						elseif ($kont['footer_kontakt_ikona'] === 'Telefon') :
						$icon = 'fa fa-phone';
						else :
						$icon = 'fa fa-envelope';
						endif;
						?>
						<li><i class="<?php echo $icon; ?>" aria-hidden="true"></i><span><?php echo $kont['footer_kontakt_text']; ?></span></li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>

				</div>

				<div class="col-12 d-block d-md-none" id="footer-kontakt-mobile">
					<?php if ($fields['footer_kontakt_repeater']) : ?>
						<ul class="footer-kontakt">
							<div class="row">
								<?php foreach($fields['footer_kontakt_repeater'] as $k) : ?>
									<?php
									if($k['footer_kontakt_ikona'] === 'Lokacija') :
									$icon = 'fa fa-map-marker';
									elseif ($kont['footer_kontakt_ikona'] === 'Telefon') :
									$icon = 'fa fa-phone';
									else :
									$icon = 'fa fa-envelope';
									endif;
									?>
									<div class="col-12 col-sm-6">
										<li class="d-flex justify-content-center justify-content-sm-start"><i class="<?php echo $icon; ?>" aria-hidden="true"></i><span><?php echo $k['footer_kontakt_text']; ?></span></li>
									</div>
								<?php endforeach; ?>
								<div class="col-12 col-sm-6">
									<li id="fb-mobile"><a href="<?php echo $fields['footer_facebook_link']; ?>" target="_blank" class="d-flex justify-content-center justify-content-sm-start"><i class="fa fa-facebook" aria-hidden="true"></i><span>Mlinotest</span></a></li>
								</div>
							</div>
						</ul>
					<?php endif; ?>
				</div>


			</div>
		</div>
	</section>
	<section class="footer-bottom" style="background: #fff;">
		<div class="container">
			<div class="row">
					<div class="col-3">
					<a href="https://www.mlinotest.si/medijsko-sredisce/izberi-slovensko-izberi-mlinotest/">
						<img src="/wp-content/uploads/2020/05/izberislovensko-mlinotest.jpg" width="318" height="70" border="0" />
					
					</a>
					</div>
					<div class="col-3">
						<a href="https://aaa.bisnode.si/?companyID=SI1000005132061&lang=sl-SI" target="_blank"><img  src="https://aaa.bisnode.si/Banner/Banner?companyID=SI1000005132061&type=2&lang=sl-SI" width="372" height="70" border="0" /></a>
					</div>
					<div class="col-3">
						<a href="<?php the_permalink(apply_filters( 'wpml_object_id', 57, 'post' )); ?>"><img  class="img-fluid" src="<?php echo get_template_directory_uri() . '/img/footer-certification-PRP.png'; ?>" alt="" height="70"></a>
					</div>
					<div class="col-3">
						<a href="https://www.mlinotest.si/o-nas/regionalni-sklad/" target="_blank"><img  class="img-fluid" src="/wp-content/uploads/2019/06/clld_esrr-300x105.png" alt=" " "></a>
					</div>
			</div>
		</div>
	</section>

<?php wp_footer(); ?>
</body>
</html>
