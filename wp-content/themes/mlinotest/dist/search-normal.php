<?php get_header(); ?>

<div class="container container-search-normal">

	<div class="row">

		<div class="col-sm-12">

		<?php $title = $wp_query->query_vars['s']; ?>

		<h1 class="page-title"><?php printf( esc_html__( 'Rezultati iskanja za niz: %s', 'ave_theme_based' ),  '<span>' . get_search_query() . '</span>' ); ?></h1>

    <div class="" id="content" tabindex="-1">
        <main class="site-main row" id="main">

                <?php if ( have_posts() ) : ?>



                    <?php while ( have_posts() ) : the_post(); ?>
											<article <?php post_class("search-normal-article col-sm-6"); ?> id="post-<?php the_ID(); ?>">

												<h2 class="search-article-title"><?php the_title(); ?></h2>

												<div class="entry-summary">
													<?php the_excerpt(); ?>
												</div><!-- .entry-summary -->

												<div class="search-read-more">
												 	<a class="sweep-right" href="<?php echo get_the_permalink(); ?>"><?php _e("Preberi več", "ave_theme_based"); ?></a>
											  </div>

											</article><!-- #post-## -->

                    <?php endwhile; ?>

                <?php else : ?>
                   no result
                <?php endif; ?>

            </main><!-- #main -->
            <?php understrap_pagination(); ?>
        </div><!-- content -->

		</div>

	</div>

</div>
<?php get_footer(); ?>
