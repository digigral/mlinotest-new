<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();

		wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $the_theme->get( 'Version' ) );
		wp_enqueue_script( 'jquery' );


		$gMapsApi = "AIzaSyDESVFE1JRqjKkcuGk7summnVc9X0QXhHM";
		wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . $gMapsApi );

		// jquery-ui
	  //	wp_enqueue_script( 'easy-autocomplete', get_template_directory_uri() . '/src/js/ui/jquery-ui.js', array(), $the_theme->get( 'Version' ), true );

		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
		wp_enqueue_script( 'transition-js', get_template_directory_uri() . '/js/transition.min.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'zoom-js', get_template_directory_uri() . '/js/zoom.min.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/js/slick.min.js', array(), $the_theme->get( 'Version' ), true );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $the_theme->get( 'Version' ), true );

		wp_enqueue_script(  'scripts', get_template_directory_uri() . '/js/scripts.js', array(), $the_theme->get( 'Version' ), true );
		wp_register_script( 'homepage-scripts', get_template_directory_uri() . '/js/homepage.js', array(), $the_theme->get( 'Version' ), true );
		wp_register_script( 'contact-scripts', get_template_directory_uri() . '/js/contact.js', array(), $the_theme->get( 'Version' ), true );
		wp_register_script( 'b2b-scripts', get_template_directory_uri() . '/js/b2b.js', array(), $the_theme->get( 'Version' ), true );
		wp_register_script( 'maps_trgovine', get_template_directory_uri() . '/js/maps_trgovine.js', array(), $the_theme->get( 'Version' ), true );
		wp_register_script( 'recipes-scripts', get_template_directory_uri() . '/js/recipes.js', array(), $the_theme->get( 'Version' ), true );


		// Localize the script with new data
		$translation_array = array(

				'email_missing_msg' => __( 'Vnos e-pošte je obvezen!', 'scripts-mlinotest-js' ),
				'name_missing_msg' => __( 'Vnos imena je obvezen!', 'scripts-imlinotest-js' ),
				'message_missing_msg' => __( 'Prosimo vnesite sporočilo.', 'scripts-mlinotest-js' ),
				'fields_missing_msg' => __( 'Manjkajoči vnosi so obvezni!', 'scripts-mlinotest-js' ),
				'submit_success_msg' => __( 'Vaše sporočilo je bilo poslano.', 'scripts-mlinotest-js' ),
				'templateUrl' => get_stylesheet_directory_uri(),
				'weekdays' => __( 'Pon-pet:', 'scripts-mlinotest-js' ),
				'saturdays' => __( 'Sob:', 'scripts-mlinotest-js' ),
				'sundays' => __( 'Ned:', 'scripts-mlinotest-js' ),
				'google_street_view' => __( 'Ogled lokacije na Google Street zemljevidu', 'scripts-mlinotest-js' ),

		);
		wp_localize_script( 'scripts', 'translations', $translation_array );



		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
