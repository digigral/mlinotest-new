
(function ($, root, undefined) {

  $(function() {

    'use strict';
     $(document).ready(function() {
        $('.homepage-slider').slick();
     });

     function setCookie( cname, cvalue, exdays ) {
    var d = new Date();
    d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  $('#cookie-notice-button2').on('click', function() {


    setCookie( "ck_confirmed", "1", 90 );
    $( "#cookie-notice" ).addClass( "is-hidden" );

    return false;
  });

  $('#cookie-notice-button3').on('click', function() {

  
    setCookie( "ck_confirmed", "1", 90 );
    $( "#cookie-notice" ).addClass( "is-hidden" );

    return false;
  });


      // rotating

      var rotation = 0,
          scrollLoc = $(document).scrollTop();
      $(window).scroll(function() {
          var newLoc = $(document).scrollTop();
          var diff = scrollLoc - newLoc;
          rotation += diff, scrollLoc = newLoc;
          var rotationStr = "rotate(" + (rotation/15) + "deg)";
          $(".recipe-image-rotate").css({
              "-webkit-transform": rotationStr,
              "-moz-transform": rotationStr,
              "transform": rotationStr
          });
      });

      // end rotating

  });


})(jQuery, this);
