(function ($, root, undefined) {

	$(function () {

	'use strict';

   var map;
	 var markersArray = [];

   var myLatLng = {lat:46.0362191, lng: 14.4958992};
   map = new google.maps.Map(document.getElementById('map'), {
       zoom: 8,
       center: myLatLng
       });

   $(document).ready(function() {
    });

    jQuery.ajax({
        type : "post",
        dataType : "json",
        url : ajaxurl,
        data : {action: "trgovine_locations"},
        success: function(response) {
           codeInit(response);
        }
    })

		function populateMarkers(response) {

			$.each(response, function( index, value ) {

				var marker1 = new google.maps.Marker({
					position: {lat: parseFloat(value.loc.lat), lng: parseFloat(value.loc.lng) },
					map: map,
					icon: value.marker
				});

				markersArray.push(marker1);

				console.log(value.content);

				marker1.addListener('click', function() {
						var friendlyName = value.loc.address;
				});

				var contentToAppend = '<h6>' + value.title + '</h6>';
				// contentToAppend += '<b> ' + value.loc.address + '</b>';
				contentToAppend += '<div class="infowindow-content">' + value.content + '</div>';
				contentToAppend += '<a href="http://maps.google.com/maps?q=&layer=c&cbll=' + value.loc.lat + ',' + value.loc.lng + '" style="display:inline-block; font-size: 0.75rem; margin-top: 16px;" target="_blank">' + translations.google_street_view + '</a>';

				marker1.info = new google.maps.InfoWindow({
					content: contentToAppend
				});

				google.maps.event.addListener(marker1, 'click', function() {

					markersArray.forEach(function(marker) {
					 marker.info.close(map, marker);
				});

					marker1.info.open(map, marker1);
				});


			});

		}

		function deleteMarkers() {
			if (markersArray) {

				$.each(markersArray, function( index, value ) {
						 markersArray[index].setMap(null);
				});

				markersArray.length = 0;
			}
		}

    function codeInit(response) {
				populateMarkers(response);
    }

		$('.trgovina-filter-button').on('click', function(){

				var sel = [];

				$('input:checkbox.trgovina-filter-button').each(function () {
			       var sThisVal = (this.checked ? $(this).val() : "");

						 if(sThisVal != "") {
						 	sel.push(sThisVal);
						 }

			  });

				jQuery.ajax({
						type : "post",
						dataType : "json",
						url : ajaxurl,
						data : {action: "trgovine_locations_tax", sel:sel},
						success: function(response) {
							 resetMap(response);
						}
				})

		});

		function resetMap(response) {

			deleteMarkers();
			populateMarkers(response);

		}

    });

})(jQuery, this);
