<?php
    $facebook = get_field('facebook_link', 'options');
    $instagram = get_field('instagram_link', 'options');
    $youtube = get_field('youtube_link', 'options');
    $linkedin = get_field('linkedin_link', 'options');
    $mail = get_field('mail_link', 'options');
    $phone = get_field('phone_link', 'options');
?>      
<div class="social-links">
    <?php if(count($facebook) > 1 ): ?>
        <a href="<?php echo $facebook["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/facebook_icon.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($instagram): ?>
        <a href="<?php echo $instagram["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/instagram_icon.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($youtube): ?>
        <a href="<?php echo $youtube["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/youtube_icon.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($linkedin): ?>
        <a href="<?php echo $linkedin["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/linkedin_icon.png'; ?>" />
        </a>
    <?php endif; ?>   
    <?php if($mail): ?>
        <a href="mailto:<?php echo $mail["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/mail_icon.png'; ?>" />
        </a>
    <?php endif; ?> 
    <?php if($phone): ?>
        <a href="tel:<?php echo $phone ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/phone_icon.png'; ?>" />
        </a>
    <?php endif; ?> 
</div>