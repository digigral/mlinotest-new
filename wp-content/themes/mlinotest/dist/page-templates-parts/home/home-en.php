<?php
$en_id = 5157;
// $en_id = 4802;
$product_main_categories = mlinotest_get_main_product_categories( $en_id );
?>

<div class="wrapper wrapper-subpages" id="izdelki-wrapper" style="padding-bottom: 32px; padding-top: 32px;">
	<div class="container" >

		<div class="row justify-content-center izdelki-top">
		
			<div class="col-lg-9 col-12">

                <?php 
                $args_en = array(
                    'post_type' => 'any',
                    'post__in' => array( $en_id ),
                );
                $query_en = new WP_Query($args_en);
                ?>
                
                <?php if( $query_en->have_posts() ) : ?>
                    <?php while( $query_en->have_posts() ) : $query_en->the_post(); ?>
                    <p class="text-center"><?php the_content(); ?></p>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>

			</div>
		</div>

		<div class="row" style="padding-top: 16px;">

			<?php foreach ($product_main_categories as $key => $mc): ?>
				<div class="col-lg-4 col-md-6 col-12 main-cat-wrapper">

					<?php
					$logo = get_field('logotip_znamke', $key);
					$my_wp_query = new WP_Query();
					$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
					$child = get_page_children( $mc->ID, $all_wp_pages );

					$categories = mlinotest_get_page_children($key);
					$first_item = $categories;
					reset($first_item);
					$first_key = key($first_item);

					// check if main cat has children
					$childFirstKey = mlinotest_get_page_children( $first_key );
					if($childFirstKey) {
						$linkID = reset($childFirstKey)->ID;
					} else {
						$linkID = $first_key;
					}
					?>

					<div class="main-cat-images">
						<a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' )); ?>">
							<img class="izdelki-logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
							<img class="img-fluid izdelki-img" src="<?php echo get_the_post_thumbnail_url($key); ?>"> <!-- img -->
						</a>
					</div>

			
					<h3 class="entry-title"><a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' ));  ?>"><?php echo get_the_title($key);  ?></a></h3>  <!-- title / link -->
					<?php echo get_post_field('post_content', $key); ?>
				

					<!-- list subcategorie -->
					<ul>
					<?php foreach($categories as $cat) :  ?>

						<?php
						$third = mlinotest_get_page_children($cat->ID);

						$perma;
						$has_third = false;

						if( count($third) > 0 ):
								$has_third = true;
				
									$perma = get_the_permalink( reset($third) );

					  endif;

						?>

						<li><a href="<?php if($has_third):  echo $perma; else: echo get_the_permalink($cat->ID); endif; ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><span><?php echo $cat->post_title; ?></span></a></li>
					<?php endforeach; ?>

					</ul>

				</div>
			<?php endforeach; ?>

		</div>
	</div>
</div>