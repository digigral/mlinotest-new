<section id="home-storitve">
    <div class="container">
        <?php if ($fields['storitve_naslov']) : ?>
        <div class="row">
            <div class="col-12">
                <div class="col-12 text-center naslov-wrapper">
                    <p class="upper-heading">Poleg okusnih dobrot, vam nudimo tudi...</p>
                    <h2 class="section-title"><?php echo $fields['storitve_naslov']; ?></h2>
                    <img src="<?php echo get_template_directory_uri() . '/img/under-title-2.png'; ?>" alt="">
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">

            <?php if ( have_rows('storitve_repeater') ) : ?>

                <?php $i=1; while( have_rows('storitve_repeater') ) : the_row(); ?>
                    <?php
                    $storitevImg = get_sub_field('posamezna_storitev_slika');
                    $link = get_sub_field('posamezna_storitev_link');
                    ?>

                    <div class="col-md-6 single-storitev">
                        <div class="row">

                            <a  href="<?php echo $link['url']; ?>">
                                <div class="d-block d-xl-flex <?php echo $ord; ?> align-items-start justify-content-start flex-wrapper">
                                    <?php if ( $storitevImg ) : ?>
                                    <img class="img-fluid" src="<?php echo $storitevImg['url']; ?>" alt="<?php echo $storitevImg['alt']; ?>">
                                    <?php endif; ?>
                                    <h4 class="<?php echo $heading; ?>"><?php the_sub_field('posamezna_storitev_naslov'); ?><br/>

                                      <span><?php if($heading == "heading-left"): ?>Imate željo po edinstvenosti torte na vašem dogodku? Preverite našo ponudbo tort po naročilu.<?php else: ?>Eden od vrhuncev vsake prireditve je pogostitev. Poskrbite da bo vaša nepozabna in naredila vtis na udeležence.<?php endif; ?></span><br/><br/>
                                      <button href="<?php echo $link['url']; ?>" class="btn btn-primary">Več o tem</button>
                                  </h4>
                                </div>
                            </a>

                        </div>

                     </div>

                <?php $i++; endwhile; ?>

            <?php endif; ?>

        </div>

        <div class="row">
            <div class="col-12 text-center">
                <a href="<?php the_permalink(apply_filters( 'wpml_object_id', 418, 'post' )); ?>" class="link-more">
                <?php if ( get_field('storitve_btn_text') ) : ?>
                    <?php echo get_field('storitve_btn_text'); ?>
                <?php else : ?>
                <?php _e('Vse storitve', 'mlinotest'); ?>
                <?php endif; ?>
                </a>
			</div>
        </div>
    </div>
</section>
