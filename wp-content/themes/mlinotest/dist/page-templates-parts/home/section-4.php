<?php
//d($fields);
$nasvet = $fields['izbrani_nasveti'];
?>

<?php
$args1 = array(
	'post_type' => 'nasveti',
	'posts_per_page' => 1,
	'orderby' => 'rand',
	'order' => 'DESC'
);
$query1 = new WP_Query( $args1 );
?>

<?php if($query1->have_posts()) : ?>
<section id="home-izbrano">
	<div class="container">

		<?php if($fields['izbrano_za_vas_naslov']) :?>
		<div class="row">
			<div class="col-12 text-center naslov-wrapper">

				<h2 class="section-title"><?php echo $fields['izbrano_za_vas_naslov']; ?></h2>
				<img src="<?php echo get_template_directory_uri() . '/img/under-title-2.png'; ?>" alt="">

				<?php if($fields['izbrano_za_vas_podnaslov']) :?>
				<p class="podnaslov d-none d-lg-block"><?php echo $fields['izbrano_za_vas_podnaslov']; ?></p>
				<?php endif; ?>	

			</div>
		</div>
		<?php endif; ?>

		<?php while ($query1->have_posts()) : $query1->the_post(); ?>
			<div class="row">
				<div class="col-lg-6 text-center text-lg-left">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('large', array('class' => 'img-fluid')); ?>
					</a>
				</div>
				<div class="col-lg-6 align-self-center text-center text-lg-left">
					<h3><?php the_title(); ?></h3>
					<?php
					$cont = get_the_content();
					$trimmed = wp_trim_words($cont, 55, $more = null);
					?>
					<p class="d-none d-lg-block"><?php echo $trimmed; ?></p>	
					<a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Preberite', 'mlinotest'); ?></a>
				</div>
			</div>
		<?php endwhile; ?>
		
	</div>
</section>
<?php endif; ?>



