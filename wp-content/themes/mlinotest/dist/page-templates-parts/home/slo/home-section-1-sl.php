<?php if ( have_rows('slider_repeater') ) : ?>
    <section id="home-banner-main" class="homepage-slider">
        <?php $i=1; while( have_rows('slider_repeater') ) : the_row(); ?>
                <?php 
                    $slider_slika = get_sub_field('slider_slika');
                    $slider_tekst = get_sub_field('slider_tekst');
                    $slider_povezava = get_sub_field('slider_povezava');
                ?>
                <div class="d-flex align-items-center text-center">
                    <img src="<?php echo $slider_slika ?>" />
                    <div class="container d-flex align-items-center">
                        <div class="holder">
                            <?php if( $slider_tekst ): ?>
                                <h1 class="text-center"><?php echo $slider_tekst ?></h1>
                            <?php endif; ?>    
                            <?php if( $slider_povezava): ?>
                                <div class="text-center w-100">
                                    <a href="#" class="btn btn-primary">Spoznaj</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>