<?php
	$en_id = 5157;
	$product_main_categories = mlinotest_get_main_product_categories($en_id);
	$nase_znamke_naslov = get_field('nase_znamke_naslov');
	$nase_znamke_tekst = get_field('nase_znamke_tekst');
?>
<section class="home-our-brands">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?php if($nase_znamke_naslov): ?>
					<h1 class="text-center"><?php echo $nase_znamke_naslov ?></h1>
				<?php endif; ?>
				<?php if($nase_znamke_tekst): ?>
					<p class="text-center">
						<?php echo $nase_znamke_tekst ?>
					</p>
				<?php endif; ?>	
            </div>
		</div>
		<div class="row justify-content-center">
			<?php foreach ($product_main_categories as $key => $mc): ?>
				<div class="col-lg-4 col-md-4 col-12 brand">
					<?php
					$logo = get_field('logotip_znamke', $key);
					$my_wp_query = new WP_Query();
					$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
					$child = get_page_children( $mc->ID, $all_wp_pages );

					$categories = mlinotest_get_page_children($key);
					$first_item = $categories;
					reset($first_item);
					$first_key = key($first_item);

					$childFirstKey = mlinotest_get_page_children( $first_key );
					if($childFirstKey) {
						$linkID = reset($childFirstKey)->ID;
					} else {
						$linkID = $first_key;
					}
					?>

					<div class="brand-wrapper d-flex justify-content-center align-items-center">
						<a href="<?php the_permalink(apply_filters( 'wpml_object_id', $linkID, 'post' )); ?>">
							<img class="izdelki-logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
    </div>
</section>