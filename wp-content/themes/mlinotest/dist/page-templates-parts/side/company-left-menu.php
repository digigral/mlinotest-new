<?php
// get data here, and do the left menu regarding page structure, pages children
$subpages= mlinotest_get_page_children(apply_filters( 'wpml_object_id', 13, 'post' )); // TO DO: make ID wml multilingual
$cur_id = get_the_id();
$subpages2 = mlinotest_get_page_children(apply_filters( 'wpml_object_id', 494, 'post' ));
$subpages3 = array_merge($subpages, $subpages2);
// d($subpages3);
?>

<?php foreach($subpages as $sub) :
	if($sub->ID == $cur_id) :
		$activeSidebar = 'active-sidebar';
	endif;

	$sec = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $sub->ID, 'post'  ) );
	foreach($sec as $s) :
		if($s->ID == $cur_id) :
			$activeSidebar = 'active-sidebar';
		endif;
	endforeach;
endforeach; ?>

<?php foreach($subpages2 as $sub2) :
	if($sub2->ID == $cur_id) :
		$activeSidebar = 'active-sidebar-2';
	endif;
	$sec = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $sub2->ID, 'post'  ) );

	foreach($sec as $s) :
		if($s->ID == $cur_id) :
			$activeSidebar = 'active-sidebar-2';
		endif;

	endforeach;
endforeach;

if( is_singular(array('sklici', 'sklepi')) or is_archive('objave') ) {
	$activeSidebar = 'active-sidebar-2';
}
if( is_singular(array('mediji' )) ) {
	$activeSidebar = 'active-sidebar';
}
?>



<!-- if active link in o nas -->
<?php if($activeSidebar == 'active-sidebar' ) : ?>

<div class="sidebar-navigation-wrapper active">
	<?php if($subpages) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 13, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages as $s): ?>

			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s->ID, 'post'  ) );
			$second_active = false;
			$has_second = false;

			if( is_singular(array('mediji' )) ) {
				if( $s->post_title == "Medijsko središče" ) {
					$second_active = true;
				}
			}

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			if($cur_id == $s->ID ) {
				$second_active = true;
			}
			if($secondLevel) {
				foreach($secondLevel as $s2) {
					if($cur_id == $s2->ID) {
						$second_active = true;
					}
				}
			}
			?>

			<?php if($has_second) : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php else : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php endif; ?>

			<?php if($has_second && $second_active && $secondLevel) : ?>
				<ul class="fds <?php if($second_active == false ){ } else { echo "hidden"; } ?>" style="padding-top: 0;">
					<?php foreach($secondLevel as $key => $s) : ?>

						<?php
						$singleActive = false;  
						if( is_singular(array('mediji' )) ) : 
							$singleActive = true; 
						endif;
						?>

						<li style="padding-left: 25px;" <?php if($s->ID == $cur_id or $singleActive && $s->post_title == "Sporočila za javnost" ) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
	
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>

<div class="sidebar-navigation-wrapper">
	<?php if($subpages2) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left dropdown-heading"><?php echo get_the_title(apply_filters( 'wpml_object_id', 494, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages2 as $s2): ?>

			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s2->ID, 'post' ) );
			$has_second = false;

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			?>
			
			<?php if($has_second) : ?>
			<li <?php if($s2->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php else : ?>
			<li <?php if($s2->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s2->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>

	<button class="dropdown-button" type="button"><i class="fa fa-angle-down" aria-hidden="true"></i></button>

	<?php endif; ?>
</div>

<!-- no active links in sidebar -->
<?php elseif( !$activeSidebar ) : ?>

<div class="sidebar-navigation-wrapper active">
	<?php if($subpages) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 13, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages as $s): ?>
			
			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s->ID  ) );
			$has_second = false;

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			?>

			<?php if($has_second) : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php else : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>

<div class="sidebar-navigation-wrapper active">
	<?php if($subpages2) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 494, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages2 as $s2): ?>
			
			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s2->ID, 'post'  ) );
			$has_second = false;

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			?>
			
			<?php if($has_second) : ?>
			<li <?php if($s2->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php else : ?>
			<li <?php if($s2->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s2->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>

<!-- if active link za delničarje -->
<?php else : ?>


<div class="sidebar-navigation-wrapper active">
	<?php if($subpages2) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 494, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages2 as $s2): ?>

			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s2->ID  ) );
			$second_active = false;
			$has_second = false;

			if( is_singular(array('sklepi' )) ) {
				if( $s2->post_title == "Sklepi skupščine" ) {
						$second_active = true;
				}
			}

			if( is_singular(array('sklici' )) ) {
				if( $s2->post_title == "Sklici skupščine" ) {
						$second_active = true;
				}
			}

			if( is_archive(array('objave' )) ) {
				if( $s2->post_title == "Objave" ) {
						$second_active = true;
				}
			}

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			if($cur_id == $s2->ID ) {
				$second_active = true;
			}
			if($secondLevel) {
				foreach($secondLevel as $s) {
					if($cur_id == $s->ID) {
						$second_active = true;
					}
				}
			}

			?>

			<?php if($has_second) : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php else : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink($s2->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s2->ID); ?></a></li>
			<?php endif; ?>


			<?php if($has_second && $second_active && $secondLevel) : ?>
				<ul class="fds <?php if($second_active == false){ } else { echo "hidden"; } ?>" style="padding-top: 0;">
					<?php foreach($secondLevel as $key => $s) : ?>
						<li style="padding-left: 25px;" <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>

<div class="sidebar-navigation-wrapper">
	<?php if($subpages) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left dropdown-heading"><?php echo get_the_title(apply_filters( 'wpml_object_id', 13, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages as $s): ?>

			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s->ID  ) );
			$has_second = false;

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			?>
			
			<?php if($has_second) : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php else : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>

	<button class="dropdown-button" type="button"><i class="fa fa-angle-down" aria-hidden="true"></i></button>

	<?php endif; ?>
</div>

<?php endif; ?>
