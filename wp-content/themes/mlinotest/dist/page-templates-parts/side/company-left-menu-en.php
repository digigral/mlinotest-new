<?php
// get data here, and do the left menu regarding page structure, pages children
$subpages= mlinotest_get_page_children(apply_filters( 'wpml_object_id', 13, 'post' )); // TO DO: make ID wml multilingual
$cur_id = get_the_id();
// $subpages2 = mlinotest_get_page_children(apply_filters( 'wpml_object_id', 494, 'post' ));
// $subpages3 = array_merge($subpages, $subpages2);
// d($subpages3);
?>

<div class="sidebar-navigation-wrapper active">
	<?php if($subpages) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 13, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages as $s): ?>
			
			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s->ID  ) );
			$has_second = false;

			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			?>

			<?php if($has_second) : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php else : ?>
			<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>

