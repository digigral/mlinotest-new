<?php
$cats = mlinotest_get_kategorije_rec_terms();
$cur_id = get_the_id();
$cur_terms = wp_get_post_terms( $cur_id, 'kategorija-recepta' );
$cur_term = $cur_terms[0];
?>

<?php foreach($cats as $cat) : ?>

<?php 
$args = array(
		'post_type' => 'recepti',
		'tax_query' => array(
				array(
						'taxonomy' => 'kategorija-recepta',
						'field'    => 'slug',
						'terms'    => $cat,
				),
		),
);
$query = new WP_Query( $args );
?>


<?php if( $cat == $cur_term) : ?>
<div class="sidebar-navigation-wrapper active">
 	<ul class="sidebar-navigation">
 		<h3 class="text-left"><?php echo $cat->name; ?></h3>

 		<?php if($query->have_posts()) : ?>
 			<ul class="dropdown-menu-sidebar">
 				<?php while($query->have_posts()) : $query->the_post(); ?>

 					<li class="<?php if( $post->ID == $cur_id) : echo 'active'; endif; ?>"><a href="<?php the_permalink(); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php the_title(); ?></a></li>
 				<?php endwhile; ?>
 			</ul>
 		<?php endif; ?>
		
	</ul>
	
	<button class="dropdown-button" type="button"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
</div>
<?php endif; ?>
<?php endforeach; ?>

<div class="sidebar-navigation-wrapper">
	<ul class="sidebar-navigation">
	<h3 class="text-left"><?php _e('Ostale kategorije', 'mlinotest'); ?></h3>
		<ul class="no-dropdown-menu-sidebar">
			<?php foreach($cats as $cat) : ?>

				<?php if( $cat != $cur_term) : ?>
				<?php
				$args2 = array(
						'post_type' => 'recepti',
						'posts_per_page' => 1,
						'tax_query' => array(
								array(
										'taxonomy' => 'kategorija-recepta',
										'field'    => 'slug',
										'terms'    => $cat->slug,
								),
						),
				);
				$query2 = new WP_Query( $args2 );
				?>
				<?php while ($query2->have_posts()) : $query2->the_post(); ?>
					<?php $id2 = get_the_id(); ?>
					<li class="js-dropdown-cat-recepti <?php if( $post->ID == $cur_id) : echo 'active'; endif; ?>"><a href="<?php the_permalink($id2); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo $cat->name; ?></a></li>
				<?php endwhile; ?>
				<?php endif; ?>

		 	<?php endforeach; ?>
		 </ul>
	</ul>
</div>




