<?php
$nasveti = mlinotest_get_all_nasveti_posts();
?>


<div class="sidebar-navigation">
	<h3 class="text-left"><?php _e('Nasveti', 'mlinotest'); ?></h3>
	<ul>
		<?php
		$countNasveti = count($nasveti);
		if($countNasveti >= 10 ) {
			$randomNasveti = array_rand($nasveti, 9);
		} else {
			$randomNasveti = array_rand($nasveti, $countNasveti - 1);
		}
		?>
		
		<!-- active izbira na vrh -->
		<?php $i = 1; foreach($nasveti as $n) : ?>

			<?php if( $cur_id == $n->ID ) : ?>
				<li <?php if($cur_id == $n->ID) : echo 'class="active"'; endif; ?> ><a href="<?php echo $n->post_name; ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo $n->post_title; ?></a></li>
			<?php endif; ?>

		<?php $i++; endforeach; ?>		

		<!-- izberi 9 nasvetov na random -->
		<?php $i = 1; foreach($nasveti as $n) : ?>

			<?php if(in_array($i, $randomNasveti) && $cur_id != $n->ID) : ?>
				<li <?php if($cur_id == $n->ID) : echo 'class="active"'; endif; ?> ><a href="<?php echo $n->post_name; ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo $n->post_title; ?></a></li>
			<?php endif; ?>

		<?php $i++; endforeach; ?>
	
	</ul>
	<div class="link-more-nasveti text-center">
		<a class="" href="<?php echo get_post_type_archive_link('nasveti'); ?>"><?php _e('Vsi nasveti', 'mlinotesti'); ?></a>
	</div>
</div>
