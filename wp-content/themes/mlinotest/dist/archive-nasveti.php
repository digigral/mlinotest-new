<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper wrapper-subpages" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main w-100" id="main">
			
			<div class="container">
			
				<div class="row justify-content-center">
					<div class="col-lg-9 col-12 text-center">
						<div class="naslov-wrapper">
							<h1><?php _e('Nasveti', 'mlinotest'); ?></h1>
						</div>
					</div>
				</div>
				
				<!-- start featured -->
				<?php 
				$featured = get_field('featured_nasveti', 'options');
				if ( $featured ) :
				?>
									
				<section id="nasveti-featured-top">
					<div class="row">
						<div class="col-lg-6">
							<h3 class="entry-title"><?php echo $featured->post_title; ?></h3>
							<?php
							$text = $featured->post_content;
							$trimmed = wp_trim_words( $text, $num_words = 70, $more = null );
							$thumb = get_the_post_thumbnail($featured->ID);
							?>
							<p><?php echo $trimmed; ?></p>
							<a class="btn btn-primary" href="<?php the_permalink($featured->ID); ?>"><?php _e('Več', 'mlinotest'); ?></a>
						</div>
						<div class="col-lg-6">
							<?php echo $thumb; ?>
						</div>
					</div>
				</section>
				
				<?php endif; ?>
				<!-- end featured -->
					
	
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="text-center"><?php _e('Ostali nasveti', 'mlinotest'); ?></h2>
					</div>
				</div>
			
				<section id="nasveti-grid">
			

					<?php 

					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

					$args1 = array (
						'post_type' => 'nasveti',
						'post__not_in' => array($featured->ID),
						'posts_per_page' => 12,
						'post_status' => 'publish',
    					'paged' => $paged
					);
					$query1 = new WP_Query( $args1 );
					$wp_query = $query1;
					?>

					<?php if($wp_query->have_posts()) : ?>
						<div class="row">

							<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
							
								<div class="col-lg-4 col-md-6 col-12 nasveti-grid-single">
									<a class="img-overlay" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('large', array( 'class' => 'img-fluid')); ?>
									</a>
									<a href="<?php the_permalink(); ?>">
										<h3 class="entry-title"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php the_title(); ?></h3>
									</a>
								</div>

							<?php endwhile; ?>

						</div>
					<?php endif; wp_reset_postdata(); ?>
				</section>
				

			</div>
			</main><!-- #main -->

			<!-- The pagination component -->
			<?php
			custom_pagination();
			?>

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php
get_footer();
?>
