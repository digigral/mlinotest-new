<?php
/**
 * Template Name: Izdelki
 *
 */

get_header();

$products = mlinotest_get_products( get_the_id() );
?>

<div class="wrapper wrapper-subpages has-fixed-element" id="izdelki-wrapper">
	<div class="container izdelki-list-wrapper">
			<div class="row">

				<div class="col-md-4 col-12 order-2 order-md-1 sidebar-column">
					<aside>
						<?php get_template_part("page-templates-parts/side/product-left-menu"); ?>
					</aside>
				</div>

				<div class="col-md-8 col-12 order-1 order-md-2">
					<div class="row">
						<div class="col-12 text-center text-md-left">
								<h1 class=""><?php the_title(); ?></h1>
								<p class="lead"><?php the_content(); ?></p>
						</div>
					</div>

					<?php $j=2; foreach($products as $p) : ?>

					<?php
					$cur_id = $p['izdelek']->ID;
					$fields_prod = get_fields($cur_id);
					$url_recepti = '?id=';
					$count = count($fields_prod['izdelek_povezani_recepti']);
					$i = 1;
					if($fields_prod['izdelek_povezani_recepti']) :
						foreach($fields_prod['izdelek_povezani_recepti'] as $povezani) :
								if($i != $count) :
								$url_recepti = $url_recepti . $povezani['recept']->ID . ',';
								else :
								$url_recepti = $url_recepti . $povezani['recept']->ID;
								endif;
								$i++;
						endforeach;
						$url_recepti .= "#seznam-izdelki";
					else :
						$url_recepti = '';
					endif;

					$layout = get_field('izdelek_layout', $cur_id);
					$badge = get_field('izdelek_znacka', $cur_id);
					$badgelink = get_field('izdelek_znacka_1_link', $cur_id);
					$badge2 = get_field('izdelek_znacka_2', $cur_id);
					?>

					<!-- izdelek vertikalen layout -->
					<?php if($layout == 'Vertikalen' or !$layout) : ?>
					<article class="single-izdelek" id="<?php echo $p['izdelek']->post_name; ?>">
						<div class="row justify-content-center izdelki-list-single">
							<?php
							$thumb = get_the_post_thumbnail($p['izdelek']->ID, '', array("class" => "img-fluid main-product-img", 'data-action' => 'zoom'));
							if ($j%2 == 0) :
							$pos1 = 'order-1 order-xl-2';
							$pos2 = 'order-2 order-xl-1';
							$imgPosClass = 'img-right';
							else :
							$pos1 = 'order-1 order-xl-1';
							$pos2 = 'order-2 order-xl-2';
							$imgPosClass = 'img-left';
							endif;
							?>
							<div class="col-xl-4 <?php echo $pos2; ?> text-center text-md-left" style="margin-bottom: 16px;">
								<div class="izdelki-img-wrapper">
									<?php echo $thumb; ?>
									<?php if( ICL_LANGUAGE_CODE=='sl' ) : ?>
										<?php if($badge) : ?>
											<?php if($badgelink): ?>
												<a target="_blank" class="link-overlay" href="<?php echo $badgelink; ?>">
													<img class="product-badge <?php echo $imgPosClass; ?>" src="<?php echo $badge['url']; ?>" alt="<?php echo $badge['url']; ?>">
												</a>
											<?php else: ?>
												<img class="product-badge <?php echo $imgPosClass; ?>" src="<?php echo $badge['url']; ?>" alt="<?php echo $badge['url']; ?>">
											<?php endif; ?>
										<?php endif; ?>
										<?php if($badge2) : ?>
											<img class="product-badge product-badge2 <?php echo $imgPosClass; ?>" src="<?php echo $badge2['url']; ?>" alt="<?php echo $badge2['url']; ?>">
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>

							<?php
							$nutri = get_field('hranilne_vrednosti', $p['izdelek']->ID);
							?>
							<div class="col-xl-8 <?php echo $pos1; ?> text-center text-md-left">
								<h3 class="entry-title"><?php echo $p['izdelek']->post_title; ?></h3>
								<p><?php echo $p['izdelek']->post_content; ?></p>
								<div class="products-btn-wrapper">

									<?php if($nutri) : ?>
									<a class="btn btn-primary js-nutritional-info-btn" href="#"><?php _e('Hranilna vrednost', 'mlinotest'); ?></a>
									<?php endif; ?>

									<?php if( ICL_LANGUAGE_CODE=='sl' ) : ?>
										<?php if($url_recepti) : ?>
										<a style="margin-right: 1rem;" class="btn btn-secondary"  href="<?php echo get_post_type_archive_link( 'recepti' ) . $url_recepti; ?>"><?php _e('Recepti', 'mlinotest'); ?></a>
										<?php endif; ?>
									<?php endif; ?>

									<?php if( !isset($_COOKIE["b2b"]) || !($_COOKIE["b2b"]) ): ?><?php else: ?>
									<!--<a class="btn btn-primary js-b2b-info-btn" href="#"><?php _e('B2b', 'mlinotest'); ?></a>-->
							  	<?php endif; ?>

								</div>
							</div>


							<?php if($nutri) : ?>
							<div class="col-12 order-3">
								<div class="nutritional-info">
									<h5><?php _e('Povprečna hranilna vrednost na 100 g', 'mlinotest'); ?></h5>
									<table>

										<?php if ( $nutri['energijska_vrednost'] ) : ?>
											<tr>
												<td><?php _e('Energijska vrednost', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['energijska_vrednost']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['mascobe'] ) : ?>
											<tr>
												<td><?php _e('Maščobe', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['mascobe']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['mascobne_kisline'] ) : ?>
											<tr>
												<td><?php _e('- od tega nasičene maščobe', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['mascobne_kisline']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['ogljikovi_hidrati'] ) : ?>
											<tr>
												<td><?php _e('Ogljikovi hidrati', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['ogljikovi_hidrati']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['sladkorji'] ) : ?>
											<tr>
												<td><?php _e('- od tega sladkorji', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['sladkorji']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['prehranska_vlaknine'] ) : ?>
											<tr>
												<td><?php _e('Prehranske vlaknine', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['prehranska_vlaknine']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['beljakovine'] ) : ?>
											<tr>
												<td><?php _e('Beljakovine', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['beljakovine']; ?></td>
											</tr>
										<?php endif; ?>

										<?php if ( $nutri['sol'] ) : ?>
											<tr>
												<td><?php _e('Sol', 'mlinotest'); ?></td>
												<td class="float-right"><?php echo $nutri['sol']; ?></td>
											</tr>
										<?php endif; ?>

									</table>
								</div>
							</div>
							<?php endif; ?>

							<?php if( !isset($_COOKIE["b2b"]) || !($_COOKIE["b2b"]) ): ?><?php else: ?>
							<div class="col-12 order-3">
								<div class="b2b-info">
									<h5><?php _e('B2B vsebine', 'mlinotest'); ?></h5>
									<p>Tukaj se prikaže b2b vsebina, ki se vnese skozi CMS.</p>
								</div>
							</div>
							<?php endif; ?>


						</div>

					</article>

					<!-- izdelek horizontalen layout -->
					<?php else : ?>

					<article class="single-izdelek izdelek-horizontal" id="<?php echo $p['izdelek']->post_name; ?>">
						<div class="row izdelki-list-single">

							<?php
							$thumb = get_the_post_thumbnail($p['izdelek']->ID, '', array("class" => "img-fluid main-product-img", 'data-action' => 'zoom'));
							?>

							<div class="col-lg-6 col-12 text-center text-md-left" style="margin-bottom: 32px;">
								<div class="izdelki-img-wrapper">
									<?php echo $thumb; ?>
									<?php if( ICL_LANGUAGE_CODE=='sl' ) : ?>
										<?php if($badge) : ?>
											<img class="product-badge" src="<?php echo $badge['url']; ?>" alt="<?php echo $badge['url']; ?>">
										<?php endif; ?>
										<?php if($badge2) : ?>
											<img class="product-badge product-badge2" src="<?php echo $badge2['url']; ?>" alt="<?php echo $badge2['url']; ?>">
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>

							<?php
							$nutri = get_field('hranilne_vrednosti', $p['izdelek']->ID);
							?>

							<div class="col-12 text-center text-md-left">
								<h3 class="entry-title"><?php echo $p['izdelek']->post_title; ?></h3>
								<p><?php echo $p['izdelek']->post_content; ?></p>
								<div class="products-btn-wrapper">

									<?php if($nutri) : ?>
									<a class="btn btn-primary js-nutritional-info-btn" href="#"><?php _e('Hranilna vrednost', 'mlinotest'); ?></a>
									<?php endif; ?>

									<?php if( ICL_LANGUAGE_CODE=='sl' ) : ?>
										<?php if($url_recepti) : ?>
										<a class="btn btn-secondary"  href="<?php echo get_post_type_archive_link( 'recepti' ) . $url_recepti; ?>"><?php _e('Recepti', 'mlinotest'); ?></a>
										<?php endif; ?>
									<?php endif; ?>

								</div>
							</div>


							<?php if($nutri) : ?>
								<div class="col-12 order-3">
									<div class="nutritional-info">
										<h5><?php _e('Povprečna hranilna vrednost na 100 g', 'mlinotest'); ?></h5>
										<table>

											<?php if ( $nutri['energijska_vrednost'] ) : ?>
												<tr>
													<td><?php _e('Energijska vrednost', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['energijska_vrednost']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['mascobe'] ) : ?>
												<tr>
													<td><?php _e('Maščobe', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['mascobe']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['mascobne_kisline'] ) : ?>
												<tr>
													<td><?php _e('- od tega nasičene maščobe', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['mascobne_kisline']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['ogljikovi_hidrati'] ) : ?>
												<tr>
													<td><?php _e('Ogljikovi hidrati', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['ogljikovi_hidrati']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['sladkorji'] ) : ?>
												<tr>
													<td><?php _e('- od tega sladkorji', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['sladkorji']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['prehranska_vlaknine'] ) : ?>
												<tr>
													<td><?php _e('Prehranske vlaknine', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['prehranska_vlaknine']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['beljakovine'] ) : ?>
												<tr>
													<td><?php _e('Beljakovine', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['beljakovine']; ?></td>
												</tr>
											<?php endif; ?>

											<?php if ( $nutri['sol'] ) : ?>
												<tr>
													<td><?php _e('Sol', 'mlinotest'); ?></td>
													<td class="float-right"><?php echo $nutri['sol']; ?></td>
												</tr>
											<?php endif; ?>

										</table>
									</div>
								</div>
								<?php endif; ?>

						</div>
					</article>

					<?php endif; ?>


					<?php $j++; endforeach; ?>
				</div>

			</div>
	</div>
</div>

<?php get_footer(); ?>
