<?php
/**
 * Template Name: Landing
 *
 */
get_header();

?>

<style>
  .landing-link img:hover {
	  opacity: 0.7;
	  transition: 0.3s;
	  display: block;
  }
  
  .landing-titles {
	  font-weight: bold;
	  display: block;
	  font-size: 20px;
  }
  
  #landing {
	  height: 372px;  background: url('/wp-content/themes/mlinotest/img/banner.jpg');     background-position-x: center;
	  background-repeat: no-repeat;
	  /*background-size: contain;*/
  }
  
  @media only screen and (max-width: 667px) {
    #landing {
        /*height: 372px;  */
		background: url('/wp-content/themes/mlinotest/img/banner-m.jpg');     background-position-x: center;
		background-repeat: no-repeat;
		background-size: cover;
    }
	
	.wrapper {
		padding-top: 1.5rem !important;
	}
}
  
</style>

<section id="landing" style="" id="home-banner-main">
	<?php
	$bannerLink = get_field('banner_link', apply_filters( 'wpml_object_id', 5, 'post' ));
	$link2 = $bannerLink['url'];
	?>

		<div class="container">
			<div class="row justify-content-end align-items-center">
				
				<!-- <div class="col-6 d-none d-lg-block title-image">
					<img src="<?php echo get_template_directory_uri() . '/img/banner.jpg'; ?>" alt="">
				</div> -->
				
			</div>
		</div>
		


</section>



<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="container">
		
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<?php the_content(); ?>
				</div>
			</div>
			
			<div style="margin-bottom: 60px;" class="row">
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ravioli-spinaca-in-ricotta-250-g"><img class="alignleft size-large wp-image-26911 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/šinaša-in-ricota.png" alt="" width="640" height="920" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#tortelini-pesto-bazilika-250-g"><img class="alignleft size-large wp-image-26913 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/pestobazilika.png" alt="" width="640" height="881" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#tortelini-pecenka-250-g"><img class="alignleft size-large wp-image-26909 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/pečenka.png" alt="" width="640" height="892" /></a>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ravioli-zlahtni-siri-250-g"><img class="alignleft size-large wp-image-26907 img-responsive" src="https://www.mlinotest.si/wp-content/themes/mlinotest/img/žlahtni-sir.png" alt="" width="640" height="866" /></a>
				</div>
			</div>
			
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/" style="color:white; cursor:pointer;" class="btn btn-primary">Podrobno o izdelkih</a>
				</div>
			</div>
			
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<h3 class="entry-title">Recepti, ki prebudijo brbončice najbolj zahtevnih.</h3>
				</div>
			</div>
			
			<div style="margin-bottom: 60px; padding-left: 10px; padding-right: 10px;"  class="row">
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/zlahtna-pasta-festa/"><img class="alignleft size-large wp-image-26911 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/žlahtna-pašta-fešta.jpg" alt="" width="640" height="920" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Žlahtna pašta fešta</p>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/prasicek-v-radicu/"><img class="alignleft size-large wp-image-26913 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/prašiček-v-radiču.jpg" alt="" width="640" height="881" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Prašiček v radiču</p>
				</div>
				<div class="col-sm-3">
					<a class="landing-link" href="https://www.mlinotest.si/recepti/pasta-popaj/"><img class="alignleft size-large wp-image-26909 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/pašta-popaj.jpg" alt="" width="640" height="892" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Pašta popaj</p>
				</div>
				<div class="col-sm-3">
					<a href="https://www.mlinotest.si/recepti/pasta-pod-oljko/" class="landing-link" ><img class="alignleft size-large wp-image-26907 img-responsive" src="https://www.mlinotest.si/wp-content/uploads/2018/10/pašta-pod-oljko-1.jpg" alt="" width="640" height="866" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Pašta pod oljko</p>
				</div>
			</div>
			
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/recepti/" style="color:white; cursor:pointer;" class="btn btn-primary" >Vsi recepti</a>
				</div>
			</div>
			
				<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.mlinotest.si/pastafesta/" target="_blank" style="color:white; cursor:pointer;" class="btn btn-primary">Namigni prijatelju</a>
				</div>
			</div>
			
		
	</div>


	
		
</div>
<br/><br/>
<?php get_footer(); ?>
