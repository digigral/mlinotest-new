<?php
/**
 * Template Name: B2B
 *
 */

get_header();

wp_enqueue_script( 'b2b-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/b2b-left-menu"); ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
				<?php the_content(); ?>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
