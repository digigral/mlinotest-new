<?php
/**
 * Template Name: Objave
 *
 */

get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
				</aside>
			</div>

            <?php
            $curYear = date("Y");
            $args1 = array(
                'post_type' => 'objave',
                'posts_per_page' => -1,
                'date_query' => array(
                    array(
                        'year'  => $curYear
                    ),
                ),
            );
            $query1 = new WP_Query( $args1 );
            
            
            $years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type ='objave' ORDER BY post_date DESC");

            ?>

            <div class="col-md-8 order-1 order-md-2">
                <h3 class="objave-title"><?php _e('Trenutne objave', 'mlinotest'); ?></h3>
                <ul class="archive-objave-list">
                <?php while ($query1->have_posts()) : $query1->the_post(); ?>
                    <li><span><?php echo get_the_date('d.m.Y'); ?></span><a href="<?php the_field('povezava_do_objave'); ?>"><?php the_title(); ?></a></li>
                    
                <?php endwhile; ?>
                </ul>

                <?php if(!empty($years)) : ?>
                <h3 class="objave-title-2"><?php _e('Arhiv objav', 'mlinotest'); ?></h3>
                <ul class="archive-objave-list">
                <?php foreach($years as $year) : ?>
                    <?php if($year == $curYear) : ?>
                    <li><a href="<?php echo get_home_url() . '/objave/' . $year;?>"><?php _e('trenutne objave', 'mlinotest'); ?></a></li>
                    <?php else : ?>
                    <li><a href="<?php echo get_home_url() . '/objave/' . $year;?>"><?php _e('za leto', 'mlinotest'); ?> <?php echo $year; ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>

		</div>
	</div>
</div>

<?php get_footer(); ?>