<?php
/**
 * Template Name: Trgovine - zemljevid
 *
 */

get_header();

wp_enqueue_script( 'maps_trgovine' );

$locations = mlinotest_get_all_trgovine_with_loc();
//d($locations);

$terms = get_trgovine_terms();
//d($terms);

?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">

		<div class="row">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h1><?php echo get_the_title(wp_get_post_parent_id(get_the_id())); ?></h1>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside id="sidebar-top--js">
					<?php get_template_part("page-templates-parts/side/shops-left-menu"); ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">
				<section class="trgovine filters">
					<h2><?php _e('Zemljevid trgovin', 'mlinotest'); ?></h2>

					<div style="margin-bottom: 32px;"><?php the_content(); ?></div>

					<?php if($terms): ?>
						<div id="tip-poslovalnice">
							<span><?php _e('Tip poslovalnice:', 'mlinotest'); ?></span>
							<?php foreach ($terms as $t): ?>
								<?php
								$marker = $marker = get_field('trgovina_cat_marker', $t);
								?>
								<label class="check-wrapper"><img src="<?php echo $marker; ?>"><input value="<?php echo $t->slug ?>" id="<?php echo $t->slug ?>" type="checkbox" name="checkbox" checked class="trgovina-filter-button" ><span class="checkmark"></span><span><?php echo $t->name; ?></span></label>
							<?php endforeach; ?>
							</div>
					<?php endif; ?>

				</section>
				<section class="trgovine-map">
					<div style="height: 550px; width: 100%;" id="map"></div> <!-- TO DO: put this in sass, remove from inline -->
				</section>

				<div class="resp-iframe-container video-maps">
					<iframe class="resp-iframe" src="https://www.facebook.com/plugins/video.php?href=https://www.facebook.com/mlinotest/videos/1662654113781310/&;show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
				</div>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>
