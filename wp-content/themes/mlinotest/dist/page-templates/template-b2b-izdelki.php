<?php
/**
 * Template Name: B2B Izdelki
 *
 */

get_header();

wp_enqueue_script( 'b2b-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/b2b-left-menu"); ?>
				</aside>
			</div>

			<div class="col-md-8 order-1 order-md-2">

                <?php the_content(); ?>

				<?php if(   !isset($_COOKIE["b2b"]) || !($_COOKIE["b2b"]) ):   ?>

				<fieldset class="kontakt-obrazec">
					<form id="kontaktirajte-nas">

						<div class="row">
							<div class="col-lg-6">
								<input type="text" name="your-name" placeholder="<?php _e('Ime *', 'mlinotest'); ?>">
							</div>
							<div class="col-lg-6">
								<input type="text" name="your-lastname" placeholder="<?php _e('Priimek *', 'mlinotest'); ?>">
							</div>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<input type="email" name="your-email" placeholder="<?php _e('E-naslov *', 'mlinotest'); ?>">
							</div>
							<div class="col-lg-6">
								<input type="text" name="your-company" placeholder="<?php _e('Podjetje *', 'mlinotest'); ?>">
							</div>
						</div>

						<button class="btn btn-submit" id="form-submit-btn" type="submit"><?php _e('Prijavite se za dostop do B2B vsebin', 'mlinotest'); ?></button>
					</form>
					<div class="form-submit-msg"></div>
				</fieldset>

			  <?php else: ?>
					<fieldset class="kontakt-obrazec">
						<form id="kontaktirajte-nas">
							<div class="row">
								<div class="col-lg-12">
									<p class="mb-0" style="color: white;"><?php _e('Vi ste prijavljeni v B2B vsebine.', 'mlinotest'); ?></p>
								</div>
							</div>
						</form>
					</fieldset>
				<?php endif; ?>

			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>
