<?php
/**
 * Template Name: Kontakt
 *
 */
get_header();

wp_enqueue_script( 'contact-scripts' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="container">
		
			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<div class="naslov-wrapper">
						<h1 class="recepti-top-naslov"><?php _e('Kontakt', 'mlinotest'); ?></h1>
					</div>
					<p><?php _e('Kontaktirate nas lahko preko spodnjega obrazca ali nas pokličete na telefonsko številko', 'mlinotest'); ?></p>
				</div>
			</div>
			
			<div class="row contact-main-wrapper">
				<div class="col-lg-6 col-12 kontaktni-podatki">
					<img class="img-fluid" src="<?php echo get_template_directory_uri() . '/img/header-logo-mlinotest.png'; ?>" alt="">
					<ul>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Tovarniška cesta 14, 5270 Ajdovščina</span></li>
						<li><i class="fa fa-phone" aria-hidden="true"></i><span>080 22 72</span></li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i><span>info@mlinotest.si</span></li>
						<li style="margin-top: 20px;"><?php _e('Sledite nam:', 'mlinotest'); ?></li>
					</ul>	
					<div class="kontakt-social-icons">
						<?php get_template_part("page-templates-parts/social-icons"); ?>
					</div>
				</div>
				<div class="col-lg-6 col-12">
					<fieldset class="kontakt-obrazec">
						<form id="kontaktirajte-nas">
							<div class="row">
								<div class="col-lg-6">
									<input type="text" name="your-name" placeholder="<?php _e('Ime *', 'mlinotest'); ?>">	

								</div>
								<div class="col-lg-6">
									<input type="email" name="your-email" placeholder="<?php _e('E-pošta *', 'mlinotest'); ?>">
								</div>
								<div class="col-12">
									<textarea name="your-message" placeholder="<?php _e('Sporočilo *', 'mlinotest'); ?>"></textarea>
								</div>
							</div>
							<button class="btn btn-submit" id="form-submit-btn" type="submit"><?php _e('Pošlji', 'mlinotest'); ?></button>
						</form>
						<div class="form-submit-msg"></div>
					</fieldset>
				</div>
			</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12" style="padding: 0;">
				<div style="height: 400px; width: 100%;" id="map"></div>
			</div>
		</div>
	</div>
			
</div>

<?php get_footer(); ?>
