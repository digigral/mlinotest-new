<?php
/**
 * Template Name: Landing5
 *
 */
get_header();

?>

<style>
  .landing-link img:hover {
	  opacity: 0.7;
	  transition: 0.3s;
	  display: block;
  }

  .landing-titles {
	  font-weight: bold;
	  display: block;
	  font-size: 20px;
  }

  #landing {
	  height: 372px;  background: url('http://www.mlinotest.si/wp-content/uploads/2019/09/Mlinotest_web_header.jpg');     background-position-x: center;
	  background-repeat: no-repeat;
	  /*background-size: contain;*/
  }

  @media only screen and (max-width: 667px) {
    #landing {
        /*height: 372px;  */
		background: url('http://www.mlinotest.si/wp-content/uploads/2019/09/Mlinotest_web_cover_mobile.png');     background-position-x: center;
		background-repeat: no-repeat;
		background-size: cover;
    }

	.wrapper {
		padding-top: 1.5rem !important;
	}
}

</style>

<section id="landing" style="" id="home-banner-main">
	<?php
	$bannerLink = get_field('banner_link', apply_filters( 'wpml_object_id', 5, 'post' ));
	$link2 = $bannerLink['url'];
	?>

		<div class="container">
			<div class="row justify-content-end align-items-center">

				<!-- <div class="col-6 d-none d-lg-block title-image">
					<img src="<?php echo get_template_directory_uri() . '/img/banner.jpg'; ?>" alt="">
				</div> -->

			</div>
		</div>



</section>



<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="container">

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">

					<h2 class="text-center" style="color: #94100e;">Vitalni obrok v 3 minutah.</h2>
					<p>&nbsp;</p>
					<p style="text-align: center;">Naše nove sveže testenine bogatijo gurmanski nadevi in slovenska ajda, ter so prvi korak k vitalnosti. <br/>
Pripravljamo jih po sodobnih tehnoloških metodah, da ohranimo pristne okuse, brez dodatnih arom.</p>
					<div style="max-width: 700px; margin: 0 auto;">
						<div class="resp-iframe-container" style="position: relative; overflow: hidden; padding-top: 56.25%;">
							<iframe class="responsive-iframe" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border: 0;" src="https://www.youtube.com/embed/MMbCt3HAEAM" frameborder="0"  width="700" height="395"  frameborder="0" allowfullscreen="allowfullscreen"></iframe>
						</div>
					</div>
				</div>
			</div>
			<div style="margin-bottom: 60px;" class="row">
				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ajdovi-ravioli-gobe-250g"><img style="display: block;" class="aligncenter size-large wp-image-26911 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/06/ajdovi-ravijoli-gobe.png" alt="" width="255" height="411.78" /></a>
				</div>
				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/izdelki/divita/divita-njoki/#ajdovi-njoki-500g"><img style="display: block; height: 87%; width:65%;" class="aligncenter size-large wp-image-26909 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/06/ajdovi-njoki.png" alt="" width="255" height="411"  /></a>
				</div>

				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/#ajdovi-ravioli-trije-siri-250g"><img style="display: block;" class="aligncenter size-large wp-image-26913 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/06/ajdovi-ravijoli-siri-1.png" alt="" width="255" height="411.78"  /></a>
				</div>

			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/izdelki/divita/divita-sveze-polnjene-testenine/" style="color:white; cursor:pointer;" class="btn btn-primary">Podrobno o izdelkih</a>
				</div>
			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<h3 class="entry-title">Recepti, ki prebudijo brbončice najbolj zahtevnih.</h3>
				</div>
			</div>

			<div style="margin-bottom: 60px; padding-left: 10px; padding-right: 10px;"  class="row">
				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/recepti/ajda-s-kosarico-malin/"><img class="alignleft size-large wp-image-26911 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/05/ajda-s-košarico-malinR2.jpg" alt="" width="640" height="920" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Ajda s košarico malin</p>
				</div>
				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/recepti/ajda-gre-v-gozd/"><img class="alignleft size-large wp-image-26913 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/05/ajda-gre-v-gozdR1.jpg" alt="" width="640" height="881" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Ajda gre v gozd</p>
				</div>
				<div class="col-sm-4">
					<a class="landing-link" href="http://www.mlinotest.si/recepti/ajda-ala-carbonara/"><img class="alignleft size-large wp-image-26909 img-responsive" src="http://www.mlinotest.si/wp-content/uploads/2019/05/ajda-a_la-carbonarar1.jpg" alt="" width="640" height="892" /></a>
					<p style="font-size:7px; color: transparent; margin-bottom: 0 !important;">. </p>
					<p class="landing-titles text-center">Ajda a’la carbonara</p>
				</div>

			</div>

			<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.mlinotest.si/recepti/" style="color:white; cursor:pointer;" class="btn btn-primary" >Vsi recepti</a>
				</div>
			</div>

				<div class="row" style="margin-bottom: 48px;">
				<div class="col-12 text-center">
					<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.mlinotest.si/pastafesta/" target="_blank" style="color:white; cursor:pointer;" class="btn btn-primary">Namigni prijatelju</a>
				</div>
			</div>


	</div>




</div>
<br/><br/>
<?php get_footer(); ?>
