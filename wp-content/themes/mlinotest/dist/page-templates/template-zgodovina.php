<?php

/**
 * Template Name: Zgodovina
 *
 */

get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="naslov-wrapper text-center">
                    <h1><?php echo get_the_title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3 order-2 order-md-1">
                <aside>
                    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                        <?php get_template_part("page-templates-parts/side/company-left-menu-en"); ?>
                    <?php else : ?>
                        <?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
                    <?php endif; ?>
                </aside>
            </div>

            <div class="col-md-9 order-1 order-md-2 zgodovina">
                <div class="zgodovina--top">
                    <h2><?php echo get_field('zgodovina_naslov') ?></h2>
                    <p>
                        <?php echo get_field('zgodovina_tekst') ?>
                    </p>
                </div>
                <?php if (have_rows('letnice_repeater')) : ?>
                    <div class="timeline">
                    <?php while (have_rows('letnice_repeater')) : the_row(); ?>
                        <div class="timelineRow align-items-center justify-content-between">
                            <div class="timelineRow--year d-flex flex-column">
                                <div><?php the_sub_field('letnica'); ?></div>
                                <div><?php the_sub_field('letnica_text'); ?></div>
                            </div>
                            <div class="timelineRow--milestoneContainer">
                                <div class="timelineRow--milestone">
                                    <div class="timelineRow--milestone-image">
                                        <img src="<?php the_sub_field('letnica_slika'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <div class="brosura row">
                    <div class="col-md-6">
                        <img src="<?php echo get_field('brosura_slika') ?>"/>
                    </div>
                    <div class="col-md-6">
                        <h2><?php echo get_field('brosura_naslov') ?></h2>
                        <p>
                            <?php echo get_field('brosura_tekst') ?>
                        </p>
                        <a href="<?php echo get_field('brosura_povezava') ?>" class="btn btn-primary"><?php echo __('Preverite', 'mlinotest') ?></a>
                    </div>
                </div>

                <?php
                $thecontent = get_the_content();
                if (!empty($thecontent)) {
                    the_content();
                }
                ?>

            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>