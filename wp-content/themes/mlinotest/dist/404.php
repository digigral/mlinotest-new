<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header();

?>

<div class="container container-404">
		<div class="row align-items-center justify-content-center">
			<div class="col-sm-12 text-center">

				<h1><?php _e("Strani ni bilo mogoče najti", "mlinotest"); ?></h1>
				<a class="home-link-404" href="<?php echo home_url(); ?>"><?php _e("Na prvo stran", "mlinotest"); ?></a>

			</div>
		</div>
</div>

<?php get_footer(); ?>
