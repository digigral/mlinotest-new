<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

$cur_id = get_the_id();
$first = mlinotest_get_page_children( apply_filters( 'wpml_object_id', 7  ) );
$cur_parent = wp_get_post_parent_id( $cur_id );
?>


<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	
	<?php 
	$posttype = get_post_type();
	?>
	<?php if($posttype != 'izdelek') : ?>
	<?php the_title( sprintf( '<h6><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h6>' ); ?>

	<?php else : ?>

		<?php foreach ($first as $f): ?>
			
			<?php 
			$children = get_children( $f->ID);
			?>

			<?php foreach($children as $c) : ?>


				<?php if ( have_rows('izdelki_v_tej_kategoriji', $c->ID) ) : ?>
				
					<?php while( have_rows('izdelki_v_tej_kategoriji', $c->ID) ) : the_row(); ?>

						<?php 
						$izdelek = get_sub_field('izdelek', $c->ID);
						?>

						<?php if($cur_id == $izdelek->ID) : ?>
							<a href="<?php echo get_permalink( $c->ID ) . '#' . $izdelek->post_name; ?>">
								<h6><?php echo $izdelek->post_title; ?></h6>
							</a>
						<?php endif; ?>
				
					<?php endwhile; ?>
				
				<?php endif; ?>


				<!-- if products have subcategory (moke, testenine itd...) -->
				<?php 
				$children2 = get_children( $c->ID);
				?>
				<?php if(!empty($children2)) : ?>
					<?php foreach($children2 as $c2) : ?>
	

						<?php if ( have_rows('izdelki_v_tej_kategoriji', $c2->ID) ) : ?>
				
							<?php while( have_rows('izdelki_v_tej_kategoriji', $c2->ID) ) : the_row(); ?>

								<?php 
								$izdelek2 = get_sub_field('izdelek', $c2->ID);
								?>

								<?php if($cur_id == $izdelek2->ID) : ?>
									<a href="<?php echo get_permalink( $c2->ID ) . '#' . $izdelek2->post_name; ?>">
										<h6><?php echo $izdelek2->post_title; ?></h6>
									</a>
								<?php endif; ?>
						
							<?php endwhile; ?>
						
						<?php endif; ?>


					<?php endforeach; ?>
				<?php endif; ?>
				<!-- if products have subcategory (moke, testenine itd...) -->


			<?php endforeach; ?>	
			
		<?php endforeach; ?>

	<?php endif; ?>

</article><!-- #post-## -->
