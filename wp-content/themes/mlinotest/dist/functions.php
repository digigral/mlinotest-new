<?php
require get_template_directory() . '/inc/setup.php';
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/security.php';
require get_template_directory() . '/inc/enqueue.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/pagination.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/custom-comments.php';
require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/bootstrap-wp-navwalker.php';
require get_template_directory() . '/inc/woocommerce.php';
require get_template_directory() . '/inc/editor.php';
require get_template_directory() . '/inc/cpts.php';
require get_template_directory() . '/inc/helpers.php';
require get_template_directory() . '/inc/filters.php';
require get_template_directory() . '/inc/ajax.php';
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/wpml.php';
require get_template_directory() . '/inc/settings.php';

require 'sendgrid-php.php';

add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}

error_reporting(E_ERROR | E_PARSE);


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDESVFE1JRqjKkcuGk7summnVc9X0QXhHM');
}

add_action('acf/init', 'my_acf_init');


add_action('send_headers', function(){
    // Enforce the use of HTTPS
	header("Strict-Transport-Security: max-age=31536000; includeSubDomains");
	// Prevent Clickjacking
	header("X-Frame-Options: SAMEORIGIN");
	// Prevent XSS Attack
	//header("Content-Security-Policy: default-src 'self';"); // FF 23+ Chrome 25+ Safari 7+ Opera 19+
	//header("X-Content-Security-Policy: default-src 'self';"); // IE 10+
	// Block Access If XSS Attack Is Suspected
	header("X-XSS-Protection: 1; mode=block");
	// Prevent MIME-Type Sniffing
	header("X-Content-Type-Options: nosniff");
	// Referrer Policy
	header("Referrer-Policy: no-referrer-when-downgrade");
}, 1);
