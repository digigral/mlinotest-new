<?php
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper wrapper-subpages has-fixed-element" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		
		<div class="row">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h1>Zbirka receptov</h1>
				</div>
			</div>
		</div>

		<div class="row">
		
			<div class="col-md-4 sidebar-column">
				<aside>
					<?php get_template_part( 'page-templates-parts/side/recepti-left-menu'); ?>
				</aside>
			</div>
			
			<div class="col-md-8">
			
				<main class="site-main" id="main">
					

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'single' ); ?>

					<?php endwhile; ?>
					

				</main><!-- #main -->

			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>