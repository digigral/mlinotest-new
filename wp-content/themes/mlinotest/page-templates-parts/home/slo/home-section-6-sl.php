<?php
    $args1 = array(
        'post_type' => 'nasveti',
        'posts_per_page' => 2,
        'orderby' => 'rand',
        'order' => 'DESC'
    );
    $query1 = new WP_Query( $args1 );
?>
<?php if($query1->have_posts()) : ?>
<section class="home-izbrano">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if($fields['izbrano_za_vas_naslov']) :?>
                    <h1 class="text-center"><?php echo $fields['izbrano_za_vas_naslov']; ?></h1>
                <?php endif; ?>
                <?php if($fields['izbrano_za_vas_podnaslov']) :?>
                    <p class="text-center">
                        <?php echo $fields['izbrano_za_vas_podnaslov'] ?>
                    </p>
                <?php endif; ?>	
            </div>
            <?php while ($query1->have_posts()) : $query1->the_post(); ?>
                <div class="col-md-6 white-grid-item-wrapper">
                    <div class="grid-item-image">
                        <?php the_post_thumbnail('large', array('class' => 'img-fluid')); ?>
                    </div>
                    <div class="grid-item-content">
                        <h2><?php the_title(); ?></h2>
                        <?php
                        $cont = get_the_content();
                        $trimmed = strip_tags(mb_strimwidth($cont, 0, 200, '...'));
                        ?>
                        <p class="d-none d-lg-block"><?php echo $trimmed; ?></p>	
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Preverite', 'mlinotest'); ?></a>
                    </div>          
                </div>
		    <?php endwhile; ?>
            
        </div>
    </div>
</section>
<?php endif; ?>
