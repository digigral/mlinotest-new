<?php
    $o_druzbi_naslov = get_field('o_druzbi_naslov');
    $o_druzbi_podnaslov = get_field('o_druzbi_podnaslov');
    $o_druzbi_tekst = get_field('o_druzbi_tekst');
    $o_druzbi_slika = get_field('o_druzbi_slika');
    $o_druzbi_povezava = get_field('o_druzbi_povezava');

    $b2b_naslov = get_field('b2b_naslov');
    $b2b_podnaslov = get_field('b2b_podnaslov');
    $b2b_tekst = get_field('b2b_tekst');
    $b2b_slika = get_field('b2b_slika');
    $b2b_povezava = get_field('b2b_povezava');

    $kariera_naslov = get_field('kariera_naslov');
    $kariera_podnaslov = get_field('kariera_podnaslov');
    $kariera_tekst = get_field('kariera_tekst');
    $kariera_slika = get_field('kariera_slika');
    $kariera_povezava = get_field('kariera_povezava');
?>

<section id="home-about" class="home-about <?php echo get_locale(); ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12 about-us">
                <img src="<?php echo $o_druzbi_slika ?>" />
                <div>
                    <h1><?php echo $o_druzbi_naslov ?></h1>
                    <h2><?php echo $o_druzbi_podnaslov ?></h2>
                    <p><?php echo $o_druzbi_tekst ?></p>
                    <a class="btn btn-primary" href="<?php echo $o_druzbi_povezava ?>">
                        <?php echo __('PREVERITE','mlinotest') ?>
                    </a>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 b2b white-grid-item-wrapper">
                <div class="grid-item-image" style="background: url(<?php echo $b2b_slika ?>);"></div>
                <div>
                    <h1><?php echo $b2b_naslov ?></h1>
                    <h2><?php echo $b2b_podnaslov ?></h2>
                    <p><?php echo $b2b_tekst ?></p>
                    <a class="btn btn-primary" href="<?php echo $b2b_povezava ?>">
                        <?= __('PREVERITE','mlinotest'); ?>
                    </a>                
                </div>
            </div>
            <div class="col-md-12 col-lg-6 kariera white-grid-item-wrapper">
                <div class="grid-item-image" style="background: url(<?php echo $kariera_slika ?>);"></div>
                <div>
                    <h1><?php echo $kariera_naslov ?></h1>
                    <h2><?php echo $kariera_podnaslov ?></h2>
                    <p><?php echo $kariera_tekst ?></p>
                    <a class="btn btn-primary" href="<?php echo $kariera_povezava ?>">
                        <?= __('PREVERITE','mlinotest'); ?>
                    </a>                
                </div>    
            </div>
        </div>
    </div>
</section>