<?php
$recept = get_random_featured_recepti();
?>

<section id="home-recepti">
	<div class="container">
		<div class="row justify-content-center js-recept-featured-wrapper">
			<div class="col-lg-6 js-recept-content">
                <div>
					<img class="dinner-icon" src="<?php echo get_template_directory_uri() . '/img/dinner.svg'; ?>" alt="">
                    <h1>
						<?php echo __('RECEPTI ZA VAS','mlinotest') ?>
					</h1>
                    <h2 class="text-center text-lg-left"><?php echo $recept['title']; ?></h2>
                    <p class="d-none d-lg-block"><?php echo $recept['content']; ?></p>

                    <?php
                    $pozicija = $recept['img_produkta_pos'];
                    if($pozicija == 'Pod tekstom') :
                    ?>
                        <?php if($recept['img_product_url']) : ?>
                        <div class="product-image-left-wrapper">
                            <a class="d-none d-lg-inline-block" href="<?php echo $recept['link_featured_prod']; ?>">
                                <img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
                            </a>
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <div class="link-wrapper d-none d-lg-block">
                        <a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a>
                    </div>
                </div>
			</div>
			<div class="col-lg-6 col-12 text-center text-lg-left">

				<?php if($recept['img_main_url']) : ?>
				<div class="rotate-img-wrapper">
					<img class="img-fluid recipe-image recipe-image-rotate" src="<?php echo $recept['img_main_url']; ?>" alt="<?php echo $recept['img_main_alt']; ?>">
				</div>
				<?php endif; ?>

				<?php if($pozicija != 'Pod tekstom') : ?>
					<?php if($recept['img_product_url']) : ?>
					<a class="product-image-link" href="<?php echo $recept['link_featured_prod']; ?>">
						<img class="product-image img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
					</a>
					<?php endif; ?>
				<?php endif; ?>

				<?php
				if($pozicija == 'Pod tekstom') :
				?>
					<?php if($recept['img_product_url']) : ?>
					<div class="product-image-left-wrapper">
						<a class="d-inline-block d-lg-none" href="<?php echo $recept['link_featured_prod']; ?>">
							<img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
						</a>
					</div>
					<?php endif; ?>
				<?php endif; ?>

				<div class="link-wrapper d-block d-lg-none">
					<a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a><br>
				</div>
			</div>
		</div>
	</div>
</section>