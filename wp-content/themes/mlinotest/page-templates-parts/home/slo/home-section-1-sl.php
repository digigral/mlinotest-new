<?php if ( have_rows('slider_repeater') ) : ?>
    <section id="home-banner-main" class="homepage-slider">
        <?php $i=1; while( have_rows('slider_repeater') ) : the_row(); ?>
                <?php 
                    $slider_slika = get_sub_field('slider_slika');
                    $slider_slika_mobile = get_sub_field('slider_slika_mobile');
                    $slider_tekst = get_sub_field('slider_tekst');
                    $slider_povezava = get_sub_field('slider_povezava');
                ?>
                <div class="d-flex align-items-center text-center">
                    <?php if($slider_slika): ?>
                        <img class="d-none d-sm-block" src="<?php echo $slider_slika ?>" />
                    <?php endif; ?>  
                    <?php if($slider_slika_mobile): ?>
                        <img class="d-block d-sm-none" src="<?php echo $slider_slika_mobile ?>" />
                    <?php else: ?>  
                        <img src="<?php echo $slider_slika ?>" />
                    <?php endif; ?>    
                    <div class="container d-flex align-items-center">
                        <div class="holder">
                            <?php if( $slider_tekst ): ?>
                                <h1 class="text-center"><?php echo $slider_tekst ?></h1>
                            <?php endif; ?>    
                            <?php if( $slider_povezava): ?>
                                <div class="text-center w-100">
                                    <a href="<?php echo $slider_povezava['url'] ?>" class="btn btn-primary"><?= __('Spoznaj', 'mlinotest');?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>