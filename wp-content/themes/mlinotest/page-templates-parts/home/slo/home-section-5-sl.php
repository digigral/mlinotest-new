<section class="home-novice">
    <div class="container">
        <div class="row">
         <?php if ( have_rows('storitve_repeater') ) : ?>
            <div class="col-md-12">
                <?php if($fields['novice_naslov']) :?>
                    <h1 class="text-center"><?php echo $fields['novice_naslov']; ?></h1>
                <?php endif; ?>
                <?php if($fields['novice_podnaslov']) :?>
                    <p class="text-center">
                        <?php echo $fields['novice_podnaslov'] ?>
                    </p>
                <?php endif; ?>	
            </div>
            <?php $i=1; while( have_rows('storitve_repeater') ) : the_row(); ?>
                <?php
                $storitevImg = get_sub_field('posamezna_storitev_slika');
                $storitevNaslov = get_sub_field('posamezna_storitev_naslov');
                $storitevTekst = get_sub_field('posamezna_storitev_tekst');
                $link = get_sub_field('posamezna_storitev_povezava');
                ?>
                <div class="col-md-12 col-lg-4 storitev-wrapper">
                    <?php if ( $storitevImg ) : ?>
                        <div style="background: url(<?php echo $storitevImg['url']; ?>);"></div>
                    <?php endif; ?>
                    <div>
                        <h2><?php echo $storitevNaslov ?></h2>
                        <p><?php echo $storitevTekst ?></p>
                        <a href="<?php echo $link['url']; ?>" class="btn btn-primary" target="<?= $link['target']?>"><?php echo __('Več o tem','mlinotest') ?></a>
                    </div>            
                </div>

            <?php $i++; endwhile; ?>

            <?php endif; ?>
        </div>
    </div>
</section>
