<section id="home-katalogi-objave">

	<div class="container">

		<div class="row" id="ne-spreglejte">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h2 class="text-center"><?php echo $fields['ne_spreglejte_naslov']; ?></h2>
					<img src="<?php echo get_template_directory_uri() . '/img/under-title-2.png'; ?>" alt="">
				</div>
			</div>

			<?php
			$img1 = $fields['ne_spreglejte_slika_1'];
			$link1 = $fields['ne_spreglejte_link_1'];
			$img2 = $fields['ne_spreglejte_slika_2'];
			$link2 = $fields['ne_spreglejte_link_2'];
			?>

			<div class="col-lg-6 text-center text-lg-left" >
				<a href="<?php echo $link1; ?>" <?php if(get_field('ne_spreglejte_link_1_nov_zavihek') == 'Odpri v novem zavihku') : echo 'target="_blank"'; endif; ?> class="img-overlay">
					<img class="img-fluid" src="<?php echo $img1['url']; ?>" alt="<?php echo $img1['alt']; ?>">
				</a>
			</div>
			<div class="col-lg-6 wrapper-catalog-img text-center text-lg-left">
				<a href="<?php echo $link2; ?>" <?php if(get_field('ne_spreglejte_link_2_nov_zavihek') == 'Odpri v novem zavihku') : echo 'target="_blank"'; endif; ?> class="img-overlay">
					<img class="img-fluid" src="<?php echo $img2['url'];  ?>" alt="<?php echo $img2['alt'];  ?>">
					<img class="img-fluid zadnja" src="<?php echo get_template_directory_uri() . '/img/home-banner-right-separate2.png'; ?>" alt="">
				</a>
			</div>
		</div>

		<div class="row" id="druzite-se">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h2><?php echo $fields['aktualno_s_facebooka_naslov']; ?></h2>
					<img src="<?php echo get_template_directory_uri() . '/img/under-title-1.png'; ?>" alt="">
				</div>
			</div>

			<?php
			$post_num = 3;
			$fb_page_id = 'mlinotest';
			$fb_access_token = '301084360403891|2cSaJErRGdP8eojVW7e7fCf-6I0';
			$url = 'https://graph.facebook.com/' . $fb_page_id . '/posts?fields=link,message,full_picture&access_token='. $fb_access_token . '&limit=' . $post_num;

			// cache facebook data
			$cache = get_template_directory() . '/facebook-data.json';

			if(file_exists($cache) && filemtime($cache) > time() - 60*60) :
				$api_response = file_get_contents($cache);
			else :
				$response = wp_remote_get( esc_url_raw( $url ) );
				$api_response = wp_remote_retrieve_body( $response );
    		file_put_contents($cache, $api_response);
			endif;

			$decoded = json_decode($api_response, true);
			$response_data = $decoded['data'];
			$fb_data = (array) $response_data;

			foreach($fb_data as $row) :
				$full_picture = $row['full_picture'];
				$link = $row['link'];
				if (!empty($row['message'])) { $message = htmlentities($row['message']); } else { $message = ''; };
				?>

				<div class="col-xl-4 col-lg-6 col-md-6 col-12 text-center text-md-left">
					<a class="img-overlay" href="<?php echo $link; ?>" target="_blank">
						<div class="social-border" style="background:url(<?php echo get_template_directory_uri() . '/img/borders-social.png'; ?>) no-repeat center; background-size: 100%;">
							<img class="img-fluid" src="<?= $full_picture; ?>" alt="">
						</div>
					</a>
					<?php
					$trimmedMsg = wp_trim_words( $message, $num_words = 35, $more = null );
					?>
					<p class="text-left fb-message"><?php echo $trimmedMsg; ?></p>
				</div>

			<?php endforeach; ?>

			<div class="col-12 text-center">
					<a href="<?php echo $fields['facebook_btn_link']; ?>" target="_blank" class="link-more"><?php echo $fields['facebook_btn_text']; ?></a>
			</div>
		</div>

	</div>
</section>
