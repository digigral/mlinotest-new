<section id="home-banner-main">
	<?php
	$bannerLink = get_field('banner_link', apply_filters( 'wpml_object_id', 5, 'post' ));
	$link2 = $bannerLink['url'];
	?>

	<?php if($link2) : ?>
	<a href="<?php echo $link2; ?>">
	<?php endif; ?>

		<div class="container">
			<div class="row justify-content-end align-items-center">
				
				<!-- <div class="col-6 d-none d-lg-block title-image">
					<img src="<?php echo get_template_directory_uri() . '/img/banner-top-title.png'; ?>" alt="">
				</div> -->
				
			</div>
		</div>
		
	<?php if($link2) : ?>
	</a>
	<?php endif; ?>

</section>
