<?php
$recept = get_random_featured_recepti();
//d($recept);
?>

<section id="home-recepti">
	<div class="container">

		<div class="row justify-content-center">
			<div class="col-12 d-block">

				<h2 class="text-center heading-light-red margin-none"><?php _e('Dobrodošli za našo mizo', 'mlinotest'); ?></h2>
			</div>
			<div class="col-lg-9 col-12 text-center">

				<?php
				if($recept['predlog_link_title'] == 'zajtrk') :
				$okusen = 'okusen';
				$kakovosten = 'kakovosten';
				$other1 = 'kosilo';
				$other2 = 'večerjo';
				elseif($recept['predlog_link_title'] == 'kosilo'):
				$kakovosten = 'kakovostno';
				$okusen = 'okusno';
				$other1 = 'zajtrk';
				$other2 = 'večerjo';
				else :
				$kakovosten = 'kakovostno';
				$okusen = 'okusno';
				$other1 = 'zajtrk';
				$other2 = 'kosilo';
				endif;
				?>
				<div class="naslov-wrapper">
					<h1 class="recepti-top-naslov">Mlinotestovi predlogi<br> za <span class="js--change-text"><?php echo $kakovosten; ?> in <?php echo $okusen; ?></span> <a class="js-recepti-pop-btn" href="#" ><?php echo $recept['predlog_link_title']; ?></a>.
						<img class="img-fluid klikni-spremeni-img" src="<?php echo get_template_directory_uri() . '/img/klikni-in-spremeni.png'; ?>" alt="">
						<div class="recepti-popup">
							<p>
								<?php _e('Predlogi za', 'mlinotest'); ?>
								<a href="#" data-obrok="<?php echo $other1; ?>" class="popup-link"><?php echo $other1; ?></a>
								<?php _e('ali', 'mlinotest'); ?>
								<a href="#" data-obrok="<?php echo $other2; ?>" class="popup-link"><?php echo $other2; ?></a>
							</p>
						</div>
					</h1>
					<img src="<?php echo get_template_directory_uri() . '/img/under-title-1.png'; ?>" alt="">
				</div>
			</div>
		</div>
		<div class="row justify-content-center js-recept-featured-wrapper">
			<div class="col-lg-6">
				<h2 class="text-center text-lg-left"><?php echo $recept['title']; ?></h2>
				<p class="d-none d-lg-block"><?php echo $recept['content']; ?></p>

				<?php
				$pozicija = $recept['img_produkta_pos'];
				if($pozicija == 'Pod tekstom') :
				?>
					<?php if($recept['img_product_url']) : ?>
					<div class="product-image-left-wrapper">
						<a class="d-none d-lg-inline-block" href="<?php echo $recept['link_featured_prod']; ?>">
							<img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
						</a>
					</div>
					<?php endif; ?>
				<?php endif; ?>


				<div class="link-wrapper d-none d-lg-block">
					<a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a>
					<a href="#" class="share-recepti js-share-btn"><?php _e('Pošlji sebi ali prijatelju', 'mlinotest'); ?></a>

					<div class="social-share-links">
						<a target="_blank" href="<?php echo get_FB_social_share_buttons($recept['title'], $recept['link']); ?>"><img class="front-social-img" src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-fb.png" class="share-img"></a>
						<a target="_blank" href="<?php echo get_TW_social_share_buttons($recept['title'], $recept['link']); ?>"><img  class="front-social-img"src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-twitter.png" class="share-img"></a>
						<a id="share-mail" href="<?php echo get_mail_social_share_buttons($recept['title'], $recept['link']); ?>">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</a>
						<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>
					</div>

				</div>
			</div>
			<div class="col-lg-6 col-12 text-center text-lg-left">

				<?php if($recept['img_main_url']) : ?>
				<div class="rotate-img-wrapper">
					<img class="img-fluid recipe-image recipe-image-rotate" src="<?php echo $recept['img_main_url']; ?>" alt="<?php echo $recept['img_main_alt']; ?>">
				</div>
				<?php endif; ?>

				<?php if($pozicija != 'Pod tekstom') : ?>
					<?php if($recept['img_product_url']) : ?>
					<a class="product-image-link" href="<?php echo $recept['link_featured_prod']; ?>">
						<img class="product-image img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
					</a>
					<?php endif; ?>
				<?php endif; ?>

				<?php
				if($pozicija == 'Pod tekstom') :
				?>
					<?php if($recept['img_product_url']) : ?>
					<div class="product-image-left-wrapper">
						<a class="d-inline-block d-lg-none" href="<?php echo $recept['link_featured_prod']; ?>">
							<img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
						</a>
					</div>
					<?php endif; ?>
				<?php endif; ?>

				<div class="link-wrapper d-block d-lg-none">
					<a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a><br>
					<a href="#" class="share-recepti js-share-btn"><?php _e('Pošlji sebi ali prijatelju', 'mlinotest'); ?></a>

					<div class="social-share-links">
						<a target="_blank" href="<?php echo get_FB_social_share_buttons($recept['title'], $recept['link']); ?>"><img class="front-social-img" src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-fb.png" class="share-img"></a>
						<a target="_blank" href="<?php echo get_TW_social_share_buttons($recept['title'], $recept['link']); ?>"><img  class="front-social-img"src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-twitter.png" class="share-img"></a>
						<a id="share-mail" href="<?php echo get_mail_social_share_buttons($recept['title'], $recept['link']); ?>">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</a>
						<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
