<?php
    $facebook = get_field('facebook_link', 'option');
    $instagram = get_field('instagram_link', 'option');
    $youtube = get_field('youtube_link', 'option');
    $linkedin = get_field('linkedin_link', 'option');
    $mail = get_field('mail_link', 'option');
    $phone = get_field('phone_link', 'option');
?>      
<div class="social-links">
    <?php if($facebook): ?>
        <a href="<?php echo $facebook["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/fb_icon_m.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($instagram): ?>
        <a href="<?php echo $instagram["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/ig_icon_m.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($youtube): ?>
        <a href="<?php echo $youtube["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/yt_icon_m.png'; ?>" />
        </a>
    <?php endif; ?>
    <?php if($linkedin): ?>
        <a href="<?php echo $linkedin["url"] ?>" class="social-link">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/li_icon_m.png'; ?>" />
        </a>
    <?php endif; ?>   
    <?php if($mail): ?>
        <a href="<?php echo $mail["url"] ?>" class="social-link mail">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/mail_icon_m.png'; ?>" />
        </a>
    <?php endif; ?> 
    <?php if($phone): ?>
        <a href="tel:<?php echo $phone ?>" class="social-link phone">
            <img src="<?php echo get_template_directory_uri() . '/img/social-icons/phone_icon_m.png'; ?>" />
        </a>
    <?php endif; ?> 
</div>