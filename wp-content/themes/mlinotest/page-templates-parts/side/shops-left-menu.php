<?php
// get data here, and do the left menu regarding page structure, pages children
$subpages= mlinotest_get_page_children(15); // TO DO: make ID wml multilingual
$cur_id = get_the_id();
?>

<?php if($subpages): ?>
    
<div class="sidebar-navigation-wrapper active">
 <div class="sidebar-navigation">
 	<h3 class="text-left"><?php echo get_the_title(15); ?></h3>
  <ul>
  <?php foreach ($subpages as $s): ?>
      <li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
  <?php endforeach; ?>
  </ul>
 </div>
</div>
<?php endif; ?>

<?php
// get data here, and do the left menu regarding page structure, pages children
// get data here, and do the left menu regarding page structure, pages children
$subpages2= mlinotest_get_page_children(36262); // TO DO: make ID wml multilingual
?>

<?php if($subpages2): ?>    
<div class="sidebar-navigation-wrapper active">
 <div class="sidebar-navigation">
 	<h3 class="text-left"><?php echo get_the_title(36262); ?></h3>
  <ul>
  <?php foreach ($subpages2 as $s): ?>
      <li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
  <?php endforeach; ?>
  </ul>
 </div>
</div>
<?php endif; ?>