<?php
// get data here, and do the left menu regarding page structure, pages children
$subpages= mlinotest_get_page_children(apply_filters( 'wpml_object_id', 13, 'post' )); // TO DO: make ID wml multilingual
$cur_id = get_the_id();
// $subpages2 = mlinotest_get_page_children(apply_filters( 'wpml_object_id', 494, 'post' ));
// $subpages3 = array_merge($subpages, $subpages2);
// d($subpages3);
?>

<div class="sidebar-navigation-wrapper active">
	<?php if($subpages) : ?>
	<div class="sidebar-navigation">
		<h3 class="text-left"><?php echo get_the_title(apply_filters( 'wpml_object_id', 13, 'post' )); ?></h3>
		<ul class="dropdown-menu-sidebar">
		<?php foreach ($subpages as $s): ?>
			
			<?php
			$secondLevel = mlinotest_get_page_children( apply_filters( 'wpml_object_id', $s->ID  ) );
			$has_second = false;
			$second_active = false;
			if( count($secondLevel) > 0 ) {
				$has_second = true;
			}
			if($cur_id == $s->ID ) {
				$second_active = true;
			}
			if($secondLevel) {
				foreach($secondLevel as $s2) {
					if($cur_id == $s2->ID) {
						$second_active = true;
					}
				}
			}
			?>

<?php if($has_second) : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink(reset($secondLevel)->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php else : ?>
			<li class="<?php if($has_second && $second_active && $secondLevel) : echo 'top-level'; endif; ?> <?php if($second_active) : echo 'active'; endif; ?>"><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
			<?php endif; ?>

			<?php if($has_second && $second_active && $secondLevel) : ?>
				<ul class="fds <?php if($second_active == false ){ } else { echo "hidden"; } ?>" style="padding-top: 0;">
					<?php foreach($secondLevel as $key => $s) : ?>

						<?php
						$singleActive = false;  
						if( is_singular(array('mediji' )) ) : 
							$singleActive = true; 
						endif;
						?>

						<li style="padding-left: 25px;" <?php if($s->ID == $cur_id or $singleActive && $s->post_title == "Sporočila za javnost" ) : echo 'class="active"'; endif; ?>><a href="<?php echo get_the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
	
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

		<?php endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>
</div>