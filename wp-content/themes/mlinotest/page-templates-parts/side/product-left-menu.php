<?php
// get data here, and do the left menu regarding page structure, pages children
$cur_id = get_the_id();
$first = mlinotest_get_page_children( apply_filters( 'wpml_object_id', 7  ) ); // TO DO: make ID wml multilingual
$cur_parent = wp_get_post_parent_id( $cur_id );

?>

<?php if($first): ?>

  <?php $counter=1; foreach ($first as $f): ?>

  <?php $second = mlinotest_get_page_children($f->ID); ?> <!-- TO DO: make ID wml multilingual -->

  <?php
     $active_check = false;
     $link_to_third = false;
     foreach ($second as $s):
      if( $cur_id == $s->ID ){
          $active_check = true;
        }
        $third = mlinotest_get_page_children($s->ID);
        foreach ($third as $t) {
            if( $cur_id == $t->ID ){
              $active_check = true;
              $link_to_third = true;
            }
        }
     endforeach;
   ?>

   <div class="sidebar-navigation-wrapper <?php if($f->ID == $cur_id || $active_check == true) : echo 'active'; endif; ?>">

     <ul class="sidebar-navigation">

        <?php

        $first_item = $second;
        reset($first_item);
        $first_key = key($first_item);

        $show_item = true;

        $link_to_third_open = false;
        $perma = null;
        foreach ($second as $s):
              $third = mlinotest_get_page_children($s->ID);
              if(count($third) > 0) {
                $link_to_third_open = true;
                if(!$perma) {
                  $perma = get_the_permalink( reset($third) );
                }
              }
        endforeach;

        ?>

				<li class="product-group-logo"><a href="<?php if($link_to_third_open){  echo $perma; } else { echo get_the_permalink($first_key); }?>"><img class="logotip-znamke" src="<?php echo get_field("logotip_znamke", $f->ID)['url']; ?>"></a></li>

				<!-- second level -->


				<?php if($second): ?>

					<ul class="dropdown-menu-sidebar">
					<?php foreach ($second as $s): ?>

            <?php
            $has_third = false;
            $third_active = false;

            $third = mlinotest_get_page_children($s->ID);

            if( count($third) > 0 ) {
              $has_third = true;
            }

            if($cur_parent == $s->ID) {
              $third_active = true;
            }
            //d($third_active);

                  // d($has_third);
            ?>

							<li class="<?php if($has_third && $third_active) : echo 'top-level'; endif; ?> <?php if( $cur_id == $s->ID ){ echo 'active'; } ?>"><a href="<?php if($has_third) { echo get_the_permalink( reset($third)->ID); } else { echo get_the_permalink($s->ID); } ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>

              <!-- third level -->
              <?php if($has_third && $third && $third_active): ?>
              <ul class="fds <?php if($third_active == false){ } else { echo "hidden"; } ?>" style="padding-top: 0;">
                  <?php foreach ($third as $key => $t): ?>
                    <li style="padding-left: 25px;" class="<?php if( $cur_id == $t->ID ){ echo 'active'; } ?>"><a href="<?php echo get_the_permalink($t->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($t->ID); ?></a></li>
                  <?php endforeach; ?>
              </ul>
              <?php endif; ?>
              <!-- end third level -->


          <?php endforeach; ?>

					</ul>
				<?php endif; ?>

				<!-- second level -->

		</ul>

		<button class="dropdown-button" type="button"><i class="fa fa-angle-down" aria-hidden="true"></i></button>

	</div>

  <?php $counter++; endforeach; ?>

<?php endif; ?>
