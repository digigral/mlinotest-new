<?php
// get data here, and do the left menu regarding page structure, pages children
$subpages= mlinotest_get_page_children(apply_filters( 'wpml_object_id', 2850, 'post' )); // TO DO: make ID wml multilingual
$cur_id = get_the_id();
?>

<?php if($subpages): ?>
 <div class="sidebar-navigation">
 	<h3 class="text-left"><?php echo get_the_title(wp_get_post_parent_id($subpages->ID)); ?></h3>
		<ul>
		<?php foreach ($subpages as $s): ?>
				<li <?php if($s->ID == $cur_id) : echo 'class="active"'; endif; ?>><a href="<?php the_permalink($s->ID); ?>"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo get_the_title($s->ID); ?></a></li>
		<?php endforeach; ?>
		</ul>
 </div>
<?php endif; ?>