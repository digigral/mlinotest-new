<?php
get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		
		<div class="row">
			<div class="col-12">
				<div class="naslov-wrapper text-center">
					<h1><?php _e('Zbirka receptov', 'mlinotest'); ?></h1>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
		
			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part( 'page-templates-parts/side/company-left-menu'); ?>
				</aside>
			</div>
			
			<div class="col-md-8 order-1 order-md-2">
			
				<main class="site-main" id="main">
					
                    <ul class="archive-objave-list">
                    <?php while ( have_posts() ) : the_post(); ?>

                        <li><span><?php echo get_the_date('d. m. Y'); ?></span><a href="<?php the_field('povezava_do_objave'); ?>"><?php the_title(); ?></a></li>     

                    <?php endwhile; ?>
                    </ul>
                    <p class="back-to-objave"><span>&lt; </span><a href="<?php the_permalink(apply_filters( 'wpml_object_id', 496, 'post' )); ?>"><?php _e('Nazaj na trenutne objave', 'mlinotest'); ?></a></p>

				</main><!-- #main -->

			</div>

		</div><!-- #primary -->


	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
