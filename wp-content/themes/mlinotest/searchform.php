<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */

?>
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<div class="input-group">
		<input class="field main-search-input" id="s" name="s" type="text"
			placeholder="<?php esc_attr_e( 'Vnesite iskalni niz', 'mlinotest' ); ?>">
		<span class="input-group-btn">
			<input class="btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Iskanje', 'mlinotest' ); ?>">
		</span>
	</div>
</form>
