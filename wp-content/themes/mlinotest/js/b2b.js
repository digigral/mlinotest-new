(function ($, root, undefined) {

	$(function () {

	'use strict';

		function createCookie(name, value, days) {
			var expires = "";
			if (days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			}

			document.cookie = name + "=" + value + expires + "; path=/";
		}


		$("#kontaktirajte-nas").submit(function(e){

			 	var email    =  $("input[name=your-email]").val();
			 	var name     =  $("input[name=your-name]").val();
			 	var lastname =  $("input[name=your-lastname]").val();
				var company  =  $("input[name=your-company]").val();

				console.log(email);
				console.log(name);
				console.log(lastname);
				console.log(company);

				var $submitMsgCont = $('.form-submit-msg');
				var $emailInput = $("input[name=your-email]");
				var $nameInput = $("input[name=your-name]");
				var $lastnameInput = $("input[name=your-lastname]");
				var $companyInput = $("input[name=your-company]");

				var errorMsg1 = '<p class="alert alert-warning">' + translations.fields_missing_msg + '</p>';
				var errorMsg2 = '<p class="alert alert-warning">' + translations.email_missing_msg + '</p>';
				var errorMsg3 = '<p class="alert alert-warning">' + translations.name_missing_msg + '</p>';
				var errorMsg4 = '<p class="alert alert-warning">' + translations.message_missing_msg + '</p>';
				var successMsg = '<p class="alert alert-success">' + translations.submit_success_msg + '</p>';


				/*
				empty message container on submit before appending
				remove missing-field class
				*/
				function emptyContainer() {
					$submitMsgCont.empty();
					if($nameInput.hasClass('missing-field') ) {
						$nameInput.removeClass('missing-field');
					}
					if($emailInput.hasClass('missing-field') ) {
						$emailInput.removeClass('missing-field');
					}
					if($lastnameInput.hasClass('missing-field') ) {
						$lastnameInput.removeClass('missing-field');
					}
					if($lastnameInput.hasClass('missing-field') ) {
						$lastnameInput.removeClass('missing-field');
					}
				}

				/*
				input validation
				*/
				if (!email || !name || !company || !lastname) {

					emptyContainer();

					if(!email) {
						$emailInput.addClass('missing-field');
					}
					if (!name  ) {
						$nameInput.addClass('missing-field');
					}
					if (!company  ) {
						$companyInput.addClass('missing-field');
					}
					if (!lastname ) {
						$lastnameInput.addClass('missing-field');
					}

					$submitMsgCont.append(errorMsg1);

					return false;
				} else {

					$submitMsgCont.empty();
					$submitMsgCont.append(successMsg);
				}


				/*
				ajax to send the data
				*/

			 createCookie("b2b","true",3);

			 $.ajax({
				 url: ajaxurl,
				 data: {
					 'action': 'contact_form2',
					 'email': email,
					 'name': name,
					 'lastname': lastname,
					 'company': company
				 },
				 success: function (data) {
				 },
				 error: function (errorThrown) {
				 }
			 });

			 // end ajax call
			 return false;

		});

		//new-b2b-form 
		$("#kontaktirajte-nas-b2b").submit(function(e){

		   var email    =  $("input[name=your-email]").val();
		   var name     =  $("input[name=your-name]").val();
		   var phone 	 =  $("input[name=your-phone]").val();
		   var option   = $("#your-option").val();
		   var company  =  $("input[name=your-company]").val();
		   var subject  =  $("input[name=your-subject]").val();
		   var message  =  $("textarea[name=your-message]").val();

		   console.log(email);
		   console.log(name);
		   console.log(phone);
		   console.log(option);
		   console.log(company);
		   console.log(subject);
		   console.log(message);

		   var $submitMsgCont = $('.form-submit-msg');
		   var $emailInput = $("input[name=your-email]");
		   var $nameInput = $("input[name=your-name]");
		   var $optionInput = $("#your-option");
		   var $companyInput = $("input[name=your-company]");
		   var $subjectInput = $("input[name=your-subject]");
		   var $messageInput = $("input[name=your-message]");

		   var errorMsg1 = '<p class="alert alert-warning">' + translations.fields_missing_msg + '</p>';
		   var errorMsg2 = '<p class="alert alert-warning">' + translations.email_missing_msg + '</p>';
		   var errorMsg3 = '<p class="alert alert-warning">' + translations.name_missing_msg + '</p>';
		   var errorMsg4 = '<p class="alert alert-warning">' + translations.message_missing_msg + '</p>';
		   var successMsg = '<p class="alert alert-success">' + translations.submit_success_msg + '</p>';


		   /*
		   empty message container on submit before appending
		   remove missing-field class
		   */
		   function emptyContainer() {
			   $submitMsgCont.empty();
			   if($nameInput.hasClass('missing-field') ) {
				   $nameInput.removeClass('missing-field');
			   }
			   if($emailInput.hasClass('missing-field') ) {
				   $emailInput.removeClass('missing-field');
			   }
			   if($optionInput.hasClass('missing-field') ) {
				   $optionInput.removeClass('missing-field');
			   }
			   if($companyInput.hasClass('missing-field') ) {
				   $companyInput.removeClass('missing-field');
			   }
			   if($subjectInput.hasClass('missing-field') ) {
				   $subjectInput.removeClass('missing-field');
			   }
			   if($messageInput.hasClass('missing-field') ) {
				   $messageInput.removeClass('missing-field');
			   }
		   }

		   /*
		   input validation
		   */
		   if (!email || !name || !company || !option || !subject || !message ) {

			   emptyContainer();

			   if(!email) {
				   $emailInput.addClass('missing-field');
			   }
			   if (!name  ) {
				   $nameInput.addClass('missing-field');
			   }
			   if (!company  ) {
				   $companyInput.addClass('missing-field');
			   }
			   if (!option ) {
				   $optionInput.addClass('missing-field');
			   }
			   if (!subject ) {
				   $subjectInput.addClass('missing-field');
			   }
			   if (!message ) {
				$messageInput.addClass('missing-field');
			   }

			   $submitMsgCont.append(errorMsg1);

			   return false;
		   } else {

			   $submitMsgCont.empty();
			   $submitMsgCont.append(successMsg);
		   }


		   /*
		   ajax to send the data
		   */

			createCookie("b2b","true",3);

			$.ajax({
				url: ajaxurl,
				data: {
					'action': 'contact_form_b2b',
					'email': email,
					'name': name,
					'phone': phone,
					'option': option,
					'company': company,
					'subject': subject,
					'message': message
				},
				success: function (data) {
				},
				error: function (errorThrown) {
				}
			});

		// end ajax call
		return false;

   	});


   });

})(jQuery, this);
