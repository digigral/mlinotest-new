(function ($, root, undefined) {

	$(function () {

		'use strict';


		$(document).ready(function() {

				/*
				mobile menu show/hide on click
				*/
				$('#hamb-icon').on('click', function(){

					if($('.nav-mobile').hasClass('active')) {
						$('.nav-mobile').removeClass('active');
						$('#hamb-icon').removeClass('closed');
					} else {
						$('.nav-mobile').addClass('active');
						$('#hamb-icon').addClass('closed');
					}
				});


			/*
			show social share buttons on click
			*/
			var $socialLinks = $('.social-share-links');
			var $shareBtn = $('.js-share-btn');

			$shareBtn.on('click', function(e){
				e.preventDefault();
				$(this).hide();
				setTimeout(function(){
					$socialLinks.addClass('show');
				}, 200);
			});

			$('.js-social-share-close').on('click', function(e){
				e.preventDefault();
				$socialLinks.removeClass('show');
				setTimeout(function(){
					$shareBtn.show();
				}, 300);
			});

			/*
			show search popup on click
			*/
			$('#js--main-search-btn').on('click', function(e){
				e.preventDefault();
				$('#js--main-search-popup').addClass('open');
			});

			$('#js--main-search-btn-mobile').on('click', function(e){
				e.preventDefault();
				$('#js--main-search-popup').addClass('open');
			});

			$('#js--search-popup-close').on('click', function(e){
				e.preventDefault();
				$('#js--main-search-popup').removeClass('open');
			});


			/*
			init slick caorusel and settings
			*/
			$('.slick-sorodni-produkti').slick({
				autoplay: false,
				arrows: true,
				prevArrow: '<a class="arrow-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>',
				nextArrow: '<a class="arrow-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>',
				adaptiveHeight: true
			});


			/*
				back to top scroll on click
				*/
				$('#back-to-top').on('click', function() {
					$("html, body").animate({ scrollTop: 0 }, 500);
  				return false;
				});

				/*
				dropdown menu show on click
				*/

				$('.dropdown-button').on('click', function() {
					if ($(this).parent().hasClass('active')) {
						$(this).parent().removeClass('active');;
					} else {
						$(this).parent().addClass('active');
					}
				});

				$('.dropdown-heading').on('click', function(){
					console.log('wrodsfd');
					if ($(this).parent().parent().hasClass('active')) {
						$(this).parent().parent().removeClass('active');
					} else {
						$(this).parent().parent().addClass('active');
					}
				});


				/*
				show izdelki nutrition on click
				*/
				$('.js-nutritional-info-btn').on('click', function(ev) {
					ev.preventDefault();

					$(this).parent().parent().parent().find('.b2b-info').removeClass('open');

					var this_nutr = $(this).parent().parent().parent().find('.nutritional-info');
					if(this_nutr.hasClass('open')) {
						this_nutr.removeClass('open');
					} else {
						this_nutr.addClass('open');
					}
				});

					/*
					show b2b content on click
					*/
					$('.js-b2b-info-btn').on('click', function(ev) {
						ev.preventDefault();

						$(this).parent().parent().parent().find('.nutritional-info').removeClass('open');

						var this_nutr = $(this).parent().parent().parent().find('.b2b-info');
						if(this_nutr.hasClass('open')) {
							this_nutr.removeClass('open');
						} else {
							this_nutr.addClass('open');
						}
					});


				/*
				popup recepti
				*/
				// cache dom in vars
				var $receptiPopup = 	$('.recepti-popup');
				var $klikniSpremeniImg = $('.klikni-spremeni-img');
				var $popupLink = $('.popup-link');
				var $receptFeaturedWrapper = $('.js-recept-featured-wrapper');
				var $receptiPopupBtn = $('.js-recepti-pop-btn');

				$receptiPopupBtn.on('click', function(e){
					e.preventDefault();
					if($receptiPopup.hasClass('show')) {
						$receptiPopup.removeClass('show');
						if ($(window).width() > 992) {
							setTimeout(function(){
								$klikniSpremeniImg.css('display', 'initial');
							}, 1000);
						}
					} else {
						$receptiPopup.addClass('show');
						$klikniSpremeniImg.css('display', 'none');
					}
				});

				$popupLink.on('click', function(){
					if ($(window).width() > 992) {
						setTimeout(function(){
							$klikniSpremeniImg.css('display', 'initial');
						}, 1000);
					}
				});


				/*
				append data to featured recipe
				*/
				function appendFeaturedData (resp) {
					var to_append = '<div class="col-lg-6">';
					to_append += '<h2 class="text-center text-lg-left">' + resp.title + '</h2>';
					to_append += '<p class="d-none d-lg-block">' + resp.content + '</p>';
					
					if(resp.img_produkta_pos === 'Pod tekstom') {
						to_append += '<div class="product-image-left-wrapper">';
						if(resp.img_product_url) {
							if (resp.link_featured_prod) {
								to_append += '<a class="d-none d-lg-inline-block" href="' + resp.link_featured_prod + '">';
							} else {
								to_append += '<a class="d-none d-lg-inline-block" href="#">';
							}
							to_append += '<img class="product-image-left img-fluid" src="' + resp.img_product_url + '" alt="' + resp.img_product_alt + '">';
							to_append += '</a>';
						}
						to_append += '</div>';
					}

					to_append += '<div class="link-wrapper d-none d-lg-block">';
					to_append += '<a href="' + resp.link + '" class="btn btn-primary">Spoznaj</a>';
					to_append += '<a href="#" class="share-recepti js-share-btn">Pošlji sebi ali prijatelju</a>';
					to_append += '<div class="social-share-links">';
					to_append += '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' + encodeURI(resp.link) + '">';
					to_append += '<img class="front-social-img" src="' +  translations.templateUrl + '/img/recipe-single-share-fb.png' + '" class="share-img"></a>';
					to_append += '<a target="_blank" href="https://twitter.com/home?status=' + resp.title + '&amp;url=' + encodeURI(resp.link) +'&amp;via=">';
					to_append += '<img class="front-social-img" src="' +  translations.templateUrl + '/img/recipe-single-share-twitter.png' + '" class="share-img"></a>';
					to_append += '<a id="share-mail" href="mailto:?subject=' + resp.title + '&amp;body=Poglej si ta recept ' + encodeURI(resp.link) + '">';
					to_append += '<i class="fa fa-envelope" aria-hidden="true"></i></a>';
					to_append += '<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>';
					to_append += '</div></div></div>';
					to_append += '<div class="col-lg-6 col-12 text-center text-lg-left">';
					to_append += '<div class="rotate-img-wrapper">';
					if(resp.img_main_url) {
						to_append += '<img class="img-fluid recipe-image recipe-image-rotate" src="'+ resp.img_main_url + '" alt="' + resp.img_main_alt + '">';
					}
					to_append += '</div>';

					if(resp.img_produkta_pos !== 'Pod tekstom') {
						if(resp.img_product_url) {
							if (resp.link_featured_prod) {
								to_append += '<a class="product-image-link" href="' + resp.link_featured_prod + '">';
							} else {
								to_append += '<a class="product-image-link" href="#">';
							}
							to_append += '<img class="product-image img-fluid" src="' + resp.img_product_url + '" alt="' + resp.img_product_alt + '">';
							to_append += '</a>';
						}
					}

					if(resp.img_produkta_pos === 'Pod tekstom') {
						to_append += '<div class="product-image-left-wrapper">';
						if(resp.img_product_url) {
							if (resp.link_featured_prod) {
								to_append += '<a class="d-inline-block d-lg-none" href="' + resp.link_featured_prod + '">';
							} else {
								to_append += '<a class="d-inline-block d-lg-none" href="#">';
							}
							to_append += '<img class="product-image-left img-fluid" src="' + resp.img_product_url + '" alt="' + resp.img_product_alt + '">';
							to_append += '</a>';
						}
						to_append += '</div>';
					}

					to_append += '<div class="link-wrapper d-block d-lg-none">';
					to_append += '<a href="' + resp.link + '" class="btn btn-primary">Spoznaj</a><br>';
					to_append += '<a href="#" class="share-recepti js-share-btn">Pošlji sebi ali prijatelju</a>';
					to_append += '<div class="social-share-links">';
					to_append += '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=' + encodeURI(resp.link) + '">';
					to_append += '<img class="front-social-img" src="' +  translations.templateUrl + '/img/recipe-single-share-fb.png' + '" class="share-img"></a>';
					to_append += '<a target="_blank" href="https://twitter.com/home?status=' + resp.title + '&amp;url=' + encodeURI(resp.link) +'&amp;via=">';
					to_append += '<img class="front-social-img" src="' +  translations.templateUrl + '/img/recipe-single-share-twitter.png' + '" class="share-img"></a>';
					to_append += '<a id="share-mail" href="mailto:?subject=' + resp.title + '&amp;body=Poglej si ta recept ' + encodeURI(resp.link) + '">';
					to_append += '<i class="fa fa-envelope" aria-hidden="true"></i></a>';
					to_append += '<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>';
					to_append += '</div></div></div>';
					$('.js-recept-featured-wrapper').append(to_append);
				}


				/*
				delete data from recept featured wrapper
				*/
				function deleteFeaturedData() {
					$receptFeaturedWrapper.empty();
				}

				function changeObrokPopup(resp) {
					var $obrokTitleText = $('.js--change-text');

					if(resp.predlog_link_title === 'zajtrk') {
						$obrokTitleText.text('kakovosten in okusen ');
					} else {
						$obrokTitleText.text('kakovostno in okusno ');
					}

					var $popupLinkFirstChild = $('.popup-link:first-child');
					var $popupLinkLastChild = $('.popup-link:last-child');
					var $obrokBtn = $('.js-recepti-pop-btn');
					$obrokBtn.text(resp.predlog_link_title);
					$popupLinkFirstChild.text(resp.other1);
					$popupLinkFirstChild.attr('data-obrok', resp.other1);
					$popupLinkLastChild.text(resp.other2);
					$popupLinkLastChild.attr('data-obrok', resp.other2);
				}


				$popupLink.on('click', function(e){
					e.preventDefault();
					$receptiPopup.removeClass('show');
					var obrok = $(this).attr('data-obrok');

					jQuery.ajax({
						type : "post",
						dataType : "json",
						url : ajaxurl,
						data : {action: "recepti_featured", obrok: obrok},
						beforeSend: function() {
							deleteFeaturedData();
							$receptFeaturedWrapper.append('<div class="col-12 text-center"><div class="loader-spinner">Loading...</div></div>');
						},
						success: function(response) {
							deleteFeaturedData();
							changeObrokPopup(response);
							appendFeaturedData(response);
							$('.js-share-btn').on('click', function(e){
								e.preventDefault();
								$('.social-share-links').addClass('show');
								$(this).hide();
							});
							$('.js-social-share-close').on('click', function(e){
								e.preventDefault();
								$('.social-share-links').removeClass('show');
								$('.js-share-btn').show();
							});
						},
						error: function (errorThrown) {
							console.log(errorThrown);
							console.log("error");
						}
					})

				});



		});


	});



})(jQuery, this);
