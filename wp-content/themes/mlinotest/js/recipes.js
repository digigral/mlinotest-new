(function ($, root, undefined) {

  $(function() {

    'use strict';
     $(document).ready(function() {
      });

      // rotating

      var rotation = 0,
          scrollLoc = $(document).scrollTop();
      $(window).scroll(function() {
          var newLoc = $(document).scrollTop();
          var diff = scrollLoc - newLoc;
          rotation += diff, scrollLoc = newLoc;
          var rotationStr = "rotate(" + (rotation/15) + "deg)";
          $(".recipe-image-rotate").css({
              "-webkit-transform": rotationStr,
              "-moz-transform": rotationStr,
              "transform": rotationStr
          });
      });

      // end rotating


      // event on filter click, do staff, filter recipes by selections
			var $recipeWrapper = $(".recipe-list");

      $('.change-recipes').on('click', function(){

          var sel = getSelectedFilters();

          var sel1 = "";
          var sel2 = "";

          $.each(sel.kategorije, function( index, value ) {
              sel2 += value  + ",";
          });

          $.each(sel.obroki, function( index, value ) {
              sel1 += value  + ",";
          });

          sel1 = sel1.substring(0,sel1.length - 1);
          sel2 = sel2.substring(0,sel2.length - 1);

          jQuery.ajax({
              type : "post",
              dataType : "json",
              url : ajaxurl,
              data : { action: "recepti_tax" , sel1: sel1, sel2: sel2 },
							beforeSend: function() {
								$recipeWrapper.empty();
								$recipeWrapper.append('<div class="col-12 text-center"><div class="loader-spinner">Loading...</div></div>');
							},
              success: function(response) {
								$recipeWrapper.empty();

                $.each(response, function( index, value ) {

                  //console.log(value.img);

                  var to_append  = '<div class="col-lg-4 single-recipe">';
                      to_append +=												'<div class="recepti-featured-single-wrapper">';
                      to_append +=															'<a href="'+value.id+'">';
                      to_append +=																	'<div class="img-wrapper" style="background:url('+value.img+') center;"></div>';
                      to_append +=																	'<h3 class="entry-title"><span>&gt;</span>'+value.title+'</h3>';
                      to_append +=							 										'</a>';
                      to_append +=							           '</div>';
                      to_append +=														'</div>';

                  $recipeWrapper.append(to_append);

               });

                //  });

              },
              error: function (errorThrown) {
              }
          })



      });


      // end filter events

      function getSelectedFilters(){

        var kategorije = [];
        var obroki = [];



        $('.kategorije-filters .change-recipes').each(function () {
             var sThisVal = (this.checked ? $(this).val() : "");
             if(sThisVal != "") {
               kategorije.push($(this).data("slug"));
             }
        });

        $('.obroki-filters .change-recipes').each(function () {
             var sThisVal = (this.checked ? $(this).val() : "");
             if(sThisVal != "") {
               obroki.push($(this).data("slug"));
             }
        });

      var selection = {
            kategorije: kategorije,
            obroki: obroki
      }

      return selection;
      }


			/*
			recipes search
			*/
			var $searchInput = $('#recepti-search-form');
			var $searchSubmit = $('#recepti-search-submit');
			var $inputVal;
			var $recipeList = $('.recipe-list');
			var postTitles = [];
			var postLinks = [];
			var postThumbs = [];

			// match input value with post titles
			function match_input_values(response, val) {

				// empty post arrays on each input change
				postTitles = [];
				postLinks = [];
				postThumbs = [];

				// enter new data
				var reg = new RegExp(val, 'g');
				for(var i in response) {
					if(response[i].title.toLowerCase().match(reg)) {
						postTitles.push(response[i].title);
						postLinks.push(response[i].link);
						postThumbs.push(response[i].thumb);
					}
				}

			}


			// append html markup
			function append_recipe_markup(titles, links, thumbs) {
				for (var i in titles) {
					var toAppend = '<div class="col-lg-4 single-recipe"><div class="recepti-featured-single-wrapper">';
					toAppend += '<a class="img-overlay" href="' + links[i] + '">';
					toAppend += '<div class="img-wrapper" style="background:url(' + thumbs[i] + ') center;"></div>';
					toAppend += '</a>';
					toAppend += '<a href="' + links[i] + '">';
					toAppend += '<h3 class="entry-title"><span>&gt;</span>' + titles[i] + '</h3>';
					toAppend += '</a>';
					toAppend += '</div></div>';
					$recipeList.append(toAppend);
				}
			}


			// on input change reload html for new search parameters
			$searchInput.bind('input', function(){
				$inputVal = $(this).val().toLowerCase();

				// if($inputVal.length >= 2) {

					jQuery.ajax({
						type : "post",
						dataType : "json",
						url : ajaxurl,
						data : {action: "recipe_titles"},
						beforeSend: function() {
							$recipeList.empty();
							$recipeList.append('<div class="col-12 text-center"><div class="loader-spinner">Loading...</div></div>');
						},
						success: function(response) {
							$recipeList.empty();
							match_input_values(response, $inputVal);
							append_recipe_markup(postTitles, postLinks, postThumbs);
						},
						error: function (errorThrown) {
						}
					})

				// }

			});


			// on submit reload html for new search parameters
			$searchSubmit.on('click', function(e){
				e.preventDefault();
				$inputVal = $searchInput.val().toLowerCase();

				if($inputVal.length >= 2) {

					jQuery.ajax({
						type : "post",
						dataType : "json",
						url : ajaxurl,
						data : {action: "recipe_titles"},
						beforeSend: function() {
							$recipeList.empty();
							$recipeList.append('<div class="col-12 text-center"><div class="loader-spinner">Loading...</div></div>');
						},
						success: function(response) {
							$recipeList.empty();
							match_input_values(response, $inputVal);
							append_recipe_markup(postTitles, postLinks, postThumbs);
						},
						error: function (errorThrown) {

						}
					})

				}
			});

  });


})(jQuery, this);
