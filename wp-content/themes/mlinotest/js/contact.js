(function ($, root, undefined) {

	$(function () {

	'use strict';


		$("#kontaktirajte-nas").submit(function(e){

			 	var email    =  $("input[name=your-email]").val();
			 	var name     =  $("input[name=your-name]").val();
			 	var message  =  $("textarea").val();

				var $submitMsgCont = $('.form-submit-msg');
				var $emailInput = $("input[name=your-email]");
				var $nameInput = $("input[name=your-name]");
				var $messageInput = $("textarea");
				var errorMsg1 = '<p class="alert alert-warning">' + translations.fields_missing_msg + '</p>';
				var errorMsg2 = '<p class="alert alert-warning">' + translations.email_missing_msg + '</p>';
				var errorMsg3 = '<p class="alert alert-warning">' + translations.name_missing_msg + '</p>';
				var errorMsg4 = '<p class="alert alert-warning">' + translations.message_missing_msg + '</p>';
				var successMsg = '<p class="alert alert-success">' + translations.submit_success_msg + '</p>';


				/*
				empty message container on submit before appending
				remove missing-field class
				*/
				function emptyContainer() {
					$submitMsgCont.empty();
					if($nameInput.hasClass('missing-field') ) {
						$nameInput.removeClass('missing-field');
					}
					if($emailInput.hasClass('missing-field') ) {
						$emailInput.removeClass('missing-field');
					}
					if($messageInput.hasClass('missing-field') ) {
						$messageInput.removeClass('missing-field');
					}
				}

				/*
				input validation
				*/
				if (!email || !name || !message) {
					emptyContainer();
					if(!email && !name && !message ) {
						$submitMsgCont.append(errorMsg1);
						$emailInput.addClass('missing-field');
						$nameInput.addClass('missing-field');
						$messageInput.addClass('missing-field');
					}
					else if (!email && !name ) {
						$submitMsgCont.append(errorMsg1);
						$emailInput.addClass('missing-field');
						$nameInput.addClass('missing-field');
					}
					else if (!email && !message ) {
						$submitMsgCont.append(errorMsg1);
						$emailInput.addClass('missing-field');
						$messageInput.addClass('missing-field');
					}
					else if (!name && !message ) {
						$submitMsgCont.append(errorMsg1);
						$nameInput.addClass('missing-field');
						$messageInput.addClass('missing-field');
					}
					else if (!email) {
						$submitMsgCont.append(errorMsg2);
						$emailInput.addClass('missing-field');
					}
					else if(!name) {
						$submitMsgCont.append(errorMsg3);
						$nameInput.addClass('missing-field');
					}
					else if(!message) {
						$submitMsgCont.append(errorMsg4);
						$messageInput.addClass('missing-field');
					}
					return false;
				} else {
					$submitMsgCont.empty();
					$submitMsgCont.append(successMsg);
				}


				/*
				ajax to send the data
				*/
			 $.ajax({
				 url: ajaxurl,
				 data: {
					 'action': 'contact_form',
					 'email': email,
					 'name': name,
					 'message': message,
				 },
				 success: function (data) {
				 },
				 error: function (errorThrown) {
				 }
			 });

			 // end ajax call
			 return false;

		});

		//new-contact-form-kariera
		$("#kontaktirajte-nas-kariera").submit(function(e){

			var email    =  $("input[name=your-email]").val();
			var name     =  $("input[name=your-name]").val();
			var message  =  $("textarea").val();

		   var $submitMsgCont = $('.form-submit-msg');
		   var $emailInput = $("input[name=your-email]");
		   var $nameInput = $("input[name=your-name]");
		   var $messageInput = $("textarea");
		   var errorMsg1 = '<p class="alert alert-warning">' + translations.fields_missing_msg + '</p>';
		   var errorMsg2 = '<p class="alert alert-warning">' + translations.email_missing_msg + '</p>';
		   var errorMsg3 = '<p class="alert alert-warning">' + translations.name_missing_msg + '</p>';
		   var errorMsg4 = '<p class="alert alert-warning">' + translations.message_missing_msg + '</p>';
		   var successMsg = '<p class="alert alert-success">' + translations.submit_success_msg + '</p>';


		   /*
		   empty message container on submit before appending
		   remove missing-field class
		   */
		   function emptyContainer() {
			   $submitMsgCont.empty();
			   if($nameInput.hasClass('missing-field') ) {
				   $nameInput.removeClass('missing-field');
			   }
			   if($emailInput.hasClass('missing-field') ) {
				   $emailInput.removeClass('missing-field');
			   }
			   if($messageInput.hasClass('missing-field') ) {
				   $messageInput.removeClass('missing-field');
			   }
		   }

		   /*
		   input validation
		   */
		   if (!email || !name || !message) {
			   emptyContainer();
			   if(!email && !name && !message ) {
				   $submitMsgCont.append(errorMsg1);
				   $emailInput.addClass('missing-field');
				   $nameInput.addClass('missing-field');
				   $messageInput.addClass('missing-field');
			   }
			   else if (!email && !name ) {
				   $submitMsgCont.append(errorMsg1);
				   $emailInput.addClass('missing-field');
				   $nameInput.addClass('missing-field');
			   }
			   else if (!email && !message ) {
				   $submitMsgCont.append(errorMsg1);
				   $emailInput.addClass('missing-field');
				   $messageInput.addClass('missing-field');
			   }
			   else if (!name && !message ) {
				   $submitMsgCont.append(errorMsg1);
				   $nameInput.addClass('missing-field');
				   $messageInput.addClass('missing-field');
			   }
			   else if (!email) {
				   $submitMsgCont.append(errorMsg2);
				   $emailInput.addClass('missing-field');
			   }
			   else if(!name) {
				   $submitMsgCont.append(errorMsg3);
				   $nameInput.addClass('missing-field');
			   }
			   else if(!message) {
				   $submitMsgCont.append(errorMsg4);
				   $messageInput.addClass('missing-field');
			   }
			   return false;
				} else {
					$submitMsgCont.empty();
					$submitMsgCont.append(successMsg);
				}


				/*
				ajax to send the data
				*/
				$.ajax({
					url: ajaxurl,
					data: {
						'action': 'contact_form',
						'email': email,
						'name': name,
						'message': message,
					},
					success: function (data) {
					},
					error: function (errorThrown) {
					}
				});

				// end ajax call
				return false;

		});



		$(document).ready(function()  {

			/*
			contact map
			*/
			function contact_map_init(resp) {

				 	var myLatLng = {lat:parseFloat(resp.lat), lng: parseFloat(resp.lng)};
					var map = new google.maps.Map(document.getElementById('map'), {
						 zoom: 11,
						 center: myLatLng
					});

					var marker = new google.maps.Marker({
						position: {lat:parseFloat(resp.lat), lng: parseFloat(resp.lng)},
						map: map
					});

					marker.addListener('click', function() {
						var friendlyName = resp.address;
					});

					marker.info = new google.maps.InfoWindow({
						content: '<b> ' + resp.address + '</b>'
					});

					google.maps.event.addListener(marker, 'mouseover', function() {
						marker.info.open(map, marker);
					});

					google.maps.event.addListener(marker, 'mouseout', function() {
						marker.info.close();
					});

			}


			jQuery.ajax({
					type : "post",
					dataType : "json",
					url : ajaxurl,
					data : {action: "contact_map" },
					success: function(response) {
						 contact_map_init(response);
					}
			})

		});


   });

})(jQuery, this);
