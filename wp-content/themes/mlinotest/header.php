<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<meta name="facebook-domain-verification" content="2s9kfzhx8mxmdti0g5q6l4rwlrw3fv" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '500718834346300');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=500718834346300&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<noscript>
	 <img height="1" width="1"
	src="https://www.facebook.com/tr?id=1991696274468547&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Ads: 761367893 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-761367893"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-761367893'); </script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157904426-2"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-157904426-2');
	</script>
	<?php if(is_page(28769)) : ?>
	<!-- Event snippet for izpolnitev obrazca krof conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> <script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-761367893/zHe6CNa-xZUBENWahusC', 'event_callback': callback }); return false; } </script>
	<?php endif; ?>

	<?php
	$banner_img_desktop = get_field('banner_slika_desktop', apply_filters( 'wpml_object_id', 5, 'post' ));
	$banner_img_mobile = get_field('banner_slika_mobile', apply_filters( 'wpml_object_id', 5, 'post' ));

	if($banner_img_desktop) :
	$banner_img_desktop2 = $banner_img_desktop['url'];
	else :
		if( ICL_LANGUAGE_CODE=='sl' ) :
			$banner_img_desktop2 = get_template_directory_uri() . '/img/cover-image-mlinotest-desktop.png';
		else :
			$banner_img_desktop2 = get_template_directory_uri() . '/img/cover_banner_img_en.jpg';
		endif;
	endif;

	if($banner_img_mobile) :
	$banner_img_mobile2 = $banner_img_mobile['url'];
	else :
		if( ICL_LANGUAGE_CODE=='sl' ) :
			$banner_img_mobile2 = get_template_directory_uri() . '/img/cover-image-mlinotest-mobile.jpg';
		else :
			$banner_img_mobile2 = get_template_directory_uri() . '/img/cover_banner_img_en.jpg';
		endif;
	endif;
	?>

	<!-- background styles go here -->
	<style>

	#home-storitve .upper-heading {
		color: #bc0f23!important;
	}

	#home-izbrano h2 {
		color: white !important;
	}

		#home-izbrano h3 {
		color: white !important;
	}


	.btn-secondary {
		    border: 2px solid #bc0f23 !important;
			color: #bc0f23 !important;
	}

	.btn-secondary:hover {
		 	background: #bc0f23 !important;
			color: white !important;
			border-color: #bc0f23 !important;
	}

	aside .sidebar-navigation .top-level {
    border-bottom: 2px solid #d01d22!important;
}

	#recepti-search-submit {
		background: #bc0f23!important;
	}

	.obroki-filters span {
		color: #bc0f23!important;
	}
	.recepti-filters-wrapper-2 span {
		color: #bc0f23!important;
	}
		#home-izbrano {
			background: url(<?php echo get_template_directory_uri() . '/img/home-background-article-left.jpg'; ?>) no-repeat top left, url(<?php echo get_template_directory_uri() . '/img/home-background-article-repeat.jpg';?>) repeat-x top right;
		}
		@media all and (max-width: 2600px) {
			#home-izbrano {
				background: url(<?php echo get_template_directory_uri() . '/img/home-background-article-left.jpg'; ?>) no-repeat top left, url(<?php echo get_template_directory_uri() . '/img/home-background-article-repeat.jpg';?>) repeat-x;
			}
		}
		@media all and (max-width: 1600px) {
			#home-izbrano {
				background: url(<?php echo get_template_directory_uri() . '/img/home-background-article-left.jpg'; ?>) no-repeat top left -100px, url(<?php echo get_template_directory_uri() . '/img/home-background-article-repeat.jpg';?>) repeat top -100px;
			}
		}
		@media all and (max-width: 992px) {
			#home-izbrano {
				background: url(<?php echo get_template_directory_uri() . '/img/home-background-article-left.jpg'; ?>) no-repeat top left -300px, url(<?php echo get_template_directory_uri() . '/img/home-background-article-repeat.jpg';?>) repeat;
			}
		}

		.check-wrapper .checkmark {
			background: #bc0f23!important;
		}
		.kontaktni-podatki .fa-map-phone {
			color: #bc0f23!important;
		}
		.kontaktni-podatki .fa-map-envelope {
			color: #bc0f23!important;
		}

		.kontaktni-podatki  .fb-kontakt a{
			background: #bc0f23!important;
		}

		#js--main-search-popup .main-search-popup-wrapper .main-search-input {
			    border-bottom: 2px solid #bc0f23!important;
				color: #bc0f23!important;

		}
		.recepti-popup{
			display: none;
		}
		.recepti-popup.show{
			display: flex;
		}
		.margin-none{
			margin: 0 !important;
		}
		.wrapper-catalog-img .zadnja{
			max-width: 220px;
			height: auto;
			right: -50px;
			position: absolute;
			z-index: 3;
			top: 50%;
			transform: translateY(-50%);
		}
	</style>

	<!-- Ajax URL-->
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>

		<!-- Google Analytics without Cookie -->
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/ga.php?v=<?php echo time(); ?>"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-27848024-1', {'storage': 'none', 'clientId': client_id});
			ga('send', 'pageview', {'anonymizeIp': true});

		</script>
		<!-- End of Google Analytics -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Q21QRNBCZ1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Q21QRNBCZ1');
</script>

</head>


<?php if(!is_front_page()) : ?>
<body <?php body_class();?>>
<?php else : ?>
<body <?php body_class(); ?>>
<?php endif; ?>

<!-- notification pasica -->
<!-- <div class="top-bar-notification">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<p>To spletno mesto je v produkciji. Nekatere vsebine in funkcionalnosti zato ne bodo delovale.</p>
			</div>
		</div>
	</div>
</div> -->
<!-- end notification pasica -->
<div class="social-icons-float d-none d-md-block">
	<?php get_template_part("page-templates-parts/social-icons"); ?>
</div>

<div id="js--main-search-popup">
	<a href="#" id="js--search-popup-close"><i class="fa fa-times" aria-hidden="true"></i></a>
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-12">
				<div class="main-search-popup-wrapper">
					<div class="logo-search-wrapper text-center">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand">
							<img src="<?php echo get_template_directory_uri() . '/img/header-logo-mlinotest.png'; ?>" alt="">
						</a>
					</div>
					<h2><?php _e('Iskalnik', 'mlinotest'); ?></h2>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<header class="header">

	<div class="container">
		<?php
		$subpages = mlinotest_get_page_children(apply_filters( 'wpml_object_id', 2850, 'post' ));
		$first = reset($subpages);
		$firstID = $first->ID;
		?>

		<div class="row">
			<div class="col-12">
				<nav class="header-top-nav d-none d-xl-flex">
					<a href="#" id="js--main-search-btn"><i class="fa fa-search" aria-hidden="true"></i><?php _e('ISKALNIK', 'mlinotest'); ?></a>
					<!-- <a href="<?php the_permalink( apply_filters( 'wpml_object_id', $firstID, 'post' ) ); ?>" class="header-top-link
							<?php if( !isset($_COOKIE["b2b"]) || !($_COOKIE["b2b"]) ): ?><?php else: ?>active<?php endif; ?>" id="b2b-link">B2B</a> -->
					<a href="<?php the_permalink(apply_filters( 'wpml_object_id', 17, 'post' )); ?>" class="header-top-link d-flex align-items-center"><?php _e('Kontakt', 'mlinotest'); ?></a>

					<?php
					$lang = get_all_lang_without_current();
					?>
					<?php if($lang) : ?>
						<?php foreach($lang as $l) : ?>
							<a class="btn-dropdown-lang d-flex align-items-center" href="<?php echo $l['url']; ?>"><?php echo $l['native_name']; ?></a>
						<?php endforeach; ?>
					<?php endif; ?>

				</nav>
			</div>
		</div>

		<div class="row align-items-end main-navigation d-none d-xl-flex">
			<div class="col-xl-5 col-12 ">
				<nav class="header-left-nav">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'header-left',
						'menu_class' => 'header-navigation'
					));
					?>
				</nav>
			</div>
			<div class="col-xl-2">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand">
					<img src="<?php echo get_template_directory_uri() . '/img/header-logo-mlinotest.png'; ?>" alt="">
				</a>
			</div>
			<div class="col-xl-5 col-12">
				<nav class="header-right-nav">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'header-right',
						'menu_class' => 'header-navigation d-flex justify-content-end'
					));
					?>
				</nav>
			</div>
		</div>

		<div class="row align-items-center main-navigation-mobile d-flex d-xl-none">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand">
				<img src="<?php echo get_template_directory_uri() . '/img/header-logo-mlinotest-mobile.png'; ?>" alt="">
			</a>
			<div class="main-mobile-navigation-right d-flex align-items-center">
				<a href="<?php the_permalink(apply_filters( 'wpml_object_id', 17, 'post' )); ?>"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
				<a href="mailto:info@mlinotest.si"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
				<a href="#" id="js--main-search-btn-mobile"><i class="fa fa-search" aria-hidden="true"></i></a>
				<?php
				$lang = get_all_lang_without_current();
				?>
				<?php if($lang) : ?>
					<?php foreach($lang as $l) : ?>
						<a class="btn-dropdown-lang" href="<?php echo $l['url']; ?>"><?php echo $l['code']; ?></a>
					<?php endforeach; ?>
				<?php endif; ?>
				<div id="hamb-icon">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>

		</div>

	</div>

	<div class="container-fluid d-block d-xl-none">
		<div class="row">
			<div class="col-12">
							<nav>
				<?php
					wp_nav_menu(array(
						'theme_location' => 'mobile-main',
						'menu_class' => 'nav-mobile'
					));
				?>
			</nav>
			</div>
		</div>

	</div>


</header>
