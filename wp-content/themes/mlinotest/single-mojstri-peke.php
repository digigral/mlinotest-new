<?php
get_header();
$container   = get_theme_mod('understrap_container_type');
$currentPostId = get_the_ID();
?>

<script>
	jQuery(function() {
		jQuery('#nagradna-igra').click(function() {
			jQuery.scrollTo(jQuery('#nagradna-igra'), 1000);
		});

		jQuery('#accordion-btn').click(function() {
			jQuery('.accordion-content').toggleClass('active');
		});
	});
</script>

<div class="wrapper custom-landing mojstri-peke" id="single-wrapper">
	<div class="container">
		<?php if (get_the_title()) : ?>

			<div class="row">
				<div class="col-12 text-center">
					<h1><?php echo get_the_title(); ?></h1>
				</div>
			</div>

		<?php endif; ?>

		<div class="flexible-content">

			<?php if (have_rows('flexible_content')) : ?>

				<?php 
					$i = 0;
					$numrows = count( get_field( 'flexible_content' ) );
					while (have_rows('flexible_content')) : the_row(); 
					$i++;
					?>

					<?php if (get_row_layout() == 'section_text_full_width_image') :
						$text = get_sub_field('section_text_full_width_image_text');
						$image = get_sub_field('section_text_full_width_image_image');
					?>

						<section class="<?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<div class="row">
								<div class="col-12">
									<?php if ($text) : ?>
										<p><?php echo $text; ?></p>
									<?php endif; ?>
								</div>
							</div>

							<div class="row">
								<div class="col-12 mt-4">
									<?php if ($image) : ?>
										<img src="<?= $image; ?>" ?>
									<?php endif; ?>
								</div>
							</div>
						</section>

					<?php elseif (get_row_layout() == 'section_text_left_image_right') :
						$title = get_sub_field('section_text_left_image_right_title');
						$text = get_sub_field('section_text_left_image_right_text');
						$image = get_sub_field('section_text_left_image_right_image');
					?>

						<section class="row <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>" >
							<div class="col-md-7">
								<?php if ($title) : ?>
									<h1><?php echo $title; ?></h1>
								<?php endif; ?>

								<?php if ($text) : ?>
									<p><?php echo $text; ?></p>
								<?php endif; ?>
							</div>
							<div class="col-md-5">
								<?php if ($image) : ?>
									<img src="<?= $image ?>" />
								<?php endif; ?>
							</div>
						</section>

					<?php elseif (get_row_layout() == 'section_text_left_video_right') :
						$title = get_sub_field('section_text_left_video_right_title');
						$text = get_sub_field('section_text_left_video_right_text');
						$video_id = get_sub_field('section_text_left_video_right_video');
					?>

						<section class="row <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($title) : ?>
								<div class="col-12 text-center mb-4">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>
							<?php if ($text) : ?>
								<div class="col-md-12">
									<p><?php echo $text; ?></p>
								</div>
							<?php endif; ?>
							<?php if ($video_id) : ?>
								<div class="col-md-12">
									<iframe class="youtube" style="width: 100%;" width="400" height="400" src="https://www.youtube.com/embed/<?= $video_id ?>" f rameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							<?php endif; ?>
						</section>

					<?php elseif (get_row_layout() == 'section_text_left_image_right_50') :
						$title = get_sub_field('section_text_left_image_right_50_title');
						$text = get_sub_field('section_text_left_image_right_50_text');
						$image = get_sub_field('section_text_left_image_right_50_image');
					?>

						<section class="row <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($title) : ?>
								<div class="col-12 text-center mb-4">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>
							<?php if ($image) : ?>
								<div class="col-md-6">
									<img src="<?= $image ?>" ?>
								</div>
							<?php endif; ?>
							<?php if ($text) : ?>
								<div class="col-md-6">
									<p><?php echo $text; ?></p>
								</div>
							<?php endif; ?>
						</section>

					<?php elseif (get_row_layout() == 'section_text_right_image_left_50') :
						$title = get_sub_field('section_text_right_image_left_50_title');
						$text = get_sub_field('section_text_right_image_left_50_text');
						$image = get_sub_field('section_text_right_image_left_50_image');
					?>

						<section class="row <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($title) : ?>
								<div class="col-12 text-center mb-4">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>
							<?php if ($image) : ?>
								<div class="col-md-6">
									<img src="<?= $image ?>" ?>
								</div>
							<?php endif; ?>
							<?php if ($text) : ?>
								<div class="col-md-6">
									<p><?php echo $text; ?></p>
								</div>
							<?php endif; ?>
						</section>
					<?php elseif (get_row_layout() == 'section_form') :
						$title = get_sub_field('section_form_title');
						$form = get_sub_field('section_form_form');
						$text = get_sub_field('section_form_text');
					?>

						<section class="row form <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($title) : ?>
								<div class="col-12 text-center mb-4">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>

							<?php if ($text) : ?>
								<div class="col-12">
									<?php echo $text; ?>
								</div>
							<?php endif; ?>

							<?php if ($form) : ?>
								<div class="col-12">
									<?php echo do_shortcode($form); ?>
								</div>
							<?php endif; ?>
						</section>

					<?php elseif (get_row_layout() == 'section_text') :
						$text = get_sub_field('section_text_field');
						$title = get_sub_field('section_text_title');
						$subtitle = get_sub_field('section_text_subtitle')
					?>

						<section class="row <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($title) : ?>
								<div class="col-12 text-center mb-4">
									<h1><?php echo $title; ?></h1>
								</div>
							<?php endif; ?>

							<?php if ($subtitle) : ?>
								<div class="col-12">
									<h2><?php echo $subtitle; ?></h2>
								</div>
							<?php endif; ?>

							<?php if ($text) : ?>
								<div class="col-12">
									<?php echo $text; ?>
								</div>
							<?php endif; ?>
						</section>
					<?php elseif (get_row_layout() == 'section_image') :
						$image = get_sub_field('section_image_field');
					?>
						<section class="row p-0 <?= $i < $numrows ? ($i%2 == 0? 'm-left' : 'm-right') : '' ?>">
							<?php if ($image) : ?>
								<div class="col-12">
									<img src="<?= $image; ?>" ?>
								</div>
							<?php endif; ?>
						</section>
					<?php endif; ?>



				<?php endwhile; ?>

			<?php endif; ?>
		</div>

		<div class="row justify-content-center pt-5  <?= $i%2 == 0? 'm-left' : 'm-right' ?>">
			<div class="col-12">
				<h2 class="text-center" style="color: #fd0d1b;"><?php _e('Spoznaj vse recepte Mojstrov peke in sodeluj v nagradni igri!', 'mlinotest'); ?></h2>
			</div>
		</div>		
		<section class="mojstri-peke-grid" id="nasveti-grid">
			<?php
			$args1 = array(
				'post_type' => 'mojstri-peke',
				'post_not_in' => array($featured->ID),
				'posts_per_page' => 10,
				'post_status' => 'publish',
				'tax_query' => array(
					array(
						'taxonomy' => 'kategorija-mojstri-peke',
						'field'    => 'slug',
						'terms'    => 'jesen',
					),
				),
			);
			$query1 = new WP_Query($args1);
			$wp_query = $query1;
			?>

			<?php if ($wp_query->have_posts()) : ?>
				<div class="row">

					<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
						<?php if ($currentPostId != get_the_id()) : ?>
							<div class="col-lg-4 col-md-6 col-12 nasveti-grid-single">
								<a class="img-overlay" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('large', array('class' => 'img-fluid')); ?>
								</a>
								<a href="<?php the_permalink(); ?>">
									<h3 class="entry-title"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php the_title(); ?></h3>
								</a>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>

				</div>
			<?php endif;
			$i++;
			?>
		</section>
		<div class="row justify-content-center pt-5  <?= $i%2 == 0? 'm-left' : 'm-right' ?>">
			<div class="col-12">
				<h2 class="text-center"><?php _e('Preveri spomladanske recepte Mojstrov peke', 'mlinotest'); ?></h2>
			</div>
		</div>	
		<section class="mojstri-peke-grid" id="nasveti-grid">
			<?php
			$args1 = array(
				'post_type' => 'mojstri-peke',
				'post_not_in' => array($featured->ID),
				'posts_per_page' => 10,
				'post_status' => 'publish',
				'tax_query' => array(
					array(
						'taxonomy' => 'kategorija-mojstri-peke',
						'field'    => 'slug',
						'terms'    => 'pomlad',
					),
				),
			);
			$query1 = new WP_Query($args1);
			$wp_query = $query1;
			?>

			<?php if ($wp_query->have_posts()) : ?>
				<div class="row">

					<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
						<?php if ($currentPostId != get_the_id()) : ?>
							<div class="col-lg-4 col-md-6 col-12 nasveti-grid-single">
								<a class="img-overlay" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('large', array('class' => 'img-fluid')); ?>
								</a>
								<a href="<?php the_permalink(); ?>">
									<h3 class="entry-title"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php the_title(); ?></h3>
								</a>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>

				</div>
			<?php endif;
			?>
		</section>
		<section class="row">
			<div class="col-12 text-center mb-4">
				<h1>Ali ste vedeli?</h1>
			</div>

			<div class="col-12">
				<p style="text-align: center;"><strong>Preberite naše članke in izvedite več o </strong><br>
					<a href="https://www.mlinotest.si/nasveti/kaj-pomenita-tip-in-moc-moke/">Tipi in moč moke</a><br>
					<a href="https://www.mlinotest.si/nasveti/priporocila-za-uporabo-nasih-izdelkov-moke-za-pripravo-razlicnih-vrst-testa-in-jedi/">Katero moko uporabimo pri pripravi različnih vrst testa</a>
				</p>
			</div>
		</section>

	</div>

</div>

<?php get_footer(); ?>