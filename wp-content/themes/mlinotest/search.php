<?php /* Template Name: search */ ?>

<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>


<div class="wrapper wrapper-subpages" id="search-results-wrapper" style="padding-top: 0;">

    <?php include("page-templates-parts/home/section-1.php"); ?>

	<div class="container" tabindex="-1">

		<div class="row">

            <div class="col-12">

			<main class="site-main" id="main">

                <?php 
                $searchQuery = get_search_query();

                $query1 = new WP_Query( array( 'post_type' => 'izdelek', 'posts_per_page' => 10, 's' => $searchQuery ) );
                $query2 = new WP_Query( array( 'post_type' => 'recepti', 'posts_per_page' => 10, 's' => $searchQuery ) );
                $args3 = array(
                    'post_type' => array( 'post', 'page', 'sklici', 'sklepi', 'objave', 'nasveti' ),
                    'posts_per_page' => 10,
                    's' => $searchQuery
                );
                $query3 = new WP_Query( $args3 );
                ?>

                    <div class="row">
                        <div class="col-12">
                            <h1 class="page-title"><?php printf( esc_html__( 'Rezultati iskanja za: %s', mlinotest ), '<span class="page-title">' . $searchQuery . '</span>' ); ?></h1>
                        </div>
                    </div>


                    <!-- ni rezultatov v nobeni od kategorij  -->
                    <?php if ( !have_posts() ) : ?>
                        <div class="row">
                            <div class="col-12">
                                <p><?php _e('Vsebine, povezane s tem iskalnim nizom, nismo našli.', 'mlinotest'); ?></p>  
                            </div>
                        </div>
                    <?php endif; ?>

                    
                    <!-- start izdelki query search -->
                    <?php if ( $query1->have_posts() ) : ?>
                        <div class="row">
                            <div class="col-12">
                                <h2 class="search-secondary-title"><?php _e('Izdelki:', 'mlinotest'); ?></h2>
                            </div>
                        </div>
                        <?php while ( $query1->have_posts() ) : $query1->the_post(); ?>
                            <?php
                            get_template_part( 'loop-templates/content', 'search' );
                            ?>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                    <!-- end izdelki query search -->

                    <!-- start recepti query search -->
                    <?php if ( $query2->have_posts() ) : ?>
                        <div class="row">
                            <div class="col-12">
                                <h2 class="search-secondary-title"><?php _e('Recepti:', 'mlinotest'); ?></h2>
                            </div>
                        </div>
                        <?php while ( $query2->have_posts() ) : $query2->the_post(); ?>
                            <?php
                            get_template_part( 'loop-templates/content', 'search' );
                            ?>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                    <!-- end recepti query search -->


                    <!-- start ostalo query search -->
                    <?php if ( $query3->have_posts() ) : ?>
                        <div class="row">
                            <div class="col-12">
                                <h2 class="search-secondary-title"><?php _e('Ostalo:', 'mlinotest'); ?></h2>
                            </div>
                        </div>
                        <?php while ( $query3->have_posts() ) : $query3->the_post(); ?>
                            <?php
                            get_template_part( 'loop-templates/content', 'search' );
                            ?>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                    <!-- end ostalo query search -->

			</main><!-- #main -->

        </div><!-- #primary -->
        
        </div>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

