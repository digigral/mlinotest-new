<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */
$fields = get_fields();
//d($fields);


$this_id = get_the_id();


$the_query = new WP_Query( array(
		'post_type' => 'izdelek',
		'posts_per_page'   => -1,
		) );

$izdelki = $the_query->posts;

$sorodni = array();

foreach ($izdelki as $key => $i) {
	 $povezani = get_field("izdelek_povezani_recepti", $i->ID);
	 //d($i);
	 if($povezani != null && $povezani != false) {
		 	foreach ($povezani as $key => $p) {
		 		if($p['recept']->ID == $this_id ) {
						$sorodni[] = $i;
				}
		 	}
	 }
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->

	<?php

	$thumb = get_field('glavna_slika_recepta');
	if ($thumb) :
	?>

	<div class="single-thumbnail-wrapper">
		<img src="<?php echo $thumb['url']; ?>" alt="<?php echo $thumb['alt']; ?>">

		<?php if($fields['recept_cas_priprave'] or $fields['recept_dodaten_opis']) : ?>
		<div class="single-thumbnail-tags">

			<?php if($fields['recept_cas_priprave']) : ?>
			<span><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $fields['recept_cas_priprave']; ?></span>
			<?php endif; ?>

			<?php if($fields['recept_dodaten_opis']) : ?>
			<span><i class="fa fa-chef-o" aria-hidden="true"></i><?php echo $fields['recept_dodaten_opis']; ?></span>
			<?php endif; ?>

		</div>
		<?php endif; ?>

	</div>
	<?php endif; ?>


	<div class="entry-content">

		<?php if($fields['priprava_repeater']) : ?>
		<div class="row">
			<div class="col-12">
				<h3><?php echo $fields['priprava_naslov']; ?></h3>

				<?php $i=1; foreach($fields['priprava_repeater'] as $p) : ?>
				<ol class="single-recept-priprava">
					<li><span><?php echo $i; ?>&period;</span><?php echo $p['priprava_text']; ?></li>
				</ol>
				<?php $i++; endforeach; ?>

			</div>
		</div>
		<?php endif; ?>

		<div class="row" style="margin-bottom: 16px;">
			<div class="col-12">
				<?php the_content(); ?>
			</div>
		</div>


		<div class="row single-recept-sestavine">

			<?php if( count($sorodni) == 0) : ?>
			<div class="col-lg-8 col-12 ingredients-table">

				<?php if($fields['sestavine_naslov']) : ?>
				<h4><?php echo $fields['sestavine_naslov']; ?></h4>
				<?php endif; ?>

				<?php if($fields['sestavine_podnaslov']) : ?>
				<h5><?php echo $fields['sestavine_podnaslov']; ?></h5>
				<?php endif; ?>

					<?php
					$table = $fields['sestavine_vsebina'];
					$body = $table['body'];
					
					$tableHeaderFirst = $table['header'][0]['c'];
					$tableHeaderSecond = $table['header'][1]['c'];
					$tableHeaderThird = $table['header'][2]['c'];
					
					$tableHeader4 = $table['header'][3]['c'];
					$tableHeader5 = $table['header'][4]['c'];
					

					
					?>

					<?php if($table) : ?>
						<div id="table-row" class="row">

							<!-- first table start -->
							<?php 
							$hasRowsFirst = $body[0][0];
							if($hasRowsFirst) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderFirst) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderFirst; ?></th>
										</tr>
									</thead>
									<?php endif; ?>

									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[0]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>

								</table>
							</div>
							<?php endif; ?>
							<!-- first table end -->

							<!-- second table start -->
							<?php 
							$hasRowsSecond = $body[0][1];
							if($hasRowsSecond) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderSecond) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderSecond; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[1]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- second table end -->

							<!-- third table start -->
							<?php 
							$hasRowsThird = $body[0][2];
							if($hasRowsThird) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderThird) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderThird; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[2]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- third table end -->
							
								<!-- 4 table start -->
							<?php 
							$hasRows4 = $body[0][3];
							if($hasRows4) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeader4) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeader4; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[3]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- 4 table end -->
							
								<!-- 5 table start -->
							<?php 
							$hasRows5 = $body[0][4];
							if($hasRows5) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeader5) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeader5; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[4]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- 5 table end -->
							
							
						
						</div>

					<?php endif; ?>

			</div>

			<!-- this is case if w have sorodni izdelki, we have markup grid 5-7 -->
			<?php else : ?>


				<div class="col-xl-5 col-12 order-2 order-xl-1">
				
					<?php if($sorodni) : ?>
						<div class="slick-sorodni-produkti">

							<?php 
							$total = count($sorodni);
							?>

					
							<?php foreach ($sorodni as $key => $s):

								$cur_id = $s->ID;

								$first = mlinotest_get_page_children( apply_filters( 'wpml_object_id', 7  ) );

								 foreach ($first as $f):

									 $children = get_children( $f->ID);

										foreach($children as $c) :

											$children2 = get_children( $c->ID);


											if(count($children2) > 0 ):
													foreach ($children2 as $key => $c2) {

														if ( have_rows('izdelki_v_tej_kategoriji', $c2->ID) ) :

															 while( have_rows('izdelki_v_tej_kategoriji', $c2->ID) ) : the_row();

																$izdelek = get_sub_field('izdelek', $c2->ID);

																	if($cur_id == $izdelek->ID):

																	 $link = get_permalink( $c->ID ) . $c2->post_name .   '#' . $izdelek->post_name;


																endif;

															 endwhile;

														 endif;

													}
											endif;

											if ( have_rows('izdelki_v_tej_kategoriji', $c->ID) ) :

												 while( have_rows('izdelki_v_tej_kategoriji', $c->ID) ) : the_row();

													$izdelek = get_sub_field('izdelek', $c->ID);
													//d($izdelek->ID);
														if($cur_id == $izdelek->ID):

														 $link = get_permalink( $c->ID ) . '#' . $izdelek->post_name;

													endif;

												 endwhile;

											 endif;

										endforeach;

								 endforeach; ?>


								<div class="slick-item">
									<a href="<?php echo $link; ?>">
										<img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($s->ID);  ?>" alt="First slide">
										<h4 class="sorodni-produkt-title"><?php echo get_the_title($s->ID); ?></h4>
									</a>
								</div>

							<?php endforeach; ?>

						</div>

					<?php endif; ?>


				</div>

				<div class="col-xl-7 col-12 order-1 order-xl-2 ingredients-table">

					<?php if($fields['sestavine_naslov']) : ?>
					<h4><?php echo $fields['sestavine_naslov']; ?></h4>
					<?php endif; ?>

					<?php if($fields['sestavine_podnaslov']) : ?>
					<h5><?php echo $fields['sestavine_podnaslov']; ?></h5>
					<?php endif; ?>

					<?php
					$table = $fields['sestavine_vsebina'];
					$body = $table['body'];
					$tableHeaderFirst = $table['header'][0]['c'];
					$tableHeaderSecond = $table['header'][1]['c'];
					$tableHeaderThird = $table['header'][2]['c'];
					
					$tableHeader4 = $table['header'][3]['c'];
					$tableHeader5 = $table['header'][4]['c'];
					?>

					<?php if($table) : ?>
						<div id="table-row" class="row">

							<!-- first table start -->
							<?php 
							$hasRowsFirst = $body[0][0];
							if($hasRowsFirst) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderFirst) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderFirst; ?></th>
										</tr>
									</thead>
									<?php endif; ?>

									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[0]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>

								</table>
							</div>
							<?php endif; ?>
							<!-- first table end -->

							<!-- second table start -->
							<?php 
							$hasRowsSecond = $body[0][1];
							if($hasRowsSecond) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderSecond) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderSecond; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[1]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- second table end -->

							<!-- third table start -->
							<?php 
							$hasRowsThird = $body[0][2];
							if($hasRowsThird) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeaderThird) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeaderThird; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[2]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- third table end -->
							
											<!-- 4 table start -->
							<?php 
							$hasRows4 = $body[0][3];
							if($hasRows4) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeader4) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeader4; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[3]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- 4 table end -->
							
								<!-- 5 table start -->
							<?php 
							$hasRows5 = $body[0][4];
							if($hasRows5) :
							?>
							<div class="col-12">
								<table>

									<?php if($tableHeader5) : ?>
									<thead>
										<tr>
											<th><?php echo $tableHeader5; ?></th>
										</tr>
									</thead>
									<?php endif; ?>


									<tbody>
										<?php foreach($body as $row) : ?>

											<?php 
											$ing = $row[4]['c']; 
											if($ing && $ing != '') :
											?>
											<tr>
												<td><?php echo $ing; ?></td>
											</tr>
											<?php endif; ?>

										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
							<!-- 5 table end -->

						
						</div>

					<?php endif; ?>


				</div>

				<?php endif; ?>
				<!-- END this is case if w have sorodni izdelki, we have markup grid 4-8 -->


			</div>
		</div>


	</div><!-- .entry-content -->


</article><!-- #post-## -->
