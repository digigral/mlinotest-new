<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */
?>


<article <?php post_class('featured2'); ?> id="post-<?php the_ID(); ?>"> <!-- TUKAJ NE POTREBUJEMO TEGA CLASSA, SAMO REGULAREN STYLING NOVIC  -->
	<div class="row">
		
		<div class="col-lg-6 post-thumbnail-wrapper">
			<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
		</div>

		<div class="col-lg-6">
			<header class="entry-header">
				<?php if ( 'novice' == get_post_type() ) : ?>
					<?php
						$terms = get_terms('novice_cat');
						$count = count($terms);
						$counter = 1;

						foreach($terms as $term){
							$link = get_term_link($term->term_id);
							if($counter != $count) {
								echo '<a href="' . $link . '">' . $term->name . ', </a>';
							} else {
								echo '<a href="' . $link . '">' . $term->name . '</a>';
							}
							$counter++;
						}

					?>
				<?php endif; ?>
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
				'</a></h2>' ); ?>
				<?php if ( 'novice' == get_post_type() ) : ?>
					<time><?php the_date('d - m - Y'); ?></time>
				<?php endif; ?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_excerpt();?>
			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<a class="btn btn-secondary" href="<?php the_permalink(); ?>">Več</a>
			</footer>


		</div>


	</div>

</article><!-- #post-## -->
