<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */
$pic_on = get_field('prikazi_prikazno_fotko');
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		
	</header><!-- .entry-header -->
	
	<?php 
	$thumb = get_the_post_thumbnail( $post->ID, 'nasveti-single' );
	if ($thumb && $pic_on) :
	?>
	<div class="single-thumbnail-wrapper">
		<?php echo $thumb; ?>	
	</div>
	<?php endif; ?>
	

	<div class="entry-content">
	
		<?php the_content(); ?>

	</div><!-- .entry-content -->


</article><!-- #post-## -->
