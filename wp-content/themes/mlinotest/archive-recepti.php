<?php
 /* The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

wp_enqueue_script( 'recipes-scripts' );

$recipes_posts = mlinotest_get_all_recipes();
$terms_obroki = mlinotest_get_obroki_terms();
$terms_kat    = mlinotest_get_kategorije_rec_terms();

$recept = get_random_featured_recepti();
//d($recept);

$r = $_GET['id'];
$r_array = array();
$has_params = false;

if( $r ) {
	$r_array = explode(',', $r);
	$has_params = true;
}

?>
<style>
#recepti-grid .recepti-featured-single-wrapper a .img-wrapper{
	border: none !important;
}
</style>


<div class="wrapper wrapper-subpages wrapper-recepti-archive" id="archive-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main w-100" id="main">
				<div class="container">

					<?php
					if($recept['predlog_link_title'] == 'zajtrk') :
					$okusen = 'okusen';
					$kakovosten = 'kakovosten';
					$other1 = 'kosilo';
					$other2 = 'večerjo';
					elseif($recept['predlog_link_title'] == 'kosilo'):
					$kakovosten = 'kakovostno';
					$okusen = 'okusno';
					$other1 = 'zajtrk';
					$other2 = 'večerjo';
					else :
					$kakovosten = 'kakovostno';
					$okusen = 'okusno';
					$other1 = 'zajtrk';
					$other2 = 'kosilo';
					endif;
					?>

					<section class="recepti-featured-top">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-12 text-center">
								<div class="naslov-wrapper">
									<h1 class="recepti-top-naslov">Mlinotestovi predlogi<br> za <span class="js--change-text"><?php echo $kakovosten; ?> in <?php echo $okusen; ?></span> <a class="js-recepti-pop-btn" href="#" ><?php echo $recept['predlog_link_title']; ?></a>.
										<img class="img-fluid klikni-spremeni-img" src="<?php echo get_template_directory_uri() . '/img/klikni-in-spremeni.png'; ?>" alt="">
										<div class="recepti-popup">
											<p>
											<?php _e('Predlogi za', 'mlinotest'); ?>
											<a href="#" data-obrok="<?php echo $other1; ?>" class="popup-link"><?php echo $other1; ?></a>
											<?php _e('ali', 'mlinotest'); ?>
											<a href="#" data-obrok="<?php echo $other2; ?>" class="popup-link"><?php echo $other2; ?></a>
											</p>
										</div>

									</h1>
								</div>
							</div>
						</div>

						<div class="row justify-content-center js-recept-featured-wrapper">
							<div class="col-lg-6">
								<h2 class="text-center text-lg-left"><?php echo $recept['title']; ?></h2>
								<p class="d-none d-lg-block"><?php echo $recept['content']; ?></p>

								<?php
								$pozicija = $recept['img_produkta_pos'];
								if($pozicija == 'Pod tekstom') :
								?>
									<?php if($recept['img_product_url']) : ?>
									<div class="product-image-left-wrapper">
										<a class="d-none d-lg-inline-block" href="<?php echo $recept['link_featured_prod']; ?>">
											<img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
										</a>
									</div>
									<?php endif; ?>
								<?php endif; ?>

								<div class="link-wrapper d-none d-lg-block">
									<a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a>
									<a href="#" class="share-recepti js-share-btn"><?php _e('Pošlji sebi ali prijatelju', 'mlinotest'); ?></a>

									<div class="social-share-links">
										<a target="_blank" href="<?php echo get_FB_social_share_buttons($recept['title'], $recept['link']); ?>"><img class="front-social-img" src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-fb.png" class="share-img"></a>
										<a target="_blank" href="<?php echo get_TW_social_share_buttons($recept['title'], $recept['link']); ?>"><img  class="front-social-img"src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-twitter.png" class="share-img"></a>
										<a id="share-mail" href="<?php echo get_mail_social_share_buttons($recept['title'], $recept['link']); ?>">
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</a>
										<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>
									</div>

								</div>
							</div>
							<div class="col-lg-6 col-12 text-center text-lg-left">

								<?php if($recept['img_main_url']) : ?>
								<div class="rotate-img-wrapper">
									<img class="img-fluid recipe-image recipe-image-rotate" src="<?php echo $recept['img_main_url']; ?>" alt="<?php echo $recept['img_main_alt']; ?>">
								</div>
								<?php endif; ?>

								<?php if($pozicija != 'Pod tekstom') : ?>
									<?php if($recept['img_product_url']) : ?>
										<a class="product-image-link" href="<?php echo $recept['link_featured_prod']; ?>">
											<img class="product-image img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
										</a>
									<?php endif; ?>
								<?php endif; ?>

								<?php
								if($pozicija == 'Pod tekstom') :
								?>
									<?php if($recept['img_product_url']) : ?>
									<div class="product-image-left-wrapper">
										<a class="d-inline-block d-lg-none" href="<?php echo $recept['link_featured_prod']; ?>">
											<img class="product-image-left img-fluid" src="<?php echo $recept['img_product_url']; ?>" alt="<?php echo $recept['img_product_alt']; ?>">
										</a>
									</div>
									<?php endif; ?>
								<?php endif; ?>

								<div class="link-wrapper d-block d-lg-none">
									<a href="<?php echo $recept['link']; ?>" class="btn btn-primary"><?php _e('Spoznaj', 'mlinotest'); ?></a><br>
									<a href="#" class="share-recepti js-share-btn"><?php _e('Pošlji sebi ali prijatelju', 'mlinotest'); ?></a>

									<div class="social-share-links">
										<a target="_blank" href="<?php echo get_FB_social_share_buttons($recept['title'], $recept['link']); ?>"><img class="front-social-img" src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-fb.png" class="share-img"></a>
										<a target="_blank" href="<?php echo get_TW_social_share_buttons($recept['title'], $recept['link']); ?>"><img  class="front-social-img"src="<?php echo get_template_directory_uri(); ?>/img/recipe-single-share-twitter.png" class="share-img"></a>
										<a id="share-mail" href="<?php echo get_mail_social_share_buttons($recept['title'], $recept['link']); ?>">
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</a>
										<a href="#" class="js-social-share-close"><i class="fa fa-times" aria-hidden="true"></i></a>
									</div>

								</div>

							</div>
						</div>
					</section>


					<section id="recepti-grid">

						<div id="seznam-izdelki" class="row justify-content-center">
							<div class="col-12 text-center">
								<div class="naslov-wrapper text-center">
									<h2 class="text-center"><?php _e('Zbirka receptov', 'mlinotest'); ?></h2>
								</div>
							</div>
						</div>

						<div class="row" id="recepti-filters">

							<div class="col-12">
								<div class="recepti-filters-wrapper">
									<h6><?php _e('Razvrsti po kategoriji:', 'mlinotest'); ?></h6>

									<?php if($terms_kat): ?>
										<div class="filters-checkboxes kategorije-filters">
											<?php foreach ($terms_kat as $t): ?>
													<label><input class="change-recipes" data-slug="<?php echo $t->slug; ?>" type="checkbox" name="cb-<?php echo $t->slug; ?>" id="cb-<?php echo $t->slug; ?>"><?php echo $t->name; ?></label>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>

								</div>
							</div>

							<div class="col-lg-6">

								<div class="recepti-filters-wrapper-2">
									<div class="filters-checkboxes obroki-filters">
										<span><?php _e('Razvrsti po obroku:', 'mlinotest'); ?></span>

										<?php if($terms_obroki): ?>
												<?php foreach ($terms_obroki as $t): ?>
														<label><input class="change-recipes" data-slug="<?php echo $t->slug; ?>" type="checkbox" name="cb-<?php echo $t->slug; ?>" id="cb-<?php echo $t->slug; ?>"><?php echo $t->name; ?></label>
												<?php endforeach; ?>
										<?php endif; ?>

									</div>
								</div>


							</div>

							<div class="col-lg-6">
								<div class="recepti-filters-wrapper-2">
									<span><?php _e('Išči po imenu', 'mlinotest'); ?></span>
									<form id="recepti-search">
										<input type="search" name="recepti-search" id="recepti-search-form">
										<button type="submit" form="recepti-search" id="recepti-search-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
									</form>
								</div>
							</div>

						</div>

						<div class="row recipe-list" style="min-height: 400px; overflow: hidden;">

							<!--	no url parameters -->
							<?php if( $recipes_posts && $has_params == false ): ?>
								<?php foreach ($recipes_posts as $key => $rp): ?>

									<?php
									$thumb = get_field('glavna_slika_recepta', $rp->ID);
									?>

									<div class="col-lg-4 col-sm-6 col-12 single-recipe">
										<div class="recepti-featured-single-wrapper">
											<a class="img-overlay" href="<?php echo get_the_permalink($rp->ID); ?>">
												<div class="img-wrapper" style="background:url(<?php echo $thumb['url']; ?>) center;"></div>
											</a>
											<a href="<?php echo get_the_permalink($rp->ID); ?>">
												<h3 class="entry-title"><span><img src="<?php echo get_template_directory_uri() . '/img/before-link.png'; ?>" alt=""></span><?php echo $rp->post_title; ?></h3>
											</a>
										</div>
									</div>

								<?php endforeach; ?>

								<!--	if there are url parameters -->
								<?php else : ?>

									<?php foreach ($r_array as $r_id): ?>

									<?php
									$thumb2 = get_field('glavna_slika_recepta', $r_id);
									?>

									<div class="col-lg-4 col-sm-6 col-12 single-recipe">
										<div class="recepti-featured-single-wrapper">
											<a class="img-overlay" href="<?php echo get_the_permalink($r_id); ?>">
												<div class="img-wrapper" style="background:url(<?php echo $thumb2['url']; ?>) center;"></div>
											</a>
											<a href="<?php echo get_the_permalink($r_id); ?>">
												<h3 class="entry-title"><span>&gt;</span><?php echo get_the_title($r_id); ?></h3>
											</a>
										</div>
									</div>

									<?php endforeach; ?>


							<?php endif; ?>

						</div>

					</section>


				</div>
			</main><!-- #main -->


		</div><!-- #primary -->

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php
get_footer();
?>
