<?php

function trgovine_locations_callback() {

  $location_array = mlinotest_get_all_trgovine_with_loc();

  echo json_encode($location_array);
	die();
}

add_action('wp_ajax_trgovine_locations', 'trgovine_locations_callback');
add_action('wp_ajax_nopriv_trgovine_locations', 'trgovine_locations_callback');


function trgovine_locations_tax_callback() {

  $sel = $_POST['sel'];

  $location_array = get_loc_trgovine_by_terms($sel);

  if($sel == null || $sel == "") {
    $location_array = mlinotest_get_all_trgovine_with_loc();
  }

  echo json_encode($location_array);
	die();
}

add_action('wp_ajax_trgovine_locations_tax', 'trgovine_locations_tax_callback');
add_action('wp_ajax_nopriv_trgovine_locations_tax', 'trgovine_locations_tax_callback');

function recepti_tax_callback() {

  $sel1  = $_POST['sel1'];
  $sel2 =  $_POST['sel2'];


  if($sel1 == "") {
    $sel1 == "";
  }
  else {
      $sel1  = explode(',', $sel1);
  }

  if($sel2 == "") {
    $sel2 == "";
  }
  else {
      $sel2  = explode(',', $sel2);
  }

  $recipes = mlinotest_get_all_recipes_by_mul_tax(  $sel1 ,  $sel2 );

  echo json_encode($recipes);
	die();
}

add_action('wp_ajax_recepti_tax', 'recepti_tax_callback');
add_action('wp_ajax_nopriv_recepti_tax', 'recepti_tax_callback');


function recepti_featured_callback() {

  $obrok  = $_POST['obrok'];

	$rand_recipe = get_random_featured_recepti_click($obrok);
	echo json_encode($rand_recipe);
	die();
}

add_action('wp_ajax_recepti_featured', 'recepti_featured_callback');
add_action('wp_ajax_nopriv_recepti_featured', 'recepti_featured_callback');


function contact_map_callback() {
	$contact_map_arr = mlinotest_get_contact_map_marker_data();

	echo json_encode($contact_map_arr);
	die();
}
add_action('wp_ajax_contact_map', 'contact_map_callback');
add_action('wp_ajax_nopriv_contact_map', 'contact_map_callback');


function recipe_titles_callback() {
	$recipe_titles = mlinotest_get_all_recipe_titles();

	echo json_encode($recipe_titles);
	die();

}
add_action('wp_ajax_recipe_titles', 'recipe_titles_callback');
add_action('wp_ajax_nopriv_recipe_titles', 'recipe_titles_callback');



// AJAX: for custom contact forms - email sendig with
// in this ajax call, we use our general sendy signup code, just that we do it with ajax this time

function contact_form_callback() {

  $response = false;
  $get_params = false;

  if ( isset($_REQUEST) ) {
		$b_email = $_REQUEST['email'];
		$b_name = $_REQUEST['name'];
		$b_text = $_REQUEST['message'];
	}

  // double validation, also here in php

  if ( $b_email != null && $b_email != "" ){
    $get_params = true;
  }

  // if validation success, we send email with sendgrid api & custom implementation
  if( $get_params == true ) {

      // we must build a variable for contet with all fields
      $email_content  = "<strong>Novo povpraševanje s spletni strani Mlinotest</strong><br/><br/>";

      $email_content .= "<strong>Email: </strong>" . $b_email . "<br/>";
      $email_content .= "<strong>Ime: </strong>" . $b_name . "<br/>";
      $email_content .= "<strong>Sporočilo: </strong>" . $b_text . "<br/>";

      $sendgrid_api_key = "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";
      $email_from       = array('wd+mlinotest@digigral.si','info@mlinotest.si');  // do this email we send all povprasevanja
      $email_subject    = "Novo povpraševanje s spletne strani";
	    $from_name        = "Mlinotest - spletno mesto";


      $sendgrid = new SendGrid($sendgrid_api_key);
      $email    = new SendGrid\Email();

      $email->addTo($email_from)
          ->setFrom("wd+mlinotest@digigral.si")
          ->setSubject($email_subject)
					->setReplyTo($b_email)
          ->setFromName($from_name)
          ->setHtml($email_content);

      $sendgrid->send($email);

      $response = true;

  }


  echo $response;
	die(); // this is required to return a proper result
}

add_action('wp_ajax_contact_form', 'contact_form_callback');
add_action('wp_ajax_nopriv_contact_form', 'contact_form_callback');


//Kariera form
function contact_form_kariera_callback() {

  $response = false;
  $get_params = false;

  if ( isset($_REQUEST) ) {
		$b_email = $_REQUEST['email'];
		$b_name = $_REQUEST['name'];
		$b_text = $_REQUEST['message'];
	}

  // double validation, also here in php

  if ( $b_email != null && $b_email != "" ){
    $get_params = true;
  }

  // if validation success, we send email with sendgrid api & custom implementation
  if( $get_params == true ) {

      // we must build a variable for contet with all fields
      $email_content  = "<strong>Novo Karierno povpraševanje s spletni strani Mlinotest</strong><br/><br/>";

      $email_content .= "<strong>Email: </strong>" . $b_email . "<br/>";
      $email_content .= "<strong>Ime: </strong>" . $b_name . "<br/>";
      $email_content .= "<strong>Sporočilo: </strong>" . $b_text . "<br/>";

      $sendgrid_api_key = "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";
      $email_from       = array('wd+mlinotest@digigral.si','info@mlinotest.si','kariera@mlinotest.si');  // do this email we send all povprasevanja
      $email_subject    = "Novo Karierno povpraševanje s spletne strani";
	    $from_name        = "Mlinotest - spletno mesto";


      $sendgrid = new SendGrid($sendgrid_api_key);
      $email    = new SendGrid\Email();

      $email->addTo($email_from)
          ->setFrom("wd+mlinotest@digigral.si")
          ->setSubject($email_subject)
					->setReplyTo($b_email)
          ->setFromName($from_name)
          ->setHtml($email_content);

      $sendgrid->send($email);

      $response = true;

  }


  echo $response;
	die(); // this is required to return a proper result
}

add_action('wp_ajax_contact_form_kariera', 'contact_form_kariera_callback');
add_action('wp_ajax_nopriv_contact_form_kariera', 'contact_form_kariera_callback');



function contact_form2_callback() {

  $response = false;
  $get_params = false;

  if ( isset($_REQUEST) ) {
		$b_email = $_REQUEST['email'];
		$b_name = $_REQUEST['name'];
		$b_lastname = $_REQUEST['lastname'];
    $b_company = $_REQUEST['company'];
	}

  // double validation, also here in php

  if ( $b_email != null && $b_email != "" ){
    $get_params = true;
  }

  // if validation success, we send email with sendgrid api & custom implementation
  if( $get_params == true ) {

      createB2bLead($b_email, $b_name, $b_lastname, $b_company);
      $response = true;

  }

  echo $response;
	die(); // this is required to return a proper result
}

add_action('wp_ajax_contact_form2', 'contact_form2_callback');
add_action('wp_ajax_nopriv_contact_form2', 'contact_form2_callback');

function contact_form_b2b_callback() {

  $response = false;
  $get_params = false;

  if ( isset($_REQUEST) ) {
		$b_email = $_REQUEST['email'];
    $b_name = $_REQUEST['name'];
    $b_phone = $_REQUEST['phone'];
		$b_option = $_REQUEST['option'];
    $b_company = $_REQUEST['company'];
    $b_subject = $_REQUEST['subject'];
    $b_message = $_REQUEST['message'];
	}

  // double validation, also here in php

  if ( $b_email != null && $b_email != "" ){
    $get_params = true;
  }

  // if validation success, we send email with sendgrid api & custom implementation
  if( $get_params == true ) {
      $email_content  = "<strong>Novo B2B povpraševanje s spletni strani Mlinotest</strong><br/><br/>";

      $email_content .= "<strong>Email: </strong>" . $b_email . "<br/>";
      $email_content .= "<strong>Ime in priimek: </strong>" . $b_name . "<br/>";
      $email_content .= "<strong>Telefon: </strong>" . $b_phone . "<br/>";
      $email_content .= "<strong>Kaj vas zanima: </strong>" . $b_option . "<br/>";
      $email_content .= "<strong>Podjetje: </strong>" . $b_company . "<br/>";
      $email_content .= "<strong>Zadeva: </strong>" . $b_subject . "<br/>";
      $email_content .= "<strong>Sporočilo: </strong>" . $b_message . "<br/>";

      $sendgrid_api_key = "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";
      
      if ( $b_option == "blagovna znamka Mlinotest za slovenski trg" ){
        $email_from = array('wd+mlinotest@digigral.si','info@mlinotest.si','simon.usaj@mlinotest.si');  
      } else if( $b_option == "blagovna znamka Mlinotest za tuje trge" ) {
        $email_from = array('wd+mlinotest@digigral.si','info@mlinotest.si','export@mlinotest.si');  
      } else if( $b_option == "privat label" ) {
        $email_from = array('wd+mlinotest@digigral.si','info@mlinotest.si','robi.rutar@mlinotest.si');  

      } else if( $b_option == "izdelki za Ho.Re.Ca" ) {
        $email_from = array('wd+mlinotest@digigral.si','info@mlinotest.si','miran.virant@mlinotest.si', 'dalibor.tesanovic@mlinotest.si');  

      } else if($b_option == "drugo") {
        $email_from = array('wd+mlinotest@digigral.si', 'info@mlinotest.si');  
      } else {
        $email_from = array('wd+mlinotest@digigral.si','info@mlinotest.si');  
      }

      $email_subject    = "Novo B2 povpraševanje s spletne strani";
      $from_name        = "Mlinotest - spletno mesto";


      $sendgrid = new SendGrid($sendgrid_api_key);
      $email    = new SendGrid\Email();

      $email->addTo($email_from)
          ->setFrom("wd+mlinotest@digigral.si")
          ->setSubject($email_subject)
          ->setReplyTo($b_email)
          ->setFromName($from_name)
          ->setHtml($email_content);

      $sendgrid->send($email);
      $response = true;

  }

  echo $response;
	die(); // this is required to return a proper result
}

add_action('wp_ajax_contact_form_b2b', 'contact_form_b2b_callback');
add_action('wp_ajax_nopriv_contact_form_b2b', 'contact_form_b2b_callback');
