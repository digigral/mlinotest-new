<?php 
$title = __('PREVERI', 'mlinotest');

function landing_button_shortcode() {
     return '<a id="accordion-btn" class="btn btn-primary"> ' . $title . '</a>';
}

add_shortcode( 'landing-button', 'landing_button_shortcode' );