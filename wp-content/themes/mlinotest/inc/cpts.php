<?php

/**
 *
 *  Custom Post Type registration
 *
 */

function register_my_posttypes()
{
    /*
   *
   * mojstri peke
   *
   */

  $labels = array(
    'name'               => _x( 'Mojstri Peke', 'post type general name', 'mlinotest' ),
    'singular_name'      => _x( 'Mojstri Peke', 'post type singular name', 'mlinotest' ),
    'menu_name'          => _x( 'Mojstri Peke', 'admin menu', 'mlinotest' ),
    'name_admin_bar'     => _x( 'Mojstri Peke', 'add new on admin bar', 'mlinotest' ),
    'add_new'            => _x( 'Dodaj nov prispevek', 'slider', 'mlinotest' ),
    'add_new_item'       => __( 'Vsi prispevki', 'mlinotest' ),
    'new_item'           => __( 'Nov prispevek', 'mlinotest' ),
    'edit_item'          => __( 'Uredi prispevek', 'mlinotest' ),
    'view_item'          => __( 'Poglej prispevek', 'mlinotest' ),
    'all_items'          => __( 'Vsi prispevki', 'mlinotest' ),
    'search_items'       => __( 'Išči prispevke', 'mlinotest' ),
    'parent_item_colon'  => __( 'Parent prispevek:', 'mlinotest' ),
    'not_found'          => __( 'Ni najdenih prispevkov.', 'mlinotest' ),
    'not_found_in_trash' => __( 'Ni najdenih prispevkov v košu.', 'mlinotest' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'mojstri-peke', 'with_front' => TRUE ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => null,
    'supports'           => array( 'title', 'author', 'thumbnail', 'revisions', 'excerpt', 'editor')
);

register_post_type( 'mojstri-peke', $args );

$labels = array(
  'name'              => _x( 'Kategorije mojstri peke', 'taxonomy general name', 'mlinotest' ),
  'singular_name'     => _x( 'Kategorije mojstri peke', 'taxonomy singular name', 'mlinotest' ),
  'search_items'      => __( 'Išči Kategorije mojstri peke', 'mlinotest' ),
  'all_items'         => __( 'Vse mojstri peke', 'mlinotest' ),
  'parent_item'       => __( 'Parent mojstri peke', 'mlinotest' ),
  'parent_item_colon' => __( 'Parent', 'mlinotest' ),
  'edit_item'         => __( 'Uredi', 'mlinotest' ),
  'update_item'       => __( 'Posodobi', 'mlinotest' ),
  'add_new_item'      => __( 'Dodaj mojstri peke', 'mlinotest' ),
  'new_item_name'     => __( 'Nova mojstri peke', 'mlinotest' ),
  'menu_name'         => __( 'Kategorije mojstri peke', 'mlinotest' ),
);


$args = array(
  'hierarchical'      => true,
  'labels'            => $labels,
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true,
  'rewrite'           => array( 'slug' => 'kategorija-mojstri-peke' ),
);

register_taxonomy( 'kategorija-mojstri-peke', array( 'mojstri-peke' ), $args );


  /*
   *
   * izdelki
   *
   */

  $labels = array(
      'name'               => _x( 'Izdelki', 'post type general name', 'mlinotest' ),
      'singular_name'      => _x( 'Izdelki', 'post type singular name', 'mlinotest' ),
      'menu_name'          => _x( 'Izdelki', 'admin menu', 'mlinotest' ),
      'name_admin_bar'     => _x( 'Izdelki', 'add new on admin bar', 'mlinotest' ),
      'add_new'            => _x( 'Dodaj nov izdelek', 'slider', 'mlinotest' ),
      'add_new_item'       => __( 'Vsi izdelki', 'mlinotest' ),
      'new_item'           => __( 'Nov izdelek', 'mlinotest' ),
      'edit_item'          => __( 'Uredi izdelek', 'mlinotest' ),
      'view_item'          => __( 'Poglej izdelek', 'mlinotest' ),
      'all_items'          => __( 'Vsi izdelki', 'mlinotest' ),
      'search_items'       => __( 'Išči izdelek', 'mlinotest' ),
      'parent_item_colon'  => __( 'Parent izdelek:', 'mlinotest' ),
      'not_found'          => __( 'Ni najdenih izdelkov.', 'mlinotest' ),
      'not_found_in_trash' => __( 'Ni najdenih izdelkov v košu.', 'mlinotest' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'izdelek', 'with_front' => FALSE ),
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'izdelek', $args );


  /*
   *
   * sklici
   *
   */

  $labels = array(
    'name'               => _x( 'Sklici', 'post type general name', 'mlinotest' ),
    'singular_name'      => _x( 'Sklic', 'post type singular name', 'mlinotest' ),
    'menu_name'          => _x( 'Sklici', 'admin menu', 'mlinotest' ),
    'name_admin_bar'     => _x( 'Sklici', 'add new on admin bar', 'mlinotest' ),
    'add_new'            => _x( 'Dodaj nov sklic', 'slider', 'mlinotest' ),
    'add_new_item'       => __( 'Vsi sklici', 'mlinotest' ),
    'new_item'           => __( 'Nov sklic', 'mlinotest' ),
    'edit_item'          => __( 'Uredi sklic', 'mlinotest' ),
    'view_item'          => __( 'Poglej sklic', 'mlinotest' ),
    'all_items'          => __( 'Vsi sklici', 'mlinotest' ),
    'search_items'       => __( 'Išči sklic', 'mlinotest' ),
    'parent_item_colon'  => __( 'Parent sklic:', 'mlinotest' ),
    'not_found'          => __( 'Ni najdenih sklicov.', 'mlinotest' ),
    'not_found_in_trash' => __( 'Ni najdenih sklicov v košu.', 'mlinotest' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'sklici-skupscine', 'with_front' => FALSE ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);

register_post_type( 'sklici', $args );


  /*
   *
   * sklepi
   *
   */

  $labels = array(
    'name'               => _x( 'Sklepi', 'post type general name', 'mlinotest' ),
    'singular_name'      => _x( 'Sklep', 'post type singular name', 'mlinotest' ),
    'menu_name'          => _x( 'Sklepi', 'admin menu', 'mlinotest' ),
    'name_admin_bar'     => _x( 'Sklepi', 'add new on admin bar', 'mlinotest' ),
    'add_new'            => _x( 'Dodaj nov sklep', 'slider', 'mlinotest' ),
    'add_new_item'       => __( 'Vsi sklepi', 'mlinotest' ),
    'new_item'           => __( 'Nov sklep', 'mlinotest' ),
    'edit_item'          => __( 'Uredi sklep', 'mlinotest' ),
    'view_item'          => __( 'Poglej sklep', 'mlinotest' ),
    'all_items'          => __( 'Vsi sklepi', 'mlinotest' ),
    'search_items'       => __( 'Išči sklep', 'mlinotest' ),
    'parent_item_colon'  => __( 'Parent sklep:', 'mlinotest' ),
    'not_found'          => __( 'Ni najdenih sklepov.', 'mlinotest' ),
    'not_found_in_trash' => __( 'Ni najdenih sklepov v košu.', 'mlinotest' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'sklepi-skupscine', 'with_front' => FALSE ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);

register_post_type( 'sklepi', $args );



 /*
   *
   * sporočila za javnost
   *
   */

  $labels = array(
    'name'               => _x( 'Medijske objave', 'post type general name', 'mlinotest' ),
    'singular_name'      => _x( 'Medijska objava', 'post type singular name', 'mlinotest' ),
    'menu_name'          => _x( 'Medijsko središče', 'admin menu', 'mlinotest' ),
    'name_admin_bar'     => _x( 'Medijsko središče', 'add new on admin bar', 'mlinotest' ),
    'add_new'            => _x( 'Dodaj nov objavo', 'slider', 'mlinotest' ),
    'add_new_item'       => __( 'Vse objave', 'mlinotest' ),
    'new_item'           => __( 'Nova objava', 'mlinotest' ),
    'edit_item'          => __( 'Uredi objavo', 'mlinotest' ),
    'view_item'          => __( 'Poglej objavo', 'mlinotest' ),
    'all_items'          => __( 'Vse objave', 'mlinotest' ),
    'search_items'       => __( 'Išči objave', 'mlinotest' ),
    'parent_item_colon'  => __( 'Parent objava:', 'mlinotest' ),
    'not_found'          => __( 'Ni najdenih objav.', 'mlinotest' ),
    'not_found_in_trash' => __( 'Ni najdenih objav v košu.', 'mlinotest' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'medijsko-sredisce', 'with_front' => FALSE ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);

register_post_type( 'mediji', $args );


  /*
   *
   * objave
   *
   */

  $labels = array(
    'name'               => _x( 'Objave', 'post type general name', 'mlinotest' ),
    'singular_name'      => _x( 'Objave', 'post type singular name', 'mlinotest' ),
    'menu_name'          => _x( 'Objave', 'admin menu', 'mlinotest' ),
    'name_admin_bar'     => _x( 'Objave', 'add new on admin bar', 'mlinotest' ),
    'add_new'            => _x( 'Dodaj novo objavo', 'slider', 'mlinotest' ),
    'add_new_item'       => __( 'Vse objave', 'mlinotest' ),
    'new_item'           => __( 'Nova objava', 'mlinotest' ),
    'edit_item'          => __( 'Uredi objavo', 'mlinotest' ),
    'view_item'          => __( 'Poglej objavo', 'mlinotest' ),
    'all_items'          => __( 'Vse objave', 'mlinotest' ),
    'search_items'       => __( 'Išči objavo', 'mlinotest' ),
    'parent_item_colon'  => __( 'Parent objava:', 'mlinotest' ),
    'not_found'          => __( 'Ni najdenih objav.', 'mlinotest' ),
    'not_found_in_trash' => __( 'Ni najdenih objav v košu.', 'mlinotest' )
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'objave', 'with_front' => FALSE ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
);

register_post_type( 'objave', $args );



  /*
   *
   * trgovine
   *
   */

  $labels = array(
      'name'               => _x( 'Trgovine', 'post type general name', 'mlinotest' ),
      'singular_name'      => _x( 'Trgovine', 'post type singular name', 'mlinotest' ),
      'menu_name'          => _x( 'Trgovine', 'admin menu', 'mlinotest' ),
      'name_admin_bar'     => _x( 'Trgovine', 'add new on admin bar', 'mlinotest' ),
      'add_new'            => _x( 'Dodaj nov trgovino', 'slider', 'mlinotest' ),
      'add_new_item'       => __( 'Vsi trgovine', 'mlinotest' ),
      'new_item'           => __( 'Nov trgovina', 'mlinotest' ),
      'edit_item'          => __( 'Uredi trgovine', 'mlinotest' ),
      'view_item'          => __( 'Poglej trgovine', 'mlinotest' ),
      'all_items'          => __( 'Vsi trgovine', 'mlinotest' ),
      'search_items'       => __( 'Išči trgovinu', 'mlinotest' ),
      'parent_item_colon'  => __( 'Parent trgovina:', 'mlinotest' ),
      'not_found'          => __( 'Ni najdenih trgovin.', 'mlinotest' ),
      'not_found_in_trash' => __( 'Ni najdenih trgovin v košu.', 'mlinotest' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'izdelek', 'with_front' => FALSE ),
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'trgovine', $args );

  $labels = array(
    'name'              => _x( 'Kategorije trgovine', 'taxonomy general name', 'mlinotest' ),
    'singular_name'     => _x( 'Kategorije trgovine', 'taxonomy singular name', 'mlinotest' ),
    'search_items'      => __( 'Išči Kategorije trgovine', 'mlinotest' ),
    'all_items'         => __( 'Vse trgovine', 'mlinotest' ),
    'parent_item'       => __( 'Parent trgovine', 'mlinotest' ),
    'parent_item_colon' => __( 'Parent', 'mlinotest' ),
    'edit_item'         => __( 'Uredi', 'mlinotest' ),
    'update_item'       => __( 'Posodobi', 'mlinotest' ),
    'add_new_item'      => __( 'Dodaj trgovine', 'mlinotest' ),
    'new_item_name'     => __( 'Nova trgovine', 'mlinotest' ),
    'menu_name'         => __( 'Kategorije trgovine', 'mlinotest' ),
  );


  $args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'kategorija-trgovine' ),
	);

	register_taxonomy( 'kategorija-trgovine', array( 'trgovine' ), $args );

  /*
   *
   * recepti
   *
   */

  $labels = array(
      'name'               => _x( 'Recepti', 'post type general name', 'mlinotest' ),
      'singular_name'      => _x( 'Recepti', 'post type singular name', 'mlinotest' ),
      'menu_name'          => _x( 'Recepti', 'admin menu', 'mlinotest' ),
      'name_admin_bar'     => _x( 'Recepti', 'add new on admin bar', 'mlinotest' ),
      'add_new'            => _x( 'Dodaj nov recept', 'slider', 'mlinotest' ),
      'add_new_item'       => __( 'Vsi Recepti', 'mlinotest' ),
      'new_item'           => __( 'Nov Recepti', 'mlinotest' ),
      'edit_item'          => __( 'Uredi recept', 'mlinotest' ),
      'view_item'          => __( 'Poglej recept', 'mlinotest' ),
      'all_items'          => __( 'Vsi Recepti', 'mlinotest' ),
      'search_items'       => __( 'Išči Recepti', 'mlinotest' ),
      'parent_item_colon'  => __( 'Parent Recepti:', 'mlinotest' ),
      'not_found'          => __( 'Ni najdenih Recepti.', 'mlinotest' ),
      'not_found_in_trash' => __( 'Ni najdenih Recepti v košu.', 'mlinotest' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'recepti', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'taxonomies' => array('post_tag'),
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'recepti', $args );

  $labels = array(
    'name'              => _x( 'Kategorije receptov', 'taxonomy general name', 'mlinotest' ),
    'singular_name'     => _x( 'Kategorije receptov', 'taxonomy singular name', 'mlinotest' ),
    'search_items'      => __( 'Išči med kategorijami receptov', 'mlinotest' ),
    'all_items'         => __( 'Vse kategorije receptov', 'mlinotest' ),
    'parent_item'       => __( 'Parent kategorije recept', 'mlinotest' ),
    'parent_item_colon' => __( 'Parent kategorije recept', 'mlinotest' ),
    'edit_item'         => __( 'Uredi kategorijo recepta', 'mlinotest' ),
    'update_item'       => __( 'Posodobi kategorijo recepta', 'mlinotest' ),
    'add_new_item'      => __( 'Dodaj kategorijo recepta', 'mlinotest' ),
    'new_item_name'     => __( 'Nova kategorija recepta', 'mlinotest' ),
    'menu_name'         => __( 'Kategorija receptov', 'mlinotest' ),
  );


  $args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'kategorija-recepta' ),
	);

	register_taxonomy( 'kategorija-recepta', array( 'recepti' ), $args );


  $labels = array(
    'name'              => _x( 'Obrok', 'taxonomy general name', 'mlinotest' ),
    'singular_name'     => _x( 'Kategorije obroki', 'taxonomy singular name', 'mlinotest' ),
    'search_items'      => __( 'Išči med obroki', 'mlinotest' ),
    'all_items'         => __( 'Vsi obroki', 'mlinotest' ),
    'parent_item'       => __( 'Parent obroki', 'mlinotest' ),
    'parent_item_colon' => __( 'Parent obroki', 'mlinotest' ),
    'edit_item'         => __( 'Uredi obrok', 'mlinotest' ),
    'update_item'       => __( 'Posodobi obroke', 'mlinotest' ),
    'add_new_item'      => __( 'Dodaj obrok', 'mlinotest' ),
    'new_item_name'     => __( 'Nova obrok', 'mlinotest' ),
    'menu_name'         => __( 'Kategorije obroki', 'mlinotest' ),
  );


  $args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'obrok' ),
	);

	register_taxonomy( 'obrok', array( 'recepti' ), $args );

  /*
   *
   * Nasveti
   *
   */

  $labels = array(
      'name'               => _x( 'Nasveti', 'post type general name', 'mlinotest' ),
      'singular_name'      => _x( 'Nasveti', 'post type singular name', 'mlinotest' ),
      'menu_name'          => _x( 'Nasveti', 'admin menu', 'mlinotest' ),
      'name_admin_bar'     => _x( 'Nasveti', 'add new on admin bar', 'mlinotest' ),
      'add_new'            => _x( 'Dodaj nov nasvet', 'slider', 'mlinotest' ),
      'add_new_item'       => __( 'Vsi Nasveti', 'mlinotest' ),
      'new_item'           => __( 'Nov Nasvet', 'mlinotest' ),
      'edit_item'          => __( 'Uredi Nasvet', 'mlinotest' ),
      'view_item'          => __( 'Poglej Nasveti', 'mlinotest' ),
      'all_items'          => __( 'Vsi Nasveti', 'mlinotest' ),
      'search_items'       => __( 'Išči Nasvete', 'mlinotest' ),
      'parent_item_colon'  => __( 'Parent Nasveti:', 'mlinotest' ),
      'not_found'          => __( 'Ni najdenih Nasvetov.', 'mlinotest' ),
      'not_found_in_trash' => __( 'Ni najdenih Nasvetov v košu.', 'mlinotest' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'nasveti', 'with_front' => true ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'nasveti', $args );


  $labels = array(
      'name'               => _x( 'B2B', 'post type general name', 'mlinotest' ),
      'singular_name'      => _x( 'B2B', 'post type singular name', 'mlinotest' ),
      'menu_name'          => _x( 'B2B', 'admin menu', 'mlinotest' ),
      'name_admin_bar'     => _x( 'B2B', 'add new on admin bar', 'mlinotest' ),
      'add_new'            => _x( 'Dodaj nov B2B', 'slider', 'mlinotest' ),
      'add_new_item'       => __( 'Vsi B2B', 'mlinotest' ),
      'new_item'           => __( 'Nov B2B', 'mlinotest' ),
      'edit_item'          => __( 'Uredi B2B', 'mlinotest' ),
      'view_item'          => __( 'Poglej B2B', 'mlinotest' ),
      'all_items'          => __( 'Vsi B2B', 'mlinotest' ),
      'search_items'       => __( 'Išči B2B', 'mlinotest' ),
      'parent_item_colon'  => __( 'Parent B2B:', 'mlinotest' ),
      'not_found'          => __( 'Ni najdenih B2B.', 'mlinotest' ),
      'not_found_in_trash' => __( 'Ni najdenih B2B v košu.', 'mlinotest' )
  );

  $args = array(
      'labels'             => $labels,
      'public'             => false,
      'publicly_queryable' => false,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => false,
      'rewrite'            => array( 'slug' => 'b2b-users', 'with_front' => false ),
      'capability_type'    => 'post',
      'has_archive'        => false,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
  );

  register_post_type( 'b2b', $args );
}

add_action( 'init', 'register_my_posttypes' );



/*
nasveti custom taxonomy
*/

add_action( 'init', 'novice_tax', 0 );
function novice_tax() {

	$labels = array(
		'name'              => _x( 'Kategorije receptov', 'taxonomy general name', 'mlinotest' ),
		'singular_name'     => _x( 'Kategorije receptov', 'taxonomy singular name', 'mlinotest' ),
		'search_items'      => __( 'IščiKategorije receptov', 'mlinotest' ),
		'all_items'         => __( 'Vse kategorije', 'mlinotest' ),
		'parent_item'       => __( 'Parent kategorije', 'mlinotest' ),
		'parent_item_colon' => __( 'Parent', 'mlinotest' ),
		'edit_item'         => __( 'Uredi', 'mlinotest' ),
		'update_item'       => __( 'Posodobi', 'mlinotest' ),
		'add_new_item'      => __( 'Dodaj kategorijo', 'mlinotest' ),
		'new_item_name'     => __( 'Nova kategorija', 'mlinotest' ),
		'menu_name'         => __( 'Kategorije receptov', 'mlinotest' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'kategorija-recepta' ),
	);

	register_taxonomy( 'kategorija-recepta', array( 'recepti' ), $args );

}



/*
cpt objave rewrite za date archive
*/
add_action('generate_rewrite_rules', 'my_date_archives_rewrite_rules');
function my_date_archives_rewrite_rules($wp_rewrite) {
  $rules = my_generate_date_archives('objave', $wp_rewrite);
  $wp_rewrite->rules = $rules + $wp_rewrite->rules;
  return $wp_rewrite;
}

function my_generate_date_archives($cpt, $wp_rewrite) {
	$rules = array();

	$post_type = get_post_type_object($cpt);
	$slug_archive = $post_type->has_archive;
	if ($slug_archive === false) return $rules;
	if ($slug_archive === true) {
	  $slug_archive = isset($post_type->rewrite['slug']) ? $post_type->rewrite['slug'] : $post_type->name;
	}

	$dates = array(
	  array(
		'rule' => "([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",
		'vars' => array('year', 'monthnum', 'day')),
	  array(
		'rule' => "([0-9]{4})/([0-9]{1,2})",
		'vars' => array('year', 'monthnum')),
	  array(
		'rule' => "([0-9]{4})",
		'vars' => array('year'))
	);

	foreach ($dates as $data) {
	  $query = 'index.php?post_type='.$cpt;
	  $rule = $slug_archive.'/'.$data['rule'];

	  $i = 1;
	  foreach ($data['vars'] as $var) {
		$query.= '&'.$var.'='.$wp_rewrite->preg_index($i);
		$i++;
	  }

	  $rules[$rule."/?$"] = $query;
	  $rules[$rule."/feed/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
	  $rules[$rule."/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
	  $rules[$rule."/page/([0-9]{1,})/?$"] = $query."&paged=".$wp_rewrite->preg_index($i);
	}

	return $rules;
  }


