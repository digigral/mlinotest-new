<?php


function get_fields_for_page($id)
{
  $fields = get_fields($id);
  return $fields;
}

function get_trgovine_terms()
{

  $terms = get_terms( 'kategorija-trgovine', array(
    'hide_empty' => false,
  ) );

  return $terms;
}


function createB2bLead($email, $name, $lastname, $company){

  $your_content = "Ime: " . $name . "<br/>";
  $your_content .= "Priimek: " . $lastname . "<br/>";
  $your_content .= "Podjetje: " . $company . "<br/>";

  $post_id = wp_insert_post(array (
     'post_type' => 'b2b',
     'post_title' => $email,
     'post_content' => $your_content,
     'post_status' => 'publish',
    ));

}

function get_excerpt_by_id($post_id)
{
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 20; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}

function mlinotest_get_main_product_categories( $id ) {


  $args = array(
      'post_parent' => $id,
      'post_type'   => 'page',
      'numberposts' => -1,
      'post_status' => 'publish',
      'orderby' => 'menu_order',
      'order'	=> 'ASC'
  );

  $children = get_children( $args );

  return $children;

}

function get_loc_trgovine_by_terms($terms)
{
  $all_locations  = array();

	$termsArr = [];

	foreach ($terms as $key => $value) {
    if($value == 'fransiza') {
      $termsArr[] = "fransiza";
    } else if ($value == 'lastna-maloprodaja' ) {
      $termsArr[] = "lastna-maloprodaja";
    } else if ($value == 'mlincek' ){
      $termsArr[] = "mlincek";
    } else if ($value == 'restavracija' ) {
			$termsArr[] = "restavracija";
		}
  }

	  $the_query = new WP_Query( array(
        'post_type' => 'trgovine',
		    'posts_per_page'   => -1,
        'tax_query' => array(
            array (
                'taxonomy' => 'kategorija-trgovine',
                'field' => 'slug',
                'terms' => $terms,
            )
        ),
    ) );

    if($the_query->posts) {

        foreach ($the_query->posts as $post) {

              $rep = get_field("trgovina_lokacija", $post->ID);
              $term_list = wp_get_post_terms($post->ID, 'kategorija-trgovine', array("fields" => "all"));
              $category = get_term( $term_list[0]->term_id, 'kategorija-trgovine' );
              $marker = get_field('trgovina_cat_marker', $category);
              $content_post = get_post($post->ID);
              $content = $content_post->post_content;
              $content = apply_filters('the_content', $content);
              $content = str_replace(']]>', ']]&gt;', $content);
              $title = get_the_title($post->ID);
              $array = array(
                      'post' => $post,
                      'loc'  => $rep,
                      'marker' => $marker,
                      'content' => $content,
                      'title' => $title
                );

              $all_locations[] = $array;
        }
      }


	// $query = new WP_Query( array(
	// 	'post_type' => 'trgovine',
	// 	'meta_q'
	// ) );

	// $path = get_template_directory() . '/map-locations.json';
	// $response = file_get_contents($path);
	// $decoded = json_decode($response, true);

	// foreach ($decoded as $dec) {
	// 		$type = $dec["type"];
	// 		if( in_array($type, $termsArr) ) {
	// 			$all_locations[] = $dec;
	// 		}
	// }

	return $all_locations;
}

function mlinotest_get_products($id)
{
  $produtcs = get_field( "izdelki_v_tej_kategoriji",$id );

  if($produtcs) {
    foreach ($produtcs as $p) {
      $fields = get_fields($p['izdelek']->ID);
      if($fields) {
        $p['izdelek']->fields = $fields;
      }
    }
  }

  return $produtcs;
}


function mlinotest_get_page_children($id){

	$args = array(
			'post_parent' => $id,
			'post_type'   => 'page',
			'numberposts' => -1,
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'	=> 'ASC'
	);

	$children = get_children( $args );

	return $children;
}


function mlinotest_get_all_trgovine_with_loc()
{

  $all_locations  = array();

  $the_query = new WP_Query( array(
        'post_type' => 'trgovine',
		    'posts_per_page'   => -1,

    ) );

    if($the_query->posts) {

      foreach ($the_query->posts as $post) {

					$rep = get_field("trgovina_lokacija", $post->ID);
					$term_list = wp_get_post_terms($post->ID, 'kategorija-trgovine', array("fields" => "all"));
					$category = get_term( $term_list[0]->term_id, 'kategorija-trgovine' );
					$marker = get_field('trgovina_cat_marker', $category);
					$content_post = get_post($post->ID);
					$content = $content_post->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]&gt;', $content);
					$title = get_the_title($post->ID);
            $array = array(
                    'post' => $post,
                    'loc'  => $rep,
										'marker' => $marker,
										'content' => $content,
										'title' => $title
              );
            $all_locations[] = $array;
      }
	  }

	return $all_locations;

		// $path = get_template_directory() . '/map-locations.json';
		// $response = file_get_contents($path);
		// $decoded = json_decode($response, true);

		// return $decoded;

}


function mlinotest_get_all_recipes()
{
  $the_query = new WP_Query( array(
      'post_type' => 'recepti',
      'posts_per_page'   => -1,
      ) );

  return $the_query->posts;
}

function mlinotest_get_obroki_terms()
{
  $terms = get_terms( 'obrok', array( 'hide_empty' => false, ) );
  return $terms;
}

function mlinotest_get_kategorije_rec_terms()
{
  $terms = get_terms( 'kategorija-recepta', array( 'hide_empty' => false, ) );
  return $terms;
}



function mlinotest_get_all_recipes_by_mul_tax( $obroki, $kategorije )
{

    if(!$obroki || !is_array($obroki) ) {
      $obroki1 = mlinotest_get_obroki_terms();
      $obroki = array();
      foreach ($obroki1 as $key => $o) {
         $obroki[] = $o->slug;
      }
    }

    if(!$kategorije || !is_array($kategorije) ) {
      $kategorije1 = mlinotest_get_kategorije_rec_terms();
      $kategorije = array();
      foreach ($kategorije1 as $key => $o) {
         $kategorije[] = $o->slug;
      }
    }

    $all  = array();

    $args = array(
        'post_type' => 'recepti',
        'posts_per_page'   => -1,

        'tax_query' => array(

              'relation' => 'AND',

              array(
                'taxonomy' => 'obrok',
                'field' => 'slug',
                'terms' => $obroki,
                'operator' => 'IN'
              ),
              array(
                'taxonomy' => 'kategorija-recepta',
                'field' => 'slug',
                'terms' => $kategorije,
                'operator' => 'IN'

              )
        )
      );

  $the_query = new WP_Query( $args );

  if($the_query->posts) {

    foreach ($the_query->posts as $post) {

              $d1 = get_the_permalink( $post->ID );
              $d2 = $post->post_name;
              $d3 = get_field('glavna_slika_recepta', $post->ID)['url'];
              $d4 = get_the_title($post->ID);

              $all[] =  array(

                  'id'    => $d1,
                  'slug'  => $d2,
                  'img'   => $d3,
                  'title' => $d4,

                );

    }

  }

  return $all;
}


function mlinotest_get_all_nasveti_posts() {
	$the_query = new WP_Query( array(
      'post_type' => 'nasveti',
	  'posts_per_page'   => -1,
	  'post_status' => 'publish'
      ) );

  return $the_query->posts;
}


function get_random_featured_recepti () {

	date_default_timezone_set('Europe/Zagreb');

	$ura_zamenjave_zajtrk = get_field('ura_menjava_vecerja_zajtrk', 5);
	$ura_zamenjave_kosilo = get_field('ura_menjava_zajtrk-kosilo', 5);
	$ura_zamenjave_vecerja = get_field('ura_menjava_kosilo_vecerja', 5);

	$predlogi_zajtrk = get_field('predlogi_zajtrk', 5);
	$predlogi_kosilo = get_field('predlogi_kosilo', 5);
	$predlogi_vecerja = get_field('predlogi_vecerja', 5);

	// get random keys from arrays
	$rand_zajtrk_key = array_rand($predlogi_zajtrk, 1);
	$rand_kosilo_key = array_rand($predlogi_kosilo, 1);
	$rand_vecerja_key = array_rand($predlogi_vecerja, 1);

	// get random post
	$rand_zajtrk = $predlogi_zajtrk[$rand_zajtrk_key];
	$rand_kosilo = $predlogi_kosilo[$rand_kosilo_key];
	$rand_vecerja = $predlogi_vecerja[$rand_vecerja_key];

	if( date("H:i:s") >= $ura_zamenjave_zajtrk && date("H:i:s") < $ura_zamenjave_kosilo ) {
		$rand_recepti = $rand_zajtrk;
		$predlog_link_title = __('zajtrk', 'mlinotest');
	} elseif ( date("H:i:s") >= $ura_zamenjave_kosilo && date("H:i:s") < $ura_zamenjave_vecerja ) {
		$rand_recepti = $rand_kosilo;
		$predlog_link_title = __('kosilo', 'mlinotest');
	} else {
		$rand_recepti = $rand_vecerja;
		$predlog_link_title = __('večerjo', 'mlinotest');
	}

	// create new array for js
	$title = $rand_recepti['recept']->post_title;
	$text = $rand_recepti['recept']->post_content;
	$trimmed_recipe = wp_trim_words( $text, 70, $more = null );
	$link = get_permalink($rand_recepti['recept']->ID);
	$img_main_url = get_the_post_thumbnail_url($rand_recepti['recept']->ID);
	$thumbnail_id = get_post_thumbnail_id($rand_recepti['recept']->ID);
	$img_main_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
	$img_product = get_field('predlogi_slika_produkta', $rand_recepti['recept']->ID);
	$img_product_url = $img_product['url'];
	$img_product_alt = $img_product['alt'];

	$link_featured_prod = get_field('predlogi_slika_produkta_link', $rand_recepti['recept']->ID);

	$pozicija = get_field('predlogi_slika_produkta_pozicija', $rand_recepti['recept']->ID);

	$feat_recepti_arr = array(
		'predlog_link_title' => $predlog_link_title,
		'title' => $title,
		'content' => $trimmed_recipe,
		'link' => $link,
		'img_main_url' => $img_main_url,
		'img_main_alt'  => $img_main_alt,
		'img_product_url' => $img_product_url,
		'img_product_alt' => $img_product_alt,
		'link_featured_prod' => $link_featured_prod,
		'img_produkta_pos' => $pozicija
	);

	return $feat_recepti_arr;

}

function get_random_featured_recepti_click($obrok) {

	$predlogi_zajtrk = get_field('predlogi_zajtrk', 5);
	$predlogi_kosilo = get_field('predlogi_kosilo', 5);
	$predlogi_vecerja = get_field('predlogi_vecerja', 5);

	// get random keys from arrays
	$rand_zajtrk_key = array_rand($predlogi_zajtrk, 1);
	$rand_kosilo_key = array_rand($predlogi_kosilo, 1);
	$rand_vecerja_key = array_rand($predlogi_vecerja, 1);

	// get random post
	$rand_zajtrk = $predlogi_zajtrk[$rand_zajtrk_key];
	$rand_kosilo = $predlogi_kosilo[$rand_kosilo_key];
	$rand_vecerja = $predlogi_vecerja[$rand_vecerja_key];

	if($obrok === 'zajtrk') {
		$title = $rand_zajtrk['recept']->post_title;
		$text = $rand_zajtrk['recept']->post_content;
		$trimmed_recipe = wp_trim_words( $text, 70, $more = null );
		$link = get_permalink($rand_zajtrk['recept']->ID);
		$img_main_url = get_the_post_thumbnail_url($rand_zajtrk['recept']->ID);
		$thumbnail_id = get_post_thumbnail_id($rand_zajtrk['recept']->ID);
		$img_main_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
		$img_product = get_field('predlogi_slika_produkta', $rand_zajtrk['recept']->ID);
		$img_product_url = $img_product['url'];
		$img_product_alt = $img_product['alt'];

		$link_featured_prod = get_field('predlogi_slika_produkta_link', $rand_zajtrk['recept']->ID);
		$pozicija = get_field('predlogi_slika_produkta_pozicija', $rand_zajtrk['recept']->ID);

		$recept_zajtrk_arr = array(
			'predlog_link_title' => 'zajtrk',
			'other1' => 'večerjo',
			'other2' => 'kosilo',
			'title' => $title,
			'content' => $trimmed_recipe,
			'link' => $link,
			'img_main_url' => $img_main_url,
			'img_main_alt'  => $img_main_alt,
			'img_product_url' => $img_product_url,
			'img_product_alt' => $img_product_alt,
			'link_featured_prod' => $link_featured_prod,
			'img_produkta_pos' => $pozicija
		);

		return $recept_zajtrk_arr;

	} elseif ($obrok === 'kosilo') {
		$title = $rand_kosilo['recept']->post_title;
		$text = $rand_kosilo['recept']->post_content;
		$trimmed_recipe = wp_trim_words( $text, 70, $more = null );
		$link = get_permalink($rand_kosilo['recept']->ID);
		$img_main_url = get_the_post_thumbnail_url($rand_kosilo['recept']->ID);
		$thumbnail_id = get_post_thumbnail_id($rand_kosilo['recept']->ID);
		$img_main_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
		$img_product = get_field('predlogi_slika_produkta', $rand_kosilo['recept']->ID);
		$img_product_url = $img_product['url'];
		$img_product_alt = $img_product['alt'];

		$link_featured_prod = get_field('predlogi_slika_produkta_link', $rand_kosilo['recept']->ID);
		$pozicija = get_field('predlogi_slika_produkta_pozicija', $rand_kosilo['recept']->ID);

		$recept_kosilo_arr = array(
			'predlog_link_title' => 'kosilo',
			'other1' => 'zajtrk',
			'other2' => 'večerjo',
			'title' => $title,
			'content' => $trimmed_recipe,
			'link' => $link,
			'img_main_url' => $img_main_url,
			'img_main_alt'  => $img_main_alt,
			'img_product_url' => $img_product_url,
			'img_product_alt' => $img_product_alt,
			'link_featured_prod' => $link_featured_prod,
			'img_produkta_pos' => $pozicija
		);

		return $recept_kosilo_arr;
	} else {

		$title = $rand_vecerja['recept']->post_title;
		$text = $rand_vecerja['recept']->post_content;
		$trimmed_recipe = wp_trim_words( $text, 70, $more = null );
		$link = get_permalink($rand_vecerja['recept']->ID);
		$img_main_url = get_the_post_thumbnail_url($rand_vecerja['recept']->ID);
		$thumbnail_id = get_post_thumbnail_id($rand_vecerja['recept']->ID);
		$img_main_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
		$img_product = get_field('predlogi_slika_produkta', $rand_vecerja['recept']->ID);
		$img_product_url = $img_product['url'];
		$img_product_alt = $img_product['alt'];

		$link_featured_prod = get_field('predlogi_slika_produkta_link', $rand_vecerja['recept']->ID);
		$pozicija = get_field('predlogi_slika_produkta_pozicija', $rand_vecerja['recept']->ID);

		$recept_vecerja_arr = array(
			'predlog_link_title' => 'večerjo',
			'other1' => 'zajtrk',
			'other2' => 'kosilo',
			'title' => $title,
			'content' => $trimmed_recipe,
			'link' => $link,
			'img_main_url' => $img_main_url,
			'img_main_alt'  => $img_main_alt,
			'img_product_url' => $img_product_url,
			'img_product_alt' => $img_product_alt,
			'link_featured_prod' => $link_featured_prod,
			'img_produkta_pos' => $pozicija
		);

		return $recept_vecerja_arr;
	}

}


function mlinotest_get_contact_map_marker_data() {
	$map = get_field('kontakt_mapa', 17);
	$lat = $map['lat'];
	$lng = $map['lng'];
	$address = $map['address'];

	$contact_map_arr = array(
		'address' => $address,
		'lat' => $lat,
		'lng' => $lng
	);

	return $contact_map_arr;

}


function mlinotest_get_all_recipe_titles() {
	$args = array(
		'post_type' => 'recepti',
		'posts_per_page' => -1
	);
	$query = new WP_Query($args);

	$title_arr = [];

	if($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
		$id = get_the_id();
		$thumb = get_field('glavna_slika_recepta', $id);
		$post = array(
			'title' => get_the_title(),
			'link'  => get_permalink(),
			'thumb' => $thumb['url']
		);
		array_push($title_arr, $post);
	endwhile; endif; wp_reset_postdata();

	return $title_arr;

}


/*
social share buttons
*/

function get_FB_social_share_buttons($title, $link) {
  $url = urlencode($link);
  $title2 = str_replace( ' ', '%20', $title);
  $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='. $url;
  return $facebookURL;
}

function get_TW_social_share_buttons($title, $link) {
  $url = urlencode($link);
  $title2 = str_replace( ' ', '%20', $title);
  $twitterURL = 'https://twitter.com/home?status='.$title2.'&amp;url='.$url.'&amp;via=';
  return $twitterURL;
}

function get_mail_social_share_buttons($title, $link) {
	$url= urlencode($link);
	$title2 = str_replace( ' ', '%20', $title);
	$mailURL = 'mailto:?subject=' . $title2 . '&amp;body=Zdravo,%0A%0Ana Mlinotestovi strani imajo ta recept: ' . $url  . '%0A%0APredlagam, da ga preizkusiva! :)%0A%0ASporoči.';
	return $mailURL;
}

