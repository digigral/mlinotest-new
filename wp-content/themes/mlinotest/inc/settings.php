<?php

// Add featured image sizes


add_image_size( 'news-size', 600, 480, true ); // width, height, crop
add_image_size( 'news-size2', 600, 480 ); // width, height, crop

add_image_size( 'product-size', 600, 600, true ); // width, height, crop
add_image_size( 'product-size2', 600, 600 ); // width, height, crop

// Register the three useful image sizes for use in Add Media modal

add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'news-size' => __( 'news-size' ),
        'news-size2' => __( 'news-size2' ),
        'product-size' => __( 'product-size' ),
        'product-size2' => __( 'product-size2' ),
    ) );
}



function binary_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop )
{
    if ( !$crop ) return null;
    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);
    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);
    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );
    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'binary_thumbnail_upscale', 10, 6 );


function custom_loginlogo() {
  echo '<style type="text/css">
  h1 a {background-image: url('.get_bloginfo('template_directory').'/img/logo.png) !important; }
  </style>';
  }
add_action('login_head', 'custom_loginlogo');
