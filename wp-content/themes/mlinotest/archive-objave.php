<?php
get_header();
?>

<div class="wrapper wrapper-subpages" id="single-wrapper">
	<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="naslov-wrapper text-center">
				<h1><?php the_archive_title(); ?></h1>
			</div>
		</div>
	</div>
		<div class="row">

			<div class="col-md-4 order-2 order-md-1">
				<aside>
					<?php get_template_part("page-templates-parts/side/company-left-menu"); ?>
				</aside>
			</div>

            <div class="col-md-8 order-1 order-md-2">
          
                
                <ul class="archive-objave-list">
                <?php while ( have_posts() ) : the_post(); ?>

                    <li><span><?php echo get_the_date('d.m.Y'); ?></span><a href="<?php the_field('povezava_do_objave'); ?>"><?php the_title(); ?></a></li>     

                <?php endwhile; ?>
                </ul>
                <p class="back-to-objave"><span>&lt; </span><a href="<?php the_permalink(apply_filters( 'wpml_object_id', 496, 'post' )); ?>"><?php _e('Nazaj na trenutne objave', 'mlinotest'); ?></a></p>
            </div>

		</div>
	</div>
</div>

<?php get_footer(); ?>


